package com.components.yaml;

import com.iwaf.framework.BasePage;

public class DTVData {
	
	public String ZipCode;
	public String PageTitle;
	public String WTBProduct_PageTitle;
	public String StoreFinder_PageTitle;
	
	public static DTVData fetch(String key){
		BasePage pageObj = new BasePage();
		DTVData obj = pageObj.getCommand().loadYaml(key, "data-pool/DTV_Data.yaml");
		return obj;
	}
}

