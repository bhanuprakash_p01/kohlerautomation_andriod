package com.components.pages;

import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.components.repository.SiteRepository;
import com.components.yaml.NewsLetterSignUp;
import com.components.yaml.SearchData;
import com.iwaf.framework.components.Target;
import com.iwaf.framework.components.IReporter.LogType;

public class Kohler_GeneralNavigation extends SitePage
{
	
	/* Defining the locators on the Page */ 
	
	public static final Target Link_bathroom = new Target("Link_bathroom","//button[@id= 'main-nav__button--bathroom']",Target.XPATH);	
	public static final Target Link_bathroomMainMenu = new Target("Link_bathroommainMenu","//div[@id='main-nav']//following::div[@id='sub-nav-tab--bathroom']",Target.XPATH);	
	public static final Target Link_bathroomPage = new Target("Link_bathroomPage","//*[@id='sub-nav-tab--bathroom']//following::a[text()=' Bathroom']",Target.XPATH);	
	public static final Target Link_bathroomPage_sink = new Target("Link_bathroomPage_sink","//*[@id='sub-nav-tab--bathroom']//following::a[text()=' Bathroom']",Target.XPATH);
	public static final Target Link_ChoregraphShowerPlanner = new Target("Link_ChoregraphShowerPlanner","//*[@id='sub-nav-tab--bathroom']/div/div/div[3]/ul/li[6]/a",Target.XPATH);
	public static final Target Link_BathroomProductBuyingGuide = new Target("Link_BathroomProductBuyingGuide","//*[@id='sub-nav-tab--bathroom']/div/div/div[3]/ul/li[1]/a",Target.XPATH);
	public static final Target Link_KitchenProductBuyingGuide = new Target("Link_KitchenProductBuyingGuide","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/a[7]",Target.XPATH);
	public static final Target Header_SaveToFolder = new Target("Header_SaveToFolder","//*[@id='hideAddModal']/h1",Target.XPATH);
	public static final Target button_SaveToFolderClose = new Target("button_SaveToFolderClose","//*[@id='hideAddModal']/button",Target.XPATH);
	public static final Target textBox_SaveToFolder = new Target("textBox_SaveToFolder","//*[@id='textarea-140-5']",Target.XPATH);
	public static final Target button_SaveToFolderSave = new Target("button_SaveToFolderSave","//*[@id='addToFolder']/button[1]",Target.XPATH);
	public static final Target button_SaveToFolderCancel = new Target("button_SaveToFolderCancel","//*[@id='addToFolder']/button[2]",Target.XPATH);
	public static final Target title_ItemAddedToFold1= new Target("title_ItemAddedToFold1","//*[@id='showThankyou']/p/span[1]",Target.XPATH);
	public static final Target title_ItemAddedToFold2 = new Target("title_ItemAddedToFold2","//*[@id='showThankyou']/p/span[2]",Target.XPATH);
	public static final Target button_ItemAddedToFoldClose = new Target("button_ItemAddedToFoldClose","//*[@id='showThankyou']/button",Target.XPATH);
	public static final Target Items_shareItems = new Target("Items_shareItems","//*[@class='share-tip organic-namespace olbox-container']",Target.XPATH);
	public static final Target Items_shareItemFaceebook = new Target("Items_shareItems","//*[@class='share-tip organic-namespace olbox-container']/div/ul/li[3]/a",Target.XPATH);
	public static final Target Win_ShowThankYou = new Target("Win_ShowThankYou","//*[@id='showThankyou']",Target.XPATH);
	public static final Target Link_Kitchen = new Target("Link_Kitchen","//button[@id= 'main-nav__button--kitchen']",Target.XPATH);	
	public static final Target Link_kitchenMainMenu = new Target("Link_kitchenMainMenu","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]",Target.XPATH);	
	public static final Target Link_KitchenPage = new Target("Link_KitchenPage","//*[@id='sub-nav-tab--kitchen']//following::a[text()='Kitchen ']",Target.XPATH);	
	public static final Target Link_kitchenPlanner = new Target("Link_kitchenPlanner","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/a[11]",Target.XPATH);
	public static final Target Link_IdeaskitchenPlanner = new Target("Link_IdeaskitchenPlanner","//*[@id='sub-nav-tab--ideas']/div/div/div[1]/a[9]",Target.XPATH);
	public static final Target Link_PressRoom = new Target("Link_PressRoom","//*[@id='footer__tab--1']/a[4]",Target.XPATH);
	public static final Target Link_FindaStore = new Target("Link_FindaStore","//*[@id='store-locatores']",Target.XPATH);
	public static final Target textbox_FindaStore = new Target("textbox_FindaStore","//*[@id='store-locator__search-field']",Target.XPATH);
	public static final Target button_FindaStore = new Target("button_FindaStore","//*[@id='store-locator__search-button-container']/div",Target.XPATH);
	public static final Target text_NoStoreLocator = new Target("text_NoStoreLocator","//*[@id='store-locator__num-results']/span[1]",Target.XPATH);
	public static final Target text_StoreLocator_num = new Target("text_StoreLocator_num","//*[@id='store-locator__num-results']",Target.XPATH);
	public static final Target text_StoreLocator_result = new Target("text_StoreLocator_result","//*[@id='store-locator__refine-search-tray']/div[2]/span[2]",Target.XPATH);
	public static final Target text_StoreLocator_location = new Target("text_StoreLocator_location","//*[@id='store-locator__location']",Target.XPATH);
	public static final Target count_StoreLocator_results = new Target("count_StoreLocator_results","//*[@id='store-locator__results']/ul/li",Target.XPATH);
	public static final Target Button_PrintCancelclick = new Target("Button_PrintCancelclick","//*[@id='print-header']/div/button[2]",Target.XPATH);
	public static final Target text_pressRoom = new Target("text_pressRoom","//*[@class='container']//following::div[@class='press-room-section press-room-intro']/h2",Target.XPATH);
	public static final Target Link_Ideas = new Target("Link_Ideas","//button[@id= 'main-nav__button--ideas ']",Target.XPATH);	
	public static final Target Link_ideasMainMenu = new Target("Link_ideasMainMenu","//*[@id='sub-nav-tab--ideas']/div",Target.XPATH);	
	public static final Target Link_IdeasPage = new Target("Link_IdeasPage","//*[@id='sub-nav-tab--ideas']//following::a[text()='Ideas']",Target.XPATH);	
	
	public static final Target Link_HelpUsToImproveMore = new Target("link_HelpUsToImpMore","//a[@id='kampylink']",Target.XPATH);	
	public static final Target text_commenttextbox = new Target("text_commenttextbox","//textarea[@id='textarea']",Target.XPATH);	
	public static final Target btn_sendmyComments = new Target("btn_comments","//button[@class='submitButton']",Target.XPATH);	
	public static final Target btn_suggestion = new Target("btn_Sugg","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[1]/label/span/span",Target.XPATH);	
	public static final Target btn_dislike = new Target("btn_dlike","//*[@id='TagId-91783']",Target.XPATH);	
	public static final Target btn_praise = new Target("btn_praise","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[3]/label/span/span",Target.XPATH);	
	public static final Target btn_Rating = new Target("btn_rating","//*[@id= 'ItemId-35770-2']",Target.XPATH);	
	public static final Target btn_closeTheWind = new Target("btn_closeTheWind","//*[@id= 'Finish']",Target.XPATH);	
	
	public static final Target link_NewsLetterSignUp = new Target("link_NewsLetterSignUp","//*[@id='footer__tab--1']/a[5]",Target.XPATH);	
	public static final Target text_Email = new Target("text_Email","//*[@id='field0']",Target.XPATH);	
	public static final Target text_FirstName = new Target("text_FirstName","//*[@id='field1']",Target.XPATH);	
	public static final Target text_LastName = new Target("btn_closeTheWind","//*[@id='field2']",Target.XPATH);	
	public static final Target text_PostalCode = new Target("btn_closeTheWind","//*[@id='field7']",Target.XPATH);	
	public static final Target select_Country = new Target("select_Country","//*[@id='field8']",Target.XPATH);	
	public static final Target select_occupation = new Target("select_occupation","//*[@id='field9']",Target.XPATH);	
	public static final Target button_interiorProducts = new Target("button_KitchenBath","//*[@id='heading2']//following::a",Target.XPATH);	
	public static final Target Div_ExpandedFields = new Target("Div_ExpandedFields","//*[@id='collapse2']",Target.XPATH);	
	public static final Target checkbox_interiorProducts = new Target("checkbox_interiorProducts","//*[@id='formElement22']//following::div[@class='check-field']/label",Target.XPATH);	
	public static final Target button_Submit = new Target("button_Submit","//*[@id='form42']//following::div[@class='pull-right']/button",Target.XPATH);	
	public static final Target text_thankyou = new Target("text_thankyou","//td[@class='valign-able']//following::span[2]",Target.XPATH);	
	
	public static final Target Link_Parts = new Target("Link_Parts","//button[@id='main-nav__button--parts']",Target.XPATH);	
	public static final Target Link_PartsMainMenu = new Target("Link_PartsMainMenu","//*[@id='sub-nav-tab--parts']",Target.XPATH);	
	public static final Target Link_PartsPage = new Target("Link_PartsPage","//*[@id='sub-nav-tab--parts']/div/div/div[2]/a[1]",Target.XPATH);	
	public static final Target Link_PartsWizard = new Target("Link_PartsWizard","/html/body/div[5]/div[2]/section[2]/div/div/a",Target.XPATH);	
	public static final Target button_ViewResults = new Target("button_ViewResults","//*[@id='question-features']//following::a",Target.XPATH);	
	public static final Target button_ViewResultsCount = new Target("button_ViewResultsCount","//*[@id='question-features']//following::a/span",Target.XPATH);	
	public static final Target Count_Results = new Target("Count_Results","//*[@id='finder-results']//following::span[1]",Target.XPATH);	
	
	public static final Target link_FindAPro = new Target("link_FindAPro","//*[@id='pro-locatores']",Target.XPATH);	
	public static final Target Ele_NeedHelpWith = new Target("Ele_NeedHelpWith","//*[@id='pro-finder']/div/div/div/div[1]/h3",Target.XPATH);	
	public static final Target Ele_CustomerSupport = new Target("Ele_CustomerSupport","//*[@id='pro-finder']/div/div/div/div[2]/h3",Target.XPATH);	
	public static final Target EleValue_NeedHelpWith = new Target("EleValue_NeedHelpWith","//*[@id='pro-finder']//following::div[@class='col-6-lg col-12-md col-block col-block-accordion']/h3",Target.XPATH);	
	public static final Target button_CustSupport = new Target("button_CustSupport","//*[@id='pro-finder']//following::div[@class='col-6-lg col-12-md col-block marg-t-25-sm marg-t-0-lg marg-b-50-sm']/button",Target.XPATH);	
	public static final Target button_NeedHelpWithSubmit = new Target("button_NeedHelpWithSubmit","//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']",Target.XPATH);	
	public static final Target button_getDesignHelp = new Target("button_getDesignHelp","//*[@id='BDH']/div/div/button",Target.XPATH);	
	public static final Target button_KohlerProductInst = new Target("button_KohlerProductInst","//div[@class='accordion marg-t-30-sm row']//following::div[@class='accordion-item col-12-lg']/button/header/h3",Target.XPATH);	
	public static final Target text_FindAProErrorMsg = new Target("text_FindAProErrorMsg","//div[@class='container no-results-find-a-pro']//following::div[@class='p10 form-help-text marg-b-40-sm']/p",Target.XPATH);	
	public static final Target Link_GoBack = new Target("Link_GoBack","//a[@class='back-results-button']",Target.XPATH);	
	
	public static final Target text_FAPPageHeader = new Target("text_FAPPageHeader","//div[@class='contact-forms-basic marg-t-50-sm']//following::div[@class='main-header']/h1",Target.XPATH);	
	public static final Target button_GetAnEstm = new Target("button_GetAnEstm","//div[@class='col-4-md why-certified-block marg-t-45-md marg-t-0-sm']//following::button",Target.XPATH);	
	public static final Target result_Address = new Target("result_Address","//div[@class='row pad-l-45-md pad-r-45-md pad-l-0-lg pad-r-0-lg']/div/h3",Target.XPATH);	
	public static final Target text_getAnEstmHeader = new Target("text_getAnEstmHeader","//div[@class='contact-forms-basic marg-t-50-sm']//following::h1",Target.XPATH);	
	
	
    public static final Target bathroom_Link=new Target("bathroom_btn","//*[@id='sub-nav-tab--bathroom']/div/a",Target.XPATH);
    public static final Target chreographShowerPlanner_Link=new Target("chreographShowerPlanner_Link","//*[@class='sub-nav-tab__section-link display-none-md pad-l-15-sm' and contains(text(),'Choreograph Shower Planner')]",Target.XPATH);
    public static final Target bathroom_Menu=new Target("bathroom_Menu","//*[@id='sub-nav-tab--bathroom']",Target.XPATH);
    public static final Target Items_shareItemFacebook = new Target("Items_shareItems","//*[@class='share-tip organic-namespace olbox-container']/div/ul/li[3]/a",Target.XPATH);
    public static final Target productBuyingGuide_Link=new Target("productBuyingGuide_Link","//*[@class='sub-nav-tab__section-link display-none-md pad-l-15-sm' and contains(text(),'Product Buying Guides')]",Target.XPATH);
    
    
    public static final Target bathroomDesign_Help=new Target("bathroomDesign_Help","//*[@id='pro-finder']/div/div/div/div[1]/div/div[2]/button/header/h3",Target.XPATH);
    public static final Target ideas_Link=new Target("ideas_Link"," //*[@id='sub-nav-tab--ideas']/div/a",Target.XPATH);
 
    
    //#################### Mobile Xpaths
    
    //sidemenu parts link from hamburger
       
       public static final Target hamburegr_parts_link=new Target("hamburegr_parts_link","//*[@id='main-nav__button--parts']",Target.XPATH);
       
       public static final Target hamburegr_parts_findparts=new Target("hamburegr_parts_findparts","//*[@id='sub-nav-tab--parts']/div/div/div[2]/a[1]",Target.XPATH);
      
       public static final Target hamburegr_parts_submenus=new Target("hamburegr_parts_submenus","//*[@id=sub-nav-tab--parts]/div/div/div[2]",Target.XPATH);
       
       public static final Target parts_wizard=new Target("parts_wizard","//section[@class='mobile-features visible-xs']//div//a[@class='btn btn-hero']",Target.XPATH);
       
       public static final Target count_visible=new Target("count_visible","(//header[@class='result-finder-header']//h3//span)[1]",Target.XPATH);
       
       public static final Target total_count_inapp=new Target("total_count_inapp","//div[@id='product-category']//div[@id='product-category__product-panels']//div//div",Target.XPATH);
       
       
       public static final Target text_NoStoreLocator_mobile=new Target("text_NoStoreLocator_mobile","(//*[@id='store-locator__num-results']/span[1])[2]",Target.XPATH);
       
       public static final Target search_again_link=new Target("search_again_link","//*[@id='store-locator__search-again']",Target.XPATH);
       
       public static final Target storelocator_number=new Target("storelocator_number","//*[@id='store-locator__search-results-header']",Target.XPATH);
       
       
       public static final Target storelocator_total_count=new Target("storelocator_total_count","//div[@id='store-locator__results']//ul//li",Target.XPATH);
       
       public static final Target storelocator_number_only=new Target("storelocator_number_only","//div[@id='store-locator__search-results-header']//span[1]",Target.XPATH);
       public static final Target our_Company=new Target("our_Company","//*[@data-toggle-target='#footer__tab--1']",Target.XPATH);
		public static final Target link_Careers=new Target("link_Careers","	//*[@id='footer__tab--1']/a[3]",Target.XPATH);
	
     
    
    
    
    public Kohler_GeneralNavigation(SiteRepository repository)
	{
		super(repository);
	}

	public Kohler_GeneralNavigation _GoToGeneralNavigation()
	{
		log("navigate to general navigation page",LogType.STEP);
		//getCommand().captureScreenshot("C:\\Users\\Arvind01\\Desktop\\Add To Cart\\HomePage.png");
		return this;
		
	}
	
	// verify Bathroom expansion and bathroom links
	public Kohler_GeneralNavigation VerifyBathroomMainMenu()
	{
		
		 try {
             
             getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
             getCommand().waitForTargetVisible(Kohler_HomePage.bathroom_btn).click(Kohler_HomePage.bathroom_btn);
             getCommand().waitFor(2);
             
             if(getCommand().isTargetVisible(bathroom_Menu))
                                             Assert.assertTrue(true, "Bathroom main menu is expanded successfully");
                                             
             int Win_size = getCommand().driver.getWindowHandles().size();
             log("Open Windows Size is :" + Win_size ,LogType.STEP);
             
             log(getCommand().getText(bathroom_Link),LogType.STEP);
             String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);                                                        
             getCommand().sendKeys(bathroom_Link, selectLinkOpeninNewTab);                                                      
             getCommand().waitFor(5);
             
             ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
             getCommand().driver.switchTo().window(tabs2.get(1));
             String pageTitle = getCommand().driver.getTitle();
		
             log("Page title is:" + pageTitle,LogType.STEP);
             Assert.assertEquals(pageTitle, "Bathroom | KOHLER", "Page is not navigated to bathroom Page");
		 
		 List<WebElement> Bathroom_links = getCommand().driver.findElements(By.xpath("//*[@id='section']/div"));
		 int link_count = Bathroom_links.size();
		 
		 for (int i = 3; i <= link_count; i++)
		 {
		             
		             WebElement link = getCommand().driver.findElement(By.xpath("//*[@id='section']/div["+i+"]/div/div[1]/a"));
		             System.out.println(link.getText());
		             log("Link text is :" + link.getText(),LogType.STEP);
		             
		                             log("Click on link in Bathroom homepage",LogType.STEP);
		                             String LinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);                                                   
		                             link.sendKeys(LinkOpeninNewTab);                                                         
		                             getCommand().waitFor(10);
		                             
		                             ArrayList<String> tabs3 = new ArrayList<String> (getCommand().driver.getWindowHandles());
		                             getCommand().driver.switchTo().window(tabs3.get(2));
		                             
		                 Win_size = getCommand().driver.getWindowHandles().size();
		                 Assert.assertEquals(3, Win_size, "On clicking on link, the page is not opned in new window");
		                 
		                 
		                 log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
		                 System.out.println(getCommand().driver.getTitle());
		
		                 
		                 getCommand().driver.close();
		             getCommand().driver.switchTo().window(tabs3.get(1));
		             
		             }
		 
		 
		            getCommand().driver.close();
		             getCommand().driver.switchTo().window(tabs2.get(0));
		}catch(Exception ex)
				{
					Assert.fail(ex.getMessage());
				}
				
				
				return this;
			}

	// verify Bathroom menu links and link navigation
	public Kohler_GeneralNavigation verifyBathroomSubMenuLinks()
	{
		try{
			
			   getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
               getCommand().waitForTargetPresent(Kohler_HomePage.bathroom_btn).click(Kohler_HomePage.bathroom_btn);
               getCommand().waitFor(2);
		
			if(getCommand().isTargetVisible(bathroom_Menu))
				Assert.assertTrue(true, "Bathroom main menu is expanded successfully");
			
			List<WebElement> Bathroom_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--bathroom']/div/div/div[1]/div[2]/div[1]/a"));
		    int link_count = Bathroom_SubMenuLinks.size();
		    
		    for (int i = 1; i <= link_count; i++)
		    {
		    		Target Bathroom_Links =new Target("Bathroom_Links","//*[@id='sub-nav-tab--bathroom']/div/div/div[1]/div[2]/div[1]/a["+i+"]",Target.XPATH);
		    		
		    		String subMenu_Text=getCommand().getText(Bathroom_Links);
							System.out.println(subMenu_Text);
							log("verify the link text "+  subMenu_Text,LogType.STEP);
							
							log("click on each link and verify in new tab",LogType.STEP);
							getCommand().sendKeys(Bathroom_Links,Keys.chord(Keys.CONTROL,Keys.RETURN) );
							getCommand().waitFor(10);
							ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
							getCommand().driver.switchTo().window(tabs2.get(1));
						    int Win_size = getCommand().driver.getWindowHandles().size();
						    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
						    
							String pageTitle = getCommand().driver.getTitle();
							log("verify the Page title "+ pageTitle,LogType.STEP);
							System.out.println(pageTitle);
											
							getCommand().driver.close();
							log("Close the tab and switch back to parent window",LogType.STEP);
							getCommand().driver.switchTo().window(tabs2.get(0));
					
		    	
		    		
		    }
			
		
		
		}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// Verify choreograph Shower Planner link functionality+ new Window
	public Kohler_GeneralNavigation verifychoreographShowerPlanner()
	{
	    try
        {
                        log("Verify choreograph Shower Planner link" ,LogType.STEP);
                        getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
                        getCommand().waitForTargetVisible(Kohler_HomePage.bathroom_btn).click(Kohler_HomePage.bathroom_btn);
                        getCommand().waitFor(2);
             
                        
        
                        if(getCommand().isTargetVisible(bathroom_Menu))
                                        Assert.assertTrue(true, "Bathroom main menu is expanded successfully");
                        
                        int Win_size = getCommand().driver.getWindowHandles().size();
                        log("Open Windows Size is :" + Win_size ,LogType.STEP);
                        
                        WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='sub-nav-tab--bathroom']/div/div/div[1]/div[2]/div[1]/a[23]"));
             	       JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
             	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
             	       Thread.sleep(3000);
             	       
                        getCommand().click(chreographShowerPlanner_Link);
                        
                       // getCommand().scrollTo(STC);
                      //  getCommand().waitForTargetPresent(chreographShowerPlanner_Link).click(chreographShowerPlanner_Link);
                       
                       // 
                        ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
                        getCommand().driver.switchTo().window(tabs.get(1));
                        log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
                        Win_size = getCommand().driver.getWindowHandles().size();
                        Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
            
                        log("Close the Child Window: "+getCommand().driver.getTitle(),LogType.STEP);
                        getCommand().driver.close();
                        getCommand().driver.switchTo().window(tabs.get(0));
                                        
                        
        }catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	
		
	}

	// verify bathroom Product Buying guide and Article page
	public Kohler_GeneralNavigation VerifyBathroomProductBuyingGuide()
	{
		 try 
         {
                         List<String> ShareLinkTextItems = new ArrayList<String>();
                         String Share_linkText=null;
                         log("Verify Product Buying Guide link" ,LogType.STEP);
                         getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
                         getCommand().waitForTargetVisible(Kohler_HomePage.bathroom_btn).click(Kohler_HomePage.bathroom_btn);
                         getCommand().waitFor(2);
                         //getCommand().waitForTargetPresent(bathroom_Link).click(bathroom_Link);
                         
                         if(getCommand().isTargetVisible(bathroom_Menu))
                                         Assert.assertTrue(true, "Bathroom main menu is expanded successfully");
                         
                         log("Verify Article page on clicking on PBG link" ,LogType.STEP);
                         WebElement element=getCommand().driver.findElement(By.xpath("//*[@class='sub-nav-tab__section-link display-none-md pad-l-15-sm' and contains(text(),'Product Buying Guides')]"));
               	       JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
               	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
               	       Thread.sleep(3000);
                         
                         
                         
                         getCommand().click(productBuyingGuide_Link);
                         System.out.println(getCommand().getPageUrl());
                         
         
                         
                         boolean condtion= true;
                         log("Verify the Article Page URL",LogType.STEP);
                         String URL = getCommand().driver.getCurrentUrl();
                         if(URL.contains("article"))
                         Assert.assertTrue(condtion);
                         
                         List<WebElement> buttons = getCommand().driver.findElements(By.xpath("//*[@id='article-page']//following::div[@class='article-page__icon-container']/button/i"));
                         //int button_count = buttons.size();
                         for(WebElement button: buttons)
                         {
                                         String text_button = button.getAttribute("class");
                                         String text = text_button.replaceFirst("icon--", "");
                                         
                                         switch (text)
                                         {
                                         case "bookmark":
                                                         
                                                         button.click();
                                                         getCommand().isTargetVisible(Header_SaveToFolder);
                                                         System.out.println(getCommand().getText(Header_SaveToFolder));
                                                         Assert.assertEquals(getCommand().getText(Header_SaveToFolder),"SAVE TO MY FOLDERS", "Application is not navigated to Save to folder window");
                                                         
                                                         getCommand().isTargetVisible(Kohler_HomePage.addToFolderTextArea);
                                                         getCommand().sendKeys(Kohler_HomePage.addToFolderTextArea, "Test");
                                                         
                                                         getCommand().isTargetVisible(Kohler_HomePage.addToFolderSubmitButton);
                                                         getCommand().click(Kohler_HomePage.addToFolderSubmitButton);
                                                         Thread.sleep(2000);
         
                                                         
                                                         if(getCommand().isTargetVisible(Win_ShowThankYou))
                                                                         Assert.assertTrue(true, "Thank you page is expanded successfully");
                                                         
                                                         getCommand().isTargetVisible(title_ItemAddedToFold1);
                                                         String title1= getCommand().getText(title_ItemAddedToFold1);
                                                         
                                                         getCommand().isTargetVisible(title_ItemAddedToFold2);
                                                         String title2= getCommand().getText(title_ItemAddedToFold2);
                                                         
                                                         String title = title1+" "+title2;
                                                         Assert.assertEquals(title,"Your item was successfully added to My Kohler Folder", "Application is not navigated to My Kohler folder");
                                                         
                                                         getCommand().isTargetVisible(button_ItemAddedToFoldClose);
                                                         getCommand().click(button_ItemAddedToFoldClose);
                                                         
                                                         break;
                                                         
                                         case "share":
                                                         button.click();
                                                         getCommand().isTargetVisible(Items_shareItems);
                                                         List<WebElement> shareLinks = getCommand().driver.findElements(By.xpath("//*[@class='share-tip organic-namespace olbox-container']/div/ul/li/a"));
                                                         for(WebElement linkText: shareLinks)
                                                         {
                                                                         Share_linkText = linkText.getText();
                                                                         log("Share options are : " + Share_linkText,LogType.STEP);
                                                         }
                                                         ShareLinkTextItems.add(Share_linkText);
                                                         
                                                         getCommand().isTargetVisible(Items_shareItemFacebook);
                                                         getCommand().click(Items_shareItemFacebook);
                                                         getCommand().waitFor(10);
                                                         
                                                         ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
                                                         getCommand().driver.switchTo().window(tabs.get(1));
			                                             log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
			                                             int Win_size = getCommand().driver.getWindowHandles().size();
			                                             Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opened in new window");
			                                             
			                                             log("Close the Child Window: "+getCommand().driver.getTitle(),LogType.STEP);
			                                             getCommand().driver.close();
			                                             getCommand().driver.switchTo().window(tabs.get(0));
                                                         
                                         break;
//                                     
                                        
                                        }
                                         
                                         
                                         }

         }catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// verify Kitchen expansion and bathroom links
	 public static final Target kitchen_Link=new Target("kitchen_Link","//*[@id='sub-nav-tab--kitchen']/div/a",Target.XPATH);
	    public static final Target kitchen_Menu=new Target("kitchen_Menu","//*[@id='sub-nav-tab--kitchen']",Target.XPATH);
	  
	    public Kohler_GeneralNavigation VerifyKitchenMainMenu()
	    {
	                                    
	                    try {                        
	                                                   log("verify Kitchen Main menu",LogType.STEP);
	                                                   getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
	                                                   getCommand().waitForTargetVisible(Kohler_HomePage.kitchen_btn).click(Kohler_HomePage.kitchen_btn);
	                                                   getCommand().waitFor(2);
	                                                                    
	                                                             
	                                                                    
	                                                  if(getCommand().isTargetVisible(kitchen_Menu))
	                                                            Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
	                                                    
	                                                   int Win_size = getCommand().driver.getWindowHandles().size();
	                                                   log("Open Windows Size is :" + Win_size ,LogType.STEP);
	                                                                    
	                                                    log(getCommand().getText(kitchen_Link),LogType.STEP);
	                                                    String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);                                                                 
	                                                    getCommand().sendKeys(kitchen_Link, selectLinkOpeninNewTab);                                                           
	                                                     getCommand().waitFor(5);
	                                                                    
	                                                   ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                                                  getCommand().driver.switchTo().window(tabs2.get(1));
	                                                        String pageTitle = getCommand().driver.getTitle();
	                                                        //System.out.println("Page title is:" + pageTitle);
	                                                        log("Page title is:" + pageTitle,LogType.STEP);
	                                                        
	                                                        Assert.assertEquals(pageTitle, "Kitchen | KOHLER", "Page is not navigated to Kitchen Page");
	                                                        
	                                                        List<WebElement> Kitchen_links = getCommand().driver.findElements(By.xpath("//*[@id='section']/div"));
	                                                        int link_count = Kitchen_links.size();
	                                                        
	                                                        for (int i = 3; i <= link_count; i++)
	                                                        {
	                                                                    
	                                                                    WebElement link = getCommand().driver.findElement(By.xpath("//*[@id='section']/div["+i+"]/div/div[1]/a"));
	                                                                    System.out.println(link.getText());
	                                                                    log("Link text is :" + link.getText(),LogType.STEP);
	                                                                    

	                                                                                    log("Click on link in Kitchen homepage",LogType.STEP);
	                                                                                    String LinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);                                                            
	                                                                                    link.sendKeys(LinkOpeninNewTab);                                                         
	                                                                                    getCommand().waitFor(5);
	                                                                                    
	                                                                                    ArrayList<String> tabs3 = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                                                                                    getCommand().driver.switchTo().window(tabs3.get(2));
	                                                                        Win_size = getCommand().driver.getWindowHandles().size();
	                                                                        Assert.assertEquals(3, Win_size, "On clicking on link, the page is not opned in new window");
	                                                                        
	                                                                        log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
	                                                                        System.out.println(getCommand().driver.getTitle());
	                                                                        getCommand().driver.close();
	                                                                    getCommand().driver.switchTo().window(tabs3.get(1));
	                                                                    
	                                                                    }
	                                                        
	                                                        
	                                                                   getCommand().driver.close();
	                                                                    getCommand().driver.switchTo().window(tabs2.get(0));
	                                                    
	                                                    
	                                    }catch(Exception ex)
	                                    {
	                                                    Assert.fail(ex.getMessage());
	                                    }
	                                    
	                                    
	                                    return this;
	                    }

		
	// verify Kitchen menu links and link navigation
	public Kohler_GeneralNavigation verifyKitchenSubMenuLinks()
	{
		
			
			try{
				
				   getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
                getCommand().waitForTargetPresent(Kohler_HomePage.kitchen_btn).click(Kohler_HomePage.kitchen_btn);
                getCommand().waitFor(2);
				//getCommand().wait(5);
			
				if(getCommand().isTargetVisible(Link_kitchenMainMenu))
					Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
				
				List<WebElement> Kitchen_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/a"));
			    int link_count = Kitchen_SubMenuLinks.size();
			    
			    for (int i = 1; i <= link_count; i++)
			    {
			    		Target Kitchen_Links =new Target("Kitchen_Links","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/a["+i+"]",Target.XPATH);
			    		
			    		String subMenu_Text=getCommand().getText(Kitchen_Links);
								System.out.println(subMenu_Text);
								log("verify the link text "+  subMenu_Text,LogType.STEP);
								
								log("click on each link and verify in new tab",LogType.STEP);
								getCommand().sendKeys(Kitchen_Links,Keys.chord(Keys.CONTROL,Keys.RETURN) );
								getCommand().waitFor(10);
								ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
								getCommand().driver.switchTo().window(tabs2.get(1));
							    int Win_size = getCommand().driver.getWindowHandles().size();
							    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
							    
								String pageTitle = getCommand().driver.getTitle();
								log("verify the Page title "+ pageTitle,LogType.STEP);
								System.out.println(pageTitle);
							
												
								getCommand().driver.close();
								log("Close the tab and switch back to parent window",LogType.STEP);
								getCommand().driver.switchTo().window(tabs2.get(0));
						
			    	
			    		
			    }
				
			
			
			}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// Verify kitchen Planner link functionality+ new Window
	public Kohler_GeneralNavigation verifyKitchenPlanner()
	{
		try
		{

            getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
            getCommand().waitForTargetPresent(Kohler_HomePage.kitchen_btn).click(Kohler_HomePage.kitchen_btn);
            getCommand().waitFor(2);
			log("Verify Kitchen Planner link" ,LogType.STEP);
			
		
			  if(getCommand().isTargetVisible(kitchen_Menu))
                  Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
  
			int Win_size = getCommand().driver.getWindowHandles().size();
			log("Open Windows Size is :" + Win_size ,LogType.STEP);
			
			WebElement element=getCommand().driver.findElement(By.xpath("//*[@id=\"sub-nav-tab--kitchen\"]/div/div/div/div[1]/a[7]"));
     	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
     	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
     	       Thread.sleep(3000);
			
			getCommand().click(Link_kitchenPlanner);
			getCommand().waitFor(2);
			
			ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			getCommand().driver.switchTo().window(tabs.get(1));
		    log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
		    Win_size = getCommand().driver.getWindowHandles().size();
		    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
		    
		    log("Close the Child Window: "+getCommand().driver.getTitle(),LogType.STEP);
		    getCommand().driver.close();
	    	getCommand().driver.switchTo().window(tabs.get(0));
			   	
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	
		
	}

	// verify Press room link/ functionality
	 public static final Target text_PressRoom=new Target("text_PressRoom","//*[@class='press-room-title']",Target.XPATH);
	    public Kohler_GeneralNavigation VerifyPressRoomLink()
	    {
	                    try {
	                                    
	                                    log("Verify PressRoom link" ,LogType.STEP);
	                                    getCommand().scrollTo(Kohler_HomePage.viewGallery_CTA).click(Kohler_HomePage.our_Company);
	                                    
	                                    WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='footer__tab--1']/a[2]"));
	                            	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
	                            	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
	                            	       Thread.sleep(3000);
	                                   
	                                    getCommand().click(Link_PressRoom);
	                                    
	                                    log("Verify the Press Room Page Title",LogType.STEP);
	                                    Assert.assertEquals(getCommand().driver.getTitle() ,"Press Room | KOHLER","Home Page Title mismatch");
	                                    
	                                    getCommand().isTargetPresent(text_PressRoom);
	                                    log("Verify the Press Room Page Header:"+getCommand().getText(text_PressRoom) ,LogType.STEP);
	                                    Assert.assertEquals(getCommand().getText(text_PressRoom),"Press Room" ,"Page header mis matches");
	                                    
	                                    
	                    }catch(Exception ex)
	                    {
	                                    Assert.fail(ex.getMessage());
	                    }
	                    return this;
	    }


	// verify Find a store functionality
	public Kohler_GeneralNavigation VerifyFindAStore()
	{
		 try
         {
                         getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
                         List<String> StoreLocationResults = new ArrayList<String>();
                         String resultName=null;
                         log("Verify Find a Store link" ,LogType.STEP);
                        
                         WebElement element=getCommand().driver.findElement(By.xpath("(//*[@class='mobile-sign-in__link'])[1]"));
               	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
               	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
               	       Thread.sleep(3000);
                         
                         
                         
               	       getCommand().click(Kohler_HomePage.link_findStore);
                         
                         
                         
                         
                         log("Verify the Find a Store Page Title",LogType.STEP);
                         System.out.println(getCommand().driver.getTitle());
                         Assert.assertEquals(getCommand().driver.getTitle() ,"Results | Find a Store | KOHLER","Home Page Title mismatch");
                         
                         log("Verify store locator text" ,LogType.STEP);
                         getCommand().isTargetVisible(text_NoStoreLocator);
                         String storeLocatorText = getCommand().getText(text_NoStoreLocator);
                         String expectedStoreLocText= "We are sorry, but we have no stores within 250 miles of your location. Please try another search.";
                         
                         if(!storeLocatorText.equalsIgnoreCase(expectedStoreLocText))
                         {
                                         log("Store is within the radius of your location",LogType.STEP);
                                         String StoreLocator_text = getCommand().getText(text_StoreLocator_num) + getCommand().getText(text_StoreLocator_result) + " for " + getCommand().getText(text_StoreLocator_location);
                                         log("Store is within the radius and results are: "+ StoreLocator_text ,LogType.STEP);
                                         getCommand().isTargetVisible(count_StoreLocator_results);
                                         Assert.assertEquals(getCommand().getText(text_StoreLocator_num).toString(), getCommand().getTargetCount(count_StoreLocator_results), "Store locator count is not matching with store locator results");
                                         
                                         List<WebElement> storeLocator_results = getCommand().driver.findElements(By.xpath("//*[@id='store-locator__results']/ul/li"));
                                         int count_results = storeLocator_results.size();
                                         
                                         for (int i = 0; i < count_results; i++)
                                         {
                                                         
                                                         resultName= getCommand().driver.findElement(By.xpath("//*[@id='store-locator__results']/ul/li["+i+"]/a")).getText();
                                                         log("Store results name is: "+ resultName  ,LogType.STEP);
                                                         StoreLocationResults.add(resultName); 
                                                         
                                         }
                                                                         
                         
                         }
                         else
                         {
                                         log("Store is not within the radius of your location",LogType.STEP);
                         }
                         
                         log("Input a value in search box to find a store location" ,LogType.STEP);
                         getCommand().isTargetVisible(textbox_FindaStore);
                         getCommand().clear(textbox_FindaStore);
                         getCommand().sendKeys(textbox_FindaStore , "ohio");
                         getCommand().isTargetVisible(button_FindaStore);
                         getCommand().click(button_FindaStore);
                         Thread.sleep(3000);
                         
                         log("Verify store locator text" ,LogType.STEP);
                         getCommand().isTargetVisibleAfterWait(text_StoreLocator_num);
                         storeLocatorText = getCommand().getText(text_StoreLocator_num);
                         System.out.println(storeLocatorText);
                         
                         
                         
                         if(!storeLocatorText.equalsIgnoreCase(expectedStoreLocText))
                         {
                                         log("Store is within the radius of your location",LogType.STEP);
                                         String StoreLocator_text = storeLocatorText +" "+ getCommand().getText(text_StoreLocator_result) + " for " + getCommand().getText(text_StoreLocator_location);
                                         log("Store is within the radius and results are: "+ StoreLocator_text ,LogType.STEP);

                                         getCommand().isTargetVisible(count_StoreLocator_results);
                                         int count =getCommand().getTargetCount(count_StoreLocator_results);
                                         String countIs = String.valueOf(count);
                                         Assert.assertEquals(storeLocatorText.toString(),countIs , "Store locator count is not matching with store locator results");
                                         
                                         List<WebElement> storeLocator_results = getCommand().driver.findElements(By.xpath("//*[@id='store-locator__results']/ul/li"));
                                         int count_results = storeLocator_results.size();
                                         
                                         for (int i = 1; i <= count_results; i++)
                                         {
                                                         
                                                         resultName= getCommand().driver.findElement(By.xpath("//*[@id='store-locator__results']/ul/li["+i+"]/a")).getText();
                                                         log("Store results name is: "+ resultName  ,LogType.STEP);
                                                         StoreLocationResults.add(resultName);
                                                         
                                         }
                                                                                         
                         
                         }
                         else
                         {
                                         log("Store is not within the radius of your location",LogType.STEP);
                                         Assert.fail("Unable to find a store within the radius");
                         }
                         
                                         
         }catch(Exception ex)
                         {
                                         Assert.fail(ex.getMessage());
                         }
                         return this;
			
	}
	
	
	// verify Find a store functionality
			public Kohler_GeneralNavigation VerifyFindAStore_mobile()
			{
				 try
		         {
		                         getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
		                         List<String> StoreLocationResults = new ArrayList<String>();
		                         String resultName=null;
		                         log("Verify Find a Store link" ,LogType.STEP);
		                        
		                         WebElement element=getCommand().driver.findElement(By.xpath("(//*[@class='mobile-sign-in__link'])[1]"));
		               	        JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
		               	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
		               	       Thread.sleep(3000);
		                            
		               	       getCommand().click(Kohler_HomePage.link_findStore);
		                           
		                         log("Verify the Find a Store Page Title",LogType.STEP);
		                         System.out.println(getCommand().driver.getTitle());
		                      Assert.assertEquals(getCommand().driver.getTitle() ,"Results | Find a Store | KOHLER","Home Page Title mismatch");
		                         
		                         log("Verify store locator text" ,LogType.STEP);
		                         
		                         getCommand().isTargetVisible(text_NoStoreLocator_mobile);
		                         String storeLocatorText = getCommand().getText(text_NoStoreLocator_mobile);
		                         System.out.println("store text..."+storeLocatorText);
		                         String expectedStoreLocText= "We are sorry, but we have no stores within 250 miles of your location. Please try another search.";
		                         
		                       //  Assert.assertEquals(expectedStoreLocText, storeLocatorText);
		                         
		                      if(!storeLocatorText.equalsIgnoreCase(expectedStoreLocText))
		                         {
		                        	 System.out.println("inside if");
		                                         log("Store is within the radius of your location",LogType.STEP);
		                                         String StoreLocator_text = getCommand().getText(text_StoreLocator_num) + getCommand().getText(text_StoreLocator_result) + " for " + getCommand().getText(text_StoreLocator_location);
		                                         log("Store is within the radius and results are: "+ StoreLocator_text ,LogType.STEP);
		                                         getCommand().isTargetVisible(count_StoreLocator_results);
		                                         Assert.assertEquals(getCommand().getText(text_StoreLocator_num).toString(), getCommand().getTargetCount(count_StoreLocator_results), "Store locator count is not matching with store locator results");
		                                         
		                                         List<WebElement> storeLocator_results = getCommand().driver.findElements(By.xpath("//*[@id='store-locator__results']/ul/li"));
		                                         int count_results = storeLocator_results.size();
		                                         
		                                         for (int i = 0; i < count_results; i++)
		                                         {
		                                                            resultName= getCommand().driver.findElement(By.xpath("//*[@id='store-locator__results']/ul/li["+i+"]/a")).getText();
		                                                         log("Store results name is: "+ resultName  ,LogType.STEP);
		                                                         StoreLocationResults.add(resultName); 
		                                                         
		                                         }
		                                                                         
		                         
		                         }
		                         else
		                         {
		                                         log("Store is not within the radius of your location",LogType.STEP);
		                         }
		                         
		                         log("verif serach gain link in find store page ",LogType.STEP);
		                         
		                         getCommand().isTargetVisible(search_again_link);
		                         
		                         getCommand().click(search_again_link);
		                         getCommand().waitFor(2);
		                         log("Input a value in search box to find a store location" ,LogType.STEP);
		                         getCommand().isTargetVisible(textbox_FindaStore);
		                         getCommand().clear(textbox_FindaStore);
		                         getCommand().sendKeys(textbox_FindaStore, "ohio");
		                         getCommand().isTargetVisible(button_FindaStore);
		                         getCommand().click(button_FindaStore);
		                         Thread.sleep(3000);
		                         
		                         log("Verify store locator text" ,LogType.STEP);
		                         getCommand().isTargetVisibleAfterWait(storelocator_number);
		                         storeLocatorText = getCommand().getText(storelocator_number);
		                         
		                        String  storeLocatorText_number = getCommand().getText(storelocator_number_only);
		                         
		                         System.out.println(storeLocatorText);
		                         
		                       
		                         
		                     int total_store_locator_count= getCommand().waitForTargetPresent(storelocator_total_count).getTargetCount(storelocator_total_count);
		                         
		                      //   Assert.assertEquals(storeLocatorText, total_store_locator_count);
		                       //  log("Verify store locator count number "+storeLocatorText+" matched with diplayed stores count "+total_store_locator_count ,LogType.STEP);
		                         
		                        
		                         if(!storeLocatorText.equalsIgnoreCase(expectedStoreLocText))
		                         {
		                                         log("Store is within the radius of your location",LogType.STEP);
		                                        // String StoreLocator_text = storeLocatorText +" "+ getCommand().getText(text_StoreLocator_result) + " for " + getCommand().getText(text_StoreLocator_location);
		                                      //   log("Store is within the radius and results are: "+ StoreLocator_text ,LogType.STEP);

		                                         getCommand().isTargetVisible(count_StoreLocator_results);
		                                         int count =getCommand().getTargetCount(count_StoreLocator_results);
		                                         String countIs = String.valueOf(count);
		                                         Assert.assertEquals(storeLocatorText_number.toString(),countIs ,"Store locator count is not matching with store locator results");
		                                         log("verify store locator count "+countIs+" and List count "+storeLocatorText_number,LogType.STEP);
		                                         List<WebElement> storeLocator_results = getCommand().driver.findElements(By.xpath("//*[@id='store-locator__results']/ul/li"));
		                                         int count_results = storeLocator_results.size();
		                                         
		                                         for (int i = 1; i <= count_results; i++)
		                                         {
		                                                         
		                                                         resultName= getCommand().driver.findElement(By.xpath("//*[@id='store-locator__results']/ul/li["+i+"]/a")).getText();
		                                                         log("Store results name is: "+ resultName  ,LogType.STEP);
		                                                         StoreLocationResults.add(resultName);
		                                                         
		                                         }
		                                                                                         
		                         
		                         }
		                         else
		                         {
		                                         log("Store is not within the radius of your location",LogType.STEP);
		                                         Assert.fail("Unable to find a store within the radius");
		                         }
		                         
		                                         
		         }catch(Exception ex)
		                         {
		                                         Assert.fail(ex.getMessage());
		                         }
		                         return this;
					
			}
	
	
	

	// verify bathroom Product Buying guide and Article page
	public Kohler_GeneralNavigation VerifyKitchenProductBuyingGuide()
	{
		 try 
         {
                         getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
                         getCommand().waitForTargetPresent(Kohler_HomePage.kitchen_btn).click(Kohler_HomePage.kitchen_btn);
                         getCommand().waitFor(2);
                         List<String> ShareLinkTextItems = new ArrayList<String>();
                         String Share_linkText=null;
                         log("Verify Product Buying Guide link" ,LogType.STEP);
         
                         if(getCommand().isTargetVisible(kitchen_Menu))
                                         Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
                         
                         log("Verify Article page on clicking on PBG link" ,LogType.STEP);
                        
                         
                         
                         
                         WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/a[4]"));
               	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
               	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
               	       Thread.sleep(3000);
                        
               	       getCommand().click(Link_KitchenProductBuyingGuide);
                         System.out.println(getCommand().getPageUrl());
                      
                    
                         
                         boolean condtion= true;
                         log("Verify the Article Page URL",LogType.STEP);
                         String URL = getCommand().driver.getCurrentUrl();
                         if(URL.contains("article"))
                         Assert.assertTrue(condtion);
                         
                         List<WebElement> buttons = getCommand().driver.findElements(By.xpath("//*[@id='article-page']//following::div[@class='article-page__icon-container']/button/i"));
                         //int button_count = buttons.size();
                         for(WebElement button: buttons)
                         {
                                         String text_button = button.getAttribute("class");
                                         String text = text_button.replaceFirst("icon--", "");
                                         
                                         switch (text)
                                         {
                                         case "bookmark":
                                                         
                                                         button.click();
                                                         getCommand().isTargetVisible(Header_SaveToFolder);
                                                         System.out.println(getCommand().getText(Header_SaveToFolder));
                                                         Assert.assertEquals(getCommand().getText(Header_SaveToFolder),"SAVE TO MY FOLDERS", "Application is not navigated to Save to folder window");
                                                         
                                                         getCommand().isTargetVisible(Kohler_HomePage.addToFolderTextArea);
                                                         getCommand().sendKeys(Kohler_HomePage.addToFolderTextArea, "Test");
                                                         
                                                         getCommand().isTargetVisible(Kohler_HomePage.addToFolderSubmitButton);
                                                         getCommand().click(Kohler_HomePage.addToFolderSubmitButton);
                                                         Thread.sleep(2000);
                                                         //getCommand().wait(5);
                                                         
                                                         if(getCommand().isTargetVisible(Win_ShowThankYou))
                                                                         Assert.assertTrue(true, "Thank you page is expanded successfully");
                                                         
                                                         getCommand().isTargetVisible(title_ItemAddedToFold1);
                                                         String title1= getCommand().getText(title_ItemAddedToFold1);
                                                         
                                                         getCommand().isTargetVisible(title_ItemAddedToFold2);
                                                         String title2= getCommand().getText(title_ItemAddedToFold2);
                                                         
                                                         String title = title1+" "+title2;
                                                         Assert.assertEquals(title,"Your item was successfully added to My Kohler Folder", "Application is not navigated to My Kohler folder");
                                                         
                                                         getCommand().isTargetVisible(button_ItemAddedToFoldClose);
                                                         getCommand().click(button_ItemAddedToFoldClose);
                                                         
                                                         break;
                                                         
                                         case "share":
                                                         button.click();
                                                         getCommand().isTargetVisible(Items_shareItems);
                                                         List<WebElement> shareLinks = getCommand().driver.findElements(By.xpath("//*[@class='share-tip organic-namespace olbox-container']/div/ul/li/a"));
                                                         for(WebElement linkText: shareLinks)
                                                         {
                                                                         Share_linkText = linkText.getText();
                                                                         log("Share options are : " + Share_linkText,LogType.STEP);
                                                         }
                                                         ShareLinkTextItems.add(Share_linkText);
                                                         
                                                         getCommand().isTargetVisible(Items_shareItemFacebook);
                                                         getCommand().click(Items_shareItemFacebook);
                                                         getCommand().waitFor(3);
                                                         
                                                         ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
                                                         getCommand().driver.switchTo().window(tabs.get(1));
                                             log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
                                             int Win_size = getCommand().driver.getWindowHandles().size();
                                             Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
                                             
                                             log("Close the Child Window: "+getCommand().driver.getTitle(),LogType.STEP);
                                             getCommand().driver.close();
                                         getCommand().driver.switchTo().window(tabs.get(0));
                                                         
                                                         break;                                   
                                      
                                                         
                                         }
                                         
                                         }

         }catch(Exception ex)
         {
                         Assert.fail(ex.getMessage());
         }
         return this;
	}

	// verify Bathroom expansion and bathroom links
	public Kohler_GeneralNavigation VerifyIdeasMainMenu()
	{
		
		try {			    
				 getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
	             getCommand().waitForTargetPresent(Kohler_HomePage.ideas_btn).click(Kohler_HomePage.ideas_btn);
	             getCommand().waitFor(2);
				//getCommand().wait(5);
			
				if(getCommand().isTargetVisible(Link_ideasMainMenu))
					Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
				
				getCommand().isTargetVisible(ideas_Link);
				
				int Win_size = getCommand().driver.getWindowHandles().size();
				log("Open Windows Size is :" + Win_size ,LogType.STEP);
				
			
				String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 				
				getCommand().sendKeys(ideas_Link, selectLinkOpeninNewTab);				
				getCommand().waitFor(2);
				
				ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
				getCommand().driver.switchTo().window(tabs2.get(1));
			    String pageTitle = getCommand().driver.getTitle();
			    Win_size = getCommand().driver.getWindowHandles().size();
			    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
			    
			    //System.out.println("Page title is:" + pageTitle);
			    log("Page title is:" + pageTitle,LogType.STEP);
			    Assert.assertEquals(pageTitle, "The Bold Look Of Kohler", "Page is not navigated to bathroom Page");
			    		    
			    getCommand().driver.close();
			    getCommand().driver.switchTo().window(tabs2.get(0));
			
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		
		return this;
	}

	// Verify Ideas kitchen Planner link functionality+ new Window
	public Kohler_GeneralNavigation verifyIdeasKitchenPlanner()
	{
		 try
         {
                         log("Verify Ideas Kitchen Planner link" ,LogType.STEP);
                         getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
                         getCommand().waitForTargetPresent(Kohler_HomePage.ideas_btn).click(Kohler_HomePage.ideas_btn);
                         getCommand().waitFor(2);
                         //getCommand().wait(5);
         
                         if(getCommand().isTargetVisible(Link_ideasMainMenu))
                                         Assert.assertTrue(true, "Ideas Kitchen main menu is expanded successfully");
                         
                         int Win_size = getCommand().driver.getWindowHandles().size();
                         log("Open Windows Size is :" + Win_size ,LogType.STEP);
                         
                         WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='sub-nav-tab--ideas']/div/div/div[1]/a[5]"));
                  	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
                  	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
                  	       Thread.sleep(3000);
                         getCommand().click(Link_IdeaskitchenPlanner);
                         getCommand().waitFor(10);
                         
                         ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
                         getCommand().driver.switchTo().window(tabs.get(1));
             log("Page title is: " + getCommand().driver.getTitle(),LogType.STEP);
             Win_size = getCommand().driver.getWindowHandles().size();
             Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
             
             log("Close the Child Window: "+getCommand().driver.getTitle(),LogType.STEP);
             getCommand().driver.close();
         getCommand().driver.switchTo().window(tabs.get(0));
                                         
                         
         }catch(Exception ex)
         {
                         Assert.fail(ex.getMessage());
         }
         return this;

	
		
	}
	
	// verify Bathroom menu links and link navigation
	public Kohler_GeneralNavigation verifyIdeasSubMenuLinks()
	{
		
		try{
			
			log("Verify Ideas sub menu items links" ,LogType.STEP);

			getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
			getCommand().waitForTargetPresent(Kohler_HomePage.ideas_btn).click(Kohler_HomePage.ideas_btn);
	     	getCommand().waitFor(2);

			//getCommand().wait(5);
		
			if(getCommand().isTargetVisible(Link_ideasMainMenu))
				Assert.assertTrue(true, "Ideas main menu is expanded successfully");
			
			List<WebElement> Ideas_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--ideas']/div/div/div[1]/a"));
		    int link_count = Ideas_SubMenuLinks.size();
		    
		    for (int i = 1; i <= link_count; i++)
		    {	
				    	String subMenuLink_Text=getCommand().driver.findElement(By.xpath("//*[@id='sub-nav-tab--ideas']/div/div/div[1]/a["+i+"]")).getText();
				    	System.out.println(subMenuLink_Text);
				    	if(subMenuLink_Text.contains("Mood Boards")||subMenuLink_Text.contains("Home Tours")||subMenuLink_Text.contains("Color")) {
				    		log("verify the link text : "+  subMenuLink_Text,LogType.STEP);
				    		Target ideas_SubMenuLinks =new Target("ideas_SubLinks","//*[@id='sub-nav-tab--ideas']/div/div/div[1]/a["+i+"]",Target.XPATH);
				    		
				    		
				    			log("click on each link and verify in new tab",LogType.STEP);
				    			getCommand().sendKeys(ideas_SubMenuLinks, Keys.chord(Keys.CONTROL,Keys.RETURN));
				    		
								getCommand().waitFor(2);
								ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
								getCommand().driver.switchTo().window(tabs2.get(1));
								
							    int Win_size = getCommand().driver.getWindowHandles().size();
							    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
							    
								String pageTitle = getCommand().driver.getTitle();
								log("verify the Page title: "+ pageTitle,LogType.STEP);
								
								getCommand().driver.close();
								log("Close the tab and switch back to parent window",LogType.STEP);
								getCommand().driver.switchTo().window(tabs2.get(0));
				    		
							
				    	}
				    		
				    	}
		    	
		  
			
			
			
		    
		}
		
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// Verify HelpUsToImproveMore link functionality
	public Kohler_GeneralNavigation VerifyKitchenHelpUsToImproveMore(String text, String feedback)
	{
		log("Click on HelpusToImproveMore",LogType.STEP);
		try {


						getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
                         getCommand().waitForTargetPresent(Kohler_HomePage.kitchen_btn).click(Kohler_HomePage.kitchen_btn);
                         getCommand().waitFor(2);
				log("verify Kitchen Main menu",LogType.STEP);
				
				//getCommand().wait(5);
		
				if(getCommand().isTargetVisible(kitchen_Menu))
					Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
			
				getCommand().waitForTargetPresent(kitchen_Link);
				getCommand().click(kitchen_Link);
				
				String pageTitle = getCommand().driver.getTitle();
			    System.out.println("Page title is:" + pageTitle);
			    log("Page title is:" + pageTitle,LogType.STEP);
			    
			    
				log("Click on HelpUsToImproveMore link",LogType.STEP);
				
				  WebElement element=getCommand().driver.findElement(By.xpath("//*[@id=\"section\"]/div[11]/div/div[2]/div/div/div[2]"));
           	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
           	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
           	       Thread.sleep(3000);
           	       getCommand().click(Link_HelpUsToImproveMore);
           	       


				
				ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
				getCommand().driver.switchTo().window(tabs2.get(1));
			    int Win_size = getCommand().driver.getWindowHandles().size();
			    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
			    
			    getCommand().waitFor(5);
			    pageTitle = getCommand().driver.getTitle();
			    //System.out.println("Page title is:" + pageTitle);
			    log("Page title is:" + pageTitle,LogType.STEP);
			    Assert.assertEquals(pageTitle, "Kohler US Study", "Title mismatch");
			    
			    log("select the type of feedback" , LogType.STEP);
			    
			    switch (feedback) {
				case "Suggestion": 
					 	getCommand().waitForTargetVisible(btn_suggestion);
					    getCommand().click(btn_suggestion);
					break;
				case "Dislike": 
				 	getCommand().waitForTargetVisible(btn_dislike);
				    getCommand().click(btn_dislike);
				    break;
				case "Praise": 
				 	getCommand().waitForTargetVisible(btn_praise);
				    getCommand().click(btn_praise);
				    break;

				default:
					break;
				}
			    
			    
			    log("Give the Comment in the text box" , LogType.STEP);
                getCommand().sendKeys(text_commenttextbox,text);
                getCommand().scrollTo(Kohler_HomePage.arrow_Forward).click(Kohler_HomePage.arrow_Forward);
                
                log("Select the rating based on feedback" , LogType.STEP);
                getCommand().waitForTargetVisible(btn_Rating);
                getCommand().click(btn_Rating);
                getCommand().waitFor(2);
                getCommand().scrollTo(Kohler_HomePage.arrow_Forward).click(Kohler_HomePage.arrow_Forward);
                getCommand().waitFor(5);
              
                log("Click on Close the window button after submitting the feedback" , LogType.STEP);
                getCommand().waitForTargetVisible(btn_closeTheWind);
                getCommand().click(btn_closeTheWind);
                
                log("Switch back to the main window from the Feedback window" , LogType.STEP);
                getCommand().driver.switchTo().window(tabs2.get(0));
			    Assert.assertEquals(getCommand().driver.getTitle() ,"Kitchen | KOHLER", "Home Page Title mismatch");
			    	
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	// Verify Newsletter Sign Up link functionality
	public Kohler_GeneralNavigation VerifyNewsLetterSignUpLink(String testData)
		{
			
			
			try {
				NewsLetterSignUp newsLetterSignUp = NewsLetterSignUp.fetch(testData);
			       
			       System.out.println("email.."+newsLetterSignUp.EmailId);
			       
			     
				      
			    	   String emailID = newsLetterSignUp.EmailId;
			    	   System.out.println("email.."+emailID);
				       String firstName = newsLetterSignUp.FirstName;
				       String lastName = newsLetterSignUp.LastName;
				       String postalCode = newsLetterSignUp.PostCode;
				       String country = newsLetterSignUp.Country;
				       String occupation = newsLetterSignUp.Occupation;
				       
				       log("verify Newsletter Sign Up link",LogType.STEP);
				      
				       WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='curalate-fan-reel']/div[4]/a"));
				       JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
				       jse.executeScript("arguments[0].scrollIntoView(true);", element);
				       Thread.sleep(4000);
				       
				       
			           getCommand().waitFor(3);
			           
				       getCommand().click(our_Company);
				       getCommand().waitFor(3);
				       getCommand().scrollTo(link_Careers);
				       
				       getCommand().waitForTargetPresent(link_NewsLetterSignUp).click(link_NewsLetterSignUp);
				       
				       getCommand().waitFor(10);
				       ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
				       getCommand().driver.switchTo().window(tabs2.get(1));
				       
				       int Win_size = getCommand().driver.getWindowHandles().size();
				    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
				    
				       String pageTitle = getCommand().driver.getTitle();
				       log("verify the Page title"+ pageTitle,LogType.STEP);
				       
				       log("Input the values to subscribe to News letter",LogType.STEP);
				       getCommand().isTargetVisible(text_Email);
				       getCommand().sendKeys(text_Email, emailID);
				       getCommand().isTargetVisible(text_FirstName);
				       getCommand().sendKeys(text_FirstName, firstName);
				       getCommand().isTargetVisible(text_LastName);
				       getCommand().sendKeys(text_LastName, lastName);
				       
				       getCommand().scrollTo(select_Country);
				       getCommand().isTargetVisible(text_PostalCode);
				       getCommand().sendKeys(text_PostalCode, postalCode);
				       
				       getCommand().isTargetVisible(select_Country);
				       getCommand().selectDropDown(select_Country, Integer.parseInt(country));
				       
				       getCommand().isTargetVisible(select_occupation);
				       getCommand().selectDropDown(select_occupation, Integer.parseInt(occupation));
				       
				       log("Click on newletter that you want to receive",LogType.STEP);
				       getCommand().scrollTo(button_interiorProducts);
				       getCommand().click(button_interiorProducts);
				       
				       
				       if(getCommand().isTargetVisible(Div_ExpandedFields))
				              Assert.assertTrue(true, "Div is expanded successfully");
				       
				       getCommand().isTargetVisible(checkbox_interiorProducts);
				       getCommand().click(checkbox_interiorProducts);
				       
				       getCommand().scrollTo(button_Submit);
				       getCommand().click(button_Submit);
				       
				       getCommand().isTargetPresentAfterWait(text_thankyou, 15);
				       System.out.println(getCommand().getText(text_thankyou));
				       log("On clicking Submit button: "+ getCommand().getText(text_thankyou),LogType.STEP);
				       
				       getCommand().driver.close();
				       log("Close the tab and switch back to parent window",LogType.STEP);
				       getCommand().driver.switchTo().window(tabs2.get(0));
				        
					
				
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			
			return this;
		}


	// verify Parts expansion and Parts links
	public Kohler_GeneralNavigation VerifyPartsMainMenu()
	{
			
			try {		
				
						String text=null;
						log("verify Parts Main menu",LogType.STEP);
						getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
	                    getCommand().waitForTargetPresent(Kohler_HomePage.parts_btn).click(Kohler_HomePage.parts_btn);
	                     getCommand().waitFor(2);
						//getCommand().wait(5);
					
						if(getCommand().isTargetVisible(Link_PartsMainMenu))
							Assert.assertTrue(true, "Parts main menu is expanded successfully");
						
						log("Click on Parts Link",LogType.STEP);
						getCommand().isTargetVisible(Link_PartsPage);
						getCommand().click(Link_PartsPage);
						
						log("On clicking on Parts link, Parts Page should be displayed",LogType.STEP);
						Assert.assertEquals(getCommand().driver.getTitle(), "Maintenance & Replacement Parts | KOHLER", "Not landed in Parts maintainence Page");
						
						
						log("click on Wizard",LogType.STEP);
						getCommand().isTargetVisible(Link_PartsWizard);
						getCommand().click(Link_PartsWizard);
						Thread.sleep(3000);
						
						List<WebElement> divs = getCommand().driver.findElements(By.xpath("//div[@class='product-finder']/div"));
						
						for (int i = 1; i < divs.size(); i++)
						{
							text = getCommand().driver.findElement(By.xpath("//div[@class='product-finder']/div["+i+"]/div/h3")).getText();
							log("get the text of Parts: "+ text ,LogType.STEP);
							
							
							if(text.contains("ROOM"))
							{
								getCommand().driver.findElement(By.xpath("//*[@id='question-room']/div[2]/ul/li[1]")).click();
								boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-room']/div[1]/figure")).isDisplayed();
								System.out.println(status);
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								Thread.sleep(10000);
							}
							else if (text.contains("PRODUCT"))
							{
								getCommand().driver.findElement(By.xpath("//*[@id='question-product-type']/div[2]/ul/li[1]")).click();
								boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-product-type']/div[1]/figure")).isDisplayed();
								System.out.println(status);
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								Thread.sleep(10000);
							}
								
							else if (text.contains("FEATURE"))
							{
								getCommand().driver.findElement(By.xpath("//*[@id='feature-type']/ul/li[1]")).click();
								boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-features']/div[1]/figure")).isDisplayed();
								System.out.println(status);
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								Thread.sleep(10000);
							}
							
						}
							
							log("verify the count on results button : ",LogType.STEP);
							getCommand().isTargetVisible(button_ViewResultsCount);
							String countIs = getCommand().getText(button_ViewResultsCount);
							
							
							getCommand().isTargetVisible(button_ViewResults);
							getCommand().click(button_ViewResults);
							
							getCommand().isTargetVisible(Count_Results);
							String count = getCommand().getText(Count_Results);
							String TotalCountIs = "";
							TotalCountIs = count.substring(0, 3);
							
							Assert.assertEquals(countIs, TotalCountIs,"Results are not the same");

				}catch(Exception ex)
				{
					Assert.fail(ex.getMessage());
				}
				
				
				return this;
			}

	
	// verify Parts expansion and Parts links
	public Kohler_GeneralNavigation VerifyPartsMainMenu_mobile()
	{
		System.out.println("enteri  into VerifyPartsMainMenu_mobile()...");
		
			
			try {		
				
				  getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);	
				
				  getCommand().waitFor(2);
				
				  getCommand().waitForTargetPresent(hamburegr_parts_link).click(hamburegr_parts_link);	
				  getCommand().waitFor(2);
				  
				  if(getCommand().isTargetVisible(hamburegr_parts_submenus))
						Assert.assertTrue(true, "Parts main menu is expanded successfully");
				  log("Click on Parts Link",LogType.STEP);
				  getCommand().waitForTargetPresent(hamburegr_parts_findparts).click(hamburegr_parts_findparts);	
					
						String text=null;
						log("verify Parts Main menu",LogType.STEP);
					//	getCommand().waitForTargetPresent(Link_Parts);
					//	getCommand().click(Link_Parts);
						//getCommand().wait(5);
					
						//	getCommand().isTargetVisible(Link_PartsPage);
					//	getCommand().click(Link_PartsPage);
						
						log("On clicking on find Parts link, Parts Page should be displayed",LogType.STEP);
						Assert.assertEquals(getCommand().driver.getTitle(), "Maintenance & Replacement Parts | KOHLER", "Not landed in Parts maintainence Page");
						
						
						log("click on Wizard",LogType.STEP);
						getCommand().isTargetVisible(parts_wizard);
						getCommand().click(parts_wizard);
						Thread.sleep(3000);
						
						List<WebElement> divs = getCommand().driver.findElements(By.xpath("//div[@class='product-finder']/div"));
						
						for (int i = 1; i < divs.size(); i++)
						{
							System.out.println("i value..."+i);
							text = getCommand().driver.findElement(By.xpath("//div[@class='product-finder']/div["+i+"]/div/h3")).getText();
							log("get the text of Parts: "+ text ,LogType.STEP);
							
							
							if(text.contains("ROOM"))
							{
								System.out.println("room");
								getCommand().driver.findElement(By.xpath("//*[@id='question-room']/div[2]/ul/li[1]")).click();
								boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-room']/div[1]/span[1]")).isDisplayed();
								System.out.println(status);
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								Thread.sleep(10000);
							}
							else if (text.contains("PRODUCT"))
							{
								System.out.println("PRODUCT");
								
								getCommand().driver.findElement(By.xpath("//*[@id='question-product-type']/div[2]/ul/li[4]")).click();
								boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-product-type']/div[1]/span[1]")).isDisplayed();
								System.out.println(status);
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								Thread.sleep(10000);
								
								//*[@id="question-room"]/div[1]/figure
								
								
							}
								
							else if (text.contains("FEATURE"))
							{
								System.out.println("FEATURE");
								getCommand().driver.findElement(By.xpath("//div[@id='feature-type']//ul/li[4]")).click();
							
								//in Mobile 3rd feature not showing as selected ->need info
								
								/*boolean status = getCommand().driver.findElement(By.xpath("//*[@id='question-features']/div[1]/figure")).isDisplayed();
								System.out.println(status);
								log("verify if item is selected and displayed : "+ status ,LogType.STEP);
								Thread.sleep(10000);*/
								
                                Thread.sleep(10000);
								
							}
							
						}
							
							
							String get_count_text=getCommand().waitForTargetVisible(count_visible).getText(count_visible);
							
							log("verify the count on header text  : "+get_count_text,LogType.STEP);
							
							
							System.out.println("get count text..."+get_count_text);
							
							
							String count_get[]= get_count_text.split(" ");
							
							for (String temp: count_get){
							      System.out.println(temp);
							}
							  
							
							int result_count = Integer.parseInt(count_get[0]);
							
							System.out.println("product count is..."+count_get[0]);
						//	getCommand().isTargetVisible(button_ViewResultsCount);
							//String countIs = getCommand().getText(button_ViewResultsCount);
							
							
							int total_displayed_product_count=getCommand().waitForTargetPresent(total_count_inapp).getTargetCount(total_count_inapp);
							
							
							/*getCommand().isTargetVisible(button_ViewResults);
							getCommand().click(button_ViewResults);
							
							getCommand().isTargetVisible(Count_Results);
							String count = getCommand().getText(Count_Results);
							String TotalCountIs = "";
							TotalCountIs = count.substring(0, 3);
							
							Assert.assertEquals(countIs, TotalCountIs,"Results are not the same");
							*/
							
					
							
							Assert.assertEquals(result_count,total_displayed_product_count,"displayed Product Count matched");
							
							log("verify the displayed count  : "+total_displayed_product_count,LogType.STEP);
							
							log("verify the displayed count and header results count : "+result_count+"......" +total_displayed_product_count,LogType.STEP);
							
							
							

				}catch(Exception ex)
				{
					Assert.fail(ex.getMessage());
				}
				
				
				return this;
			}
	
	
	
	// verify Find a Pro Functionality with invalid Zip code
	public Kohler_GeneralNavigation VerifyFindAProInvalidZipCode(String Testdata)
	{
		SearchData search = SearchData.fetch(Testdata);
		
		try {		
			
			String value_pin = search.PinCode; 
			System.out.println("pincode...."+value_pin);
			ArrayList<String> elements_value = new ArrayList<String>();
			log("verify Find a pro functionality",LogType.STEP);
			  getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
              getCommand().waitForTargetPresent(Kohler_HomePage.link_findPro).click(Kohler_HomePage.link_findPro);
               getCommand().waitFor(5);
			
			String title = getCommand().driver.getTitle();
			Assert.assertEquals(title, "Find a Pro | KOHLER");
			
			getCommand().scrollTo(Ele_CustomerSupport);
			
			Assert.assertTrue(getCommand().isTargetVisible(Ele_CustomerSupport),"Element is not displayed in Find a Pro Page");
			getCommand().scrollTo(Ele_NeedHelpWith);
			
			
			Assert.assertTrue(getCommand().isTargetVisible(Ele_NeedHelpWith), "Element is not displayed in Find a Pro Page");
			
			getCommand().isTargetVisible(EleValue_NeedHelpWith);
			String val = getCommand().getText(EleValue_NeedHelpWith);
			 //getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='col-6-lg col-12-md col-block col-block-accordion']/h3")).getText();
			
			String expect = "I NEED HELP WITH";
			if(val.equals(expect))
			{
				List<WebElement> inner_Ele = getCommand().driver.findElements(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']/button/header/h3"));
				int count = inner_Ele.size();
				
				for (int i = 0; i < count; i++)
				{
					log("verify element text in Need help : "+ inner_Ele.get(i).getText(),LogType.STEP);
					//System.out.println(inner_Ele.get(i).getText());
					elements_value.add(inner_Ele.get(i).getText());
				}
				
				
				for (int i = 0; i < count; i++)
				{
					inner_Ele = getCommand().driver.findElements(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']/button/header/h3"));
					
					if(!inner_Ele.get(i).getText().equals("BATHROOM DESIGN HELP"))
					{
						int x = i+1;
						getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']["+x+"]/button/header/h3")).click();
						if(x==3 || x==4)
							x= x-1;
						getCommand().driver.findElement(By.xpath("//label[@class='input__label']//following::input[@class='input__value']["+x+"]")).sendKeys(value_pin);
						getCommand().driver.findElement(By.xpath("//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']["+x+"]")).click();
						
						log("verify the landing page Title : " +getCommand().driver.getTitle() ,LogType.STEP);
						//System.out.println(getCommand().driver.getTitle());
						getCommand().isTargetVisible(text_FindAProErrorMsg);
						
						log("verify the text msg in landing page " + getCommand().getText(text_FindAProErrorMsg),LogType.STEP);
						String text=getCommand().getText(text_FindAProErrorMsg);
					//	Assert.assertEquals(getCommand().getText(text_FindAProErrorMsg).contains("We have no professional partners for the zip code you entered."),"Text mimatches");
						//System.out.println(getCommand().getText(text_FindAProErrorMsg));
						
						
						if(text.contains("We have no professional partners for the zip code you entered."))
						{
							log("verify the text  " + text,LogType.STEP);
						}
						else
						{
							//Assert.assertEquals(getCommand().getText(text_FindAProErrorMsg).contains("We have no professional partners for the zip code you entered."),"Text matched");
							log("Text not matched and the actual text is:  " + text,LogType.STEP);
						}
						getCommand().isTargetVisible(Link_GoBack);
						getCommand().click(Link_GoBack);
						
						
					}
					else
					{
						inner_Ele.get(i).click();
						
						getCommand().isTargetVisible(button_getDesignHelp);
						getCommand().click(button_getDesignHelp);
												
						log("verify landing page title : "+ getCommand().driver.getTitle(),LogType.STEP);
						//System.out.println(getCommand().driver.getTitle());
						getCommand().driver.navigate().back();
						
					}
					
					
				}
				
				log("verify landing page of customer support",LogType.STEP);
				getCommand().isTargetVisible(button_CustSupport);
				getCommand().click(button_CustSupport);
				log("verify landing page title : "+ getCommand().driver.getTitle(),LogType.STEP);
				//System.out.println(getCommand().driver.getTitle());
				getCommand().driver.navigate().back();
			}


			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
	
	
	return this;
	}
	
	// verify Find a Pro Functionality with valid Zip code
	public Kohler_GeneralNavigation VerifyFindAPro(String ProductInst , String BathHome)
	{
		SearchData SearchData1 = SearchData.fetch(ProductInst);
		SearchData SearchData2 = SearchData.fetch(BathHome);
		try {		
			
			String ProductInstvalue = SearchData1.PinCode; 
			String BathRoomHomeCons = SearchData2.PinCode;
			
			log("verify Find a pro functionality",LogType.STEP);
			  getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
              getCommand().waitForTargetPresent(Kohler_HomePage.link_findPro).click(Kohler_HomePage.link_findPro);
               getCommand().waitFor(5);
			
			String title = getCommand().driver.getTitle();
			Assert.assertEquals(title, "Find a Pro | KOHLER");
			
			Assert.assertTrue(getCommand().isTargetVisible(Ele_NeedHelpWith), "Element is not displayed in Find a Pro Page");
			
			Assert.assertTrue(getCommand().isTargetVisible(Ele_CustomerSupport),"Element is not displayed in Find a Pro Page");
			
			List<WebElement> inner_Ele = getCommand().driver.findElements(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']/button/header/h3"));
			int count = inner_Ele.size();
					
			for (int i = 0; i < count; i++)
			{
					inner_Ele = getCommand().driver.findElements(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']/button/header/h3"));
					
					if(inner_Ele.get(i).getText().equals("KOHLER PRODUCT INSTALLATION"))
					{
						int x = i+1;
						getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']["+x+"]/button/header/h3")).click();
						getCommand().driver.findElement(By.xpath("//label[@class='input__label']//following::input[@class='input__value']["+x+"]")).sendKeys(ProductInstvalue);
						getCommand().driver.findElement(By.xpath("//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']["+x+"]")).click();
						
						log("verify the landing page Title : " +getCommand().driver.getTitle() ,LogType.STEP);
						//System.out.println(getCommand().driver.getTitle());
						getCommand().isTargetVisible(text_FAPPageHeader);
						
						log("verify the text msg in landing page " + getCommand().getText(text_FAPPageHeader),LogType.STEP);
						
						Assert.assertTrue(getCommand().isTargetPresent(button_GetAnEstm),"Get an Estimation button is not visible");
						
						log("verify that view result page is displayed with address",LogType.STEP);
						if(getCommand().isTargetVisible(result_Address) && getCommand().getTargetCount(result_Address)> 0)
							Assert.assertTrue(true, "Search results are available and visible");
						
						log("verify the Get an estimation button " + getCommand().getText(text_FAPPageHeader),LogType.STEP);
						getCommand().isTargetVisible(button_GetAnEstm);
						getCommand().click(button_GetAnEstm);
						
						log("verify landing page title : "+ getCommand().driver.getTitle(),LogType.STEP);
						System.out.println(getCommand().driver.getTitle());
						
						Assert.assertEquals(getCommand().getText(text_getAnEstmHeader), "Get an Estimate", "Header message mismatches");
						
						log("Click on Go back link and it should navigate user to previous page",LogType.STEP);
						getCommand().isTargetVisible(Link_GoBack);
						getCommand().click(Link_GoBack);
						
						log("Click on back button and it should navigate user to previous page"+ getCommand().driver.getTitle(),LogType.STEP);
						getCommand().driver.navigate().back();
			
						
					}
					else if(inner_Ele.get(i).getText().equals("FULL BATHROOM REMODEL"))
					{
						int x = i+1;
						getCommand().scrollTo(Ele_NeedHelpWith);
						getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']["+x+"]/button/header/h3")).click();
						if(x==3)
							x= x-1;
						getCommand().driver.findElement(By.xpath("//label[@class='input__label']//following::input[@class='input__value']["+x+"]")).sendKeys(BathRoomHomeCons);
						getCommand().driver.findElement(By.xpath("//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']["+x+"]")).click();
						
						log("verify the landing page Title : " +getCommand().driver.getTitle() ,LogType.STEP);
						//System.out.println(getCommand().driver.getTitle());
						getCommand().isTargetVisible(text_FAPPageHeader);
						
						log("verify the text msg in landing page " + getCommand().getText(text_FAPPageHeader),LogType.STEP);
						Assert.assertTrue(getCommand().isTargetPresent(button_GetAnEstm),"Get an Estimation button is not visible");
						
						if(getCommand().isTargetVisible(result_Address) && getCommand().getTargetCount(result_Address)> 0)
							Assert.assertTrue(true, "Search results are available and visible");
												
						getCommand().isTargetVisible(button_GetAnEstm);
						getCommand().click(button_GetAnEstm);
						
						log("verify landing page title : "+ getCommand().driver.getTitle(),LogType.STEP);
						System.out.println(getCommand().driver.getTitle());
												
						Assert.assertEquals(getCommand().getText(text_getAnEstmHeader), "Get an Estimate", "Header message mismatches");
						
						log("Click on Go back link and it should navigate user to previous page",LogType.STEP);
						getCommand().isTargetVisible(Link_GoBack);
						getCommand().click(Link_GoBack);
						
						log("Click on back button and it should navigate user to previous page"+ getCommand().driver.getTitle(),LogType.STEP);
						getCommand().driver.navigate().back();
			
						
					}
					else if(inner_Ele.get(i).getText().equals("NEW HOME CONSTRUCTION"))
					{
						int x = i+1;
						getCommand().scrollTo(Ele_NeedHelpWith);
						getCommand().driver.findElement(By.xpath("//*[@id='pro-finder']//following::div[@class='accordion-item col-12-lg']["+x+"]/button/header/h3")).click();
						if(x==4)
							x= x-1;
						getCommand().driver.findElement(By.xpath("//label[@class='input__label']//following::input[@class='input__value']["+x+"]")).sendKeys(BathRoomHomeCons);
						getCommand().driver.findElement(By.xpath("//div[@class='col-3-md marg-t-10-md']//following::input[@type='submit']["+x+"]")).click();
						
						log("verify the landing page Title : " +getCommand().driver.getTitle() ,LogType.STEP);
						//System.out.println(getCommand().driver.getTitle());
						getCommand().isTargetVisible(text_FAPPageHeader);
						
						log("verify the text msg in landing page " + getCommand().getText(text_FAPPageHeader),LogType.STEP);
						
						if(getCommand().isTargetVisible(result_Address) && getCommand().getTargetCount(result_Address)> 0)
							Assert.assertTrue(true, "Search results are available and visible");
												
						if(!getCommand().isTargetVisible(button_GetAnEstm));
						Assert.assertTrue(true, "Get an Estimation Button is not visible");
						
						log("Click on Go back link and it should navigate user to previous page",LogType.STEP);
						getCommand().isTargetVisible(Link_GoBack);
						getCommand().click(Link_GoBack);
						
						
					}
				}
				
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
	
	
	return this;
	}

	
	// verify Kitchen menu links navigation and Page Title
	public Kohler_GeneralNavigation verifyKitchenSubMenuLinksPageTitles()
		{
			ArrayList<String> Pagetitles = new ArrayList<String>();
			try{
				
				   getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
                   getCommand().waitForTargetPresent(Kohler_HomePage.kitchen_btn).click(Kohler_HomePage.kitchen_btn);
                   getCommand().waitFor(2);
				//getCommand().wait(5);
			
				if(getCommand().isTargetVisible(Link_kitchenMainMenu))
					Assert.assertTrue(true, "Kitchen main menu is expanded successfully");
				
				List<WebElement> Kitchen_SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/a"));
			    int link_count = Kitchen_SubMenuLinks.size();
			    
			    for (int i = 1; i <= link_count; i++)
			    {
			    		Target Kitchen_Links =new Target("Kitchen_Links","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/a["+i+"]",Target.XPATH);
			    		
			    		String subMenu_Text=getCommand().getText(Kitchen_Links);
								System.out.println(subMenu_Text);
								log("verify the link text "+  subMenu_Text,LogType.STEP);
								
								log("click on each link and verify in new tab",LogType.STEP);
								getCommand().sendKeys(Kitchen_Links,Keys.chord(Keys.CONTROL,Keys.RETURN) );
								getCommand().waitFor(10);
								ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
								getCommand().driver.switchTo().window(tabs2.get(1));
							    int Win_size = getCommand().driver.getWindowHandles().size();
							    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
							    
								String pageTitle = getCommand().driver.getTitle();
								log("verify the Page title "+ pageTitle,LogType.STEP);
								System.out.println(pageTitle);
								Pagetitles.add(pageTitle);
												
								getCommand().driver.close();
								log("Close the tab and switch back to parent window",LogType.STEP);
								getCommand().driver.switchTo().window(tabs2.get(0));
						
			    	
			    		
			    }
				
			    int title_count = Pagetitles.size();
				
				for (int j = 0; j < title_count; j++)
				{
					String titleIs = Pagetitles.get(j);
					
				
						if(titleIs.contains("KOHLER") && titleIs.length() >=6)
						{
							log("Page title contains KOHLER",LogType.STEP);
						}
						else
						{	log("Page title is not ending with KOHLER",LogType.ERROR_MESSAGE);
							Assert.fail("Page title is not ending with KOHLER" );
						}
					
					
				
				
				
				}
			}
			
			catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			
			return this;
		}
		
	
		public Kohler_GeneralNavigation VerifyContactUs() {
			try {
				
					log("verify Contact us  functionality from hamburger menu",LogType.STEP);
					getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
					  WebElement element=getCommand().driver.findElement(By.xpath("(//*[@class='mobile-sign-in__link'])[3]"));
	               	  JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
	               	 jse.executeScript("arguments[0].scrollIntoView(true);", element);
	               	 Thread.sleep(3000);
	               	log("verify the navigation to contact us page",LogType.STEP);
	               	 getCommand().click(Kohler_HomePage.link_contactUs);
		            getCommand().waitFor(3);
		            String contactUs_Page=getCommand().getPageTitle();
		            
		         	log("Getting page tilte while clicking contact us link is: "+contactUs_Page,LogType.STEP);
		            Assert.assertEquals(contactUs_Page, "Contact Us | KOHLER","Page title mismatches");
			}
			catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
			
			return this;
		}
	
	
}

