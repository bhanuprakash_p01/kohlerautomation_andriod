package com.components.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.components.repository.SiteRepository;
import com.components.yaml.KohlerSearchData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;

public class Kohler_SearchPage extends SitePage{

	//Defining the Locators on the page
   // public static final Target search_Input  = new Target("search_Input","//*[@id='nav-searchbox']",Target.XPATH);
   // public static final Target search_btn  = new Target("search_btn","//*[@id='header__button--search-button-desktop']",Target.XPATH);
    public static final Target Inspiration  = new Target("Inspiration","//*[@id='search-hero-navigation']/a[2]",Target.XPATH);
    public static final Target Show_me_more = new Target("Show_me_more","(//*[contains(text(),'Show me more')])[1]",Target.XPATH);
    public static final Target Resource = new Target("Resource","//*[@id='search-hero-navigation']/a[4]",Target.XPATH);
    public static final Target Read_the_article = new Target("Read_the_article","(//*[contains(text(),'Read the article')])[1]",Target.XPATH);
   // public static final Target Collection  = new Target("Collection","//*[@id='product-category-content']/div[1]/div[1]/h1",Target.XPATH);
    public static final Target Collection  = new Target("Collection","#product-category-content > div.row.marg-b-20-sm.marg-t-50-sm.marg-b-65-md > div.col-8-md.marg-b-20-sm.marg-b-0-md > h1 ",Target.CSS);
    public static final Target Did_Mean_Two  = new Target("Did_Mean_Two","//*[@id='search-hero']/div/div[1]/h2[2]",Target.XPATH);
    public static final Target Article = new Target("Article", "//*[@id='product-categories']/ul/li[1]/a" , Target.XPATH);
    public static final Target Poplin_Products =new Target("Poplin_Products","//*[@id='search-results-panels']/div/a[1]/p[1]",Target.XPATH);
    public static final Target choreograph_Products =new Target ("choreograph_Products","//*[@id='search-results-panels']/div/a[1]/p[1]",Target.XPATH);
    public static final Target Link_parts = new Target("Link_parts","//button[@id= 'main-nav__button--parts']",Target.XPATH);	
    public static final Target Search_partsTerms =new Target("Search_partsTerms","(//*[@id='searchTerms'])[2]",Target.XPATH);
    public static final Target Link_partsMainMenu = new Target("Link_partsmainMenu","//*[@id=\"sub-nav-tab--parts\"]/div/div/div[2]",Target.XPATH);
    public static final Target button_PartsSearchIcon = new Target("button_PartsSearchIcon","//*[@id=\"redesignSearchPartsForm\"]/div/div[1]/span/button",Target.XPATH);
    public static final Target Kitchen_sinks = new Target("Kitchen_sinks",".//*[@id='section']/div[3]/div/div[1]/a/span",Target.XPATH);
    //---------------------------------------Mobile xpaths--------------------------------------------------------------
    public static final Target search_Icon=new Target("search_Icon","(//*[@class='icon--search'])[1]",Target.XPATH);
	public static final Target search_Input=new Target("search_Input","//*[@id='search']",Target.XPATH);
	public static final Target search_btn=new Target("search_btn","//*[@id='header__button--search-button-mobile']",Target.XPATH);
	public static final Target tops_Dropdown=new Target("tops_Dropdown","//*[@id='search-hero-select']/select",Target.XPATH);
	public static final Target findAll_parts=new Target("findAll_parts","//*[@id='sub-nav-tab--parts']/div/div/div[2]/a[1]",Target.XPATH);
	public static final Target tops_Results_cat=new Target("tops_Results_cat","//*[@id='categories-and-filters__refine-search-button']",Target.XPATH);


	
	
	public Kohler_SearchPage(SiteRepository repository)
    {
                    super(repository);
    }

    //Navigating to Kohler Search Page
    public Kohler_SearchPage _GoToSearchPage()
    {
                    log("Navigate to Kohler Search Page",LogType.STEP);
                    return this;
                    
    }
	
	// Verify Search functionality for Kitchen
	public Kohler_SearchPage VerifySearchFunctionalityKitchen()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				String PageTitle="Kitchen | KOHLER";
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchK = KohlerSearchData.fetch("SearchK");
				String SearchKFetched = SearchK.keyword;
				getCommand().sendKeys(search_Input, SearchKFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Kitchen Section Landing page ");
				log("Accessed to Kitchen Section Landing page",LogType.STEP);
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	
	// Verify Search functionality for Toilets
	public Kohler_SearchPage VerifySearchFunctionalityToilets()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchT = KohlerSearchData.fetch("SearchT");
				String SearchTFetched = SearchT.keyword;
				getCommand().sendKeys(search_Input, SearchTFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				String PageTitle="Browse Kohler Toilets | KOHLER";
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Toilets category Landing page ");
				log("Accessed to Toilets Section Landing page",LogType.STEP);
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Vanity
	public Kohler_SearchPage VerifySearchFunctionalityVanity()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchV = KohlerSearchData.fetch("SearchV");
				String SearchVFetched = SearchV.keyword;
				getCommand().sendKeys(search_Input, SearchVFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				String PageTitle="Bathroom Vanities |Bathroom | KOHLER";
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Vanity category Landing page ");
				log("Accessed to Vanity category Landing page", LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	
	// Verify Search functionality for Faucet
	public Kohler_SearchPage VerifySearchFunctionalityFaucet()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);

				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchF = KohlerSearchData.fetch("SearchF");
				String SearchFFetched = SearchF.keyword;
				getCommand().sendKeys(search_Input, SearchFFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				String PageTitle="Kohler Faucets | KOHLER";
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Faucets Families Landing page ");
				log("Accessed to Faucets Families Landing page",LogType.STEP);
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Products 14660-4-CP and K-73060-4-CP
	public Kohler_SearchPage VerifySearchFunctionalityProduct()
	{
		try {
				log("Verifying Search option in application with Product 14660-4-CP",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				getCommand().waitFor(2);
				KohlerSearchData SearchP1 = KohlerSearchData.fetch("SearchP1");
				String SearchP1Fetched = SearchP1.keyword;
				getCommand().sendKeys(search_Input, SearchP1Fetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				String PageTitle="K-14660-4 | Loure Tall Single-Control Sink Faucet | KOHLER";
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.titleContains(PageTitle));
				
				String CurrentpageTitle = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle, CurrentpageTitle, "Not accessed to Product 14660-4-CP page ");
				log("Accessed to Product 14660-4-CP page",LogType.STEP);
				
				//Verify of product K-73060-4-CP
				log("Verifying Search option in application with product K-73060-4-CP",LogType.STEP);
				getCommand().isTargetVisible(search_Input);
				getCommand().clear(search_Input);
				
				KohlerSearchData SearchP2 = KohlerSearchData.fetch("SearchP2");
				String SearchP2Fetched = SearchP2.keyword;
				getCommand().sendKeys(search_Input, SearchP2Fetched);
				
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
			
				String PageTitle1="K-73060-4 | Composed Widespread Bathroom Sink Faucet | KOHLER";
				
				WebDriverWait wait1 = new WebDriverWait(getCommand().driver, 180);
				wait1.until(ExpectedConditions.titleContains(PageTitle1));
				
				String CurrentpageTitle1 = getCommand().getPageTitle();
				
				Assert.assertEquals(PageTitle1, CurrentpageTitle1, "Not accessed to Product K-73060-4-CP page ");
				log("Accessed to Product K-73060-4-CP page ",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Arm
	public Kohler_SearchPage VerifySearchFunctionalityArm()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);

				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchAr = KohlerSearchData.fetch("SearchAr");
				String SearchArFetched = SearchAr.keyword;
				getCommand().sendKeys(search_Input, SearchArFetched);
			
				log("Enter search key in serachbox  "+SearchArFetched,LogType.STEP);
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
				
				List<WebElement> SubMenuLinks = getCommand().driver.findElements(By.xpath("//*[@id='search-hero-select']//select//option"));
				int link_count = SubMenuLinks.size();
				System.out.println("Link size is "+link_count);
				
				String[] submenusText = {"PRODUCTS","PARTS","RESOURCES","TECHNICAL","INSPIRATION"};
 
				getCommand().driver.findElement(By.xpath("//div[@id='search-hero-select']//select")).click();
				
				for (int i = 1; i < link_count; i++)
				{
						String link_text = SubMenuLinks.get(i).getText();
					System.out.println(link_text);
					getCommand().waitFor(5);
					
				//	Assert.assertEquals(link_text.contains(submenusText[i-1]), "The search keyword Arm not returned the sublinks ");
					
					if(link_text.contains(submenusText[i-1]))
					{
						System.out.println("enter into if condition");
						log("Verify search results in dropdown : "+link_text ,LogType.STEP);
					}
				} 
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Inspiration tabs
	public Kohler_SearchPage VerifyFunctionalityInspiration()
	{
		try {
			
			log("Verifying Search option in application",LogType.STEP);
			getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
			
			KohlerSearchData SearchTop = KohlerSearchData.fetch("SearchTop");
			String SearchTopFetched = SearchTop.keyword;
			getCommand().sendKeys(search_Input, SearchTopFetched);
  	      	
			log("Click on search button",LogType.STEP);
  	      	getCommand().isTargetVisible(search_btn);
  	      	getCommand().click(search_btn);
			
  	      	WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
	     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
  	      	
			String Url="ideas.kohler.com";
			log("Click on Inspiration link",LogType.STEP);
			getCommand().selectDropDown(tops_Dropdown, 4);
			getCommand().waitFor(5);
			getCommand().scrollTo(tops_Results_cat);
			getCommand().sendKeys(Show_me_more, Keys.chord(Keys.CONTROL,Keys.RETURN));
			
			ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			
		    getCommand().driver.switchTo().window(tabs.get(1));
		    WebDriverWait wait1 = new WebDriverWait(getCommand().driver, 180);
	     	wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='content col2x']/div/h1")));
		    
			String CurrentUrl=getCommand().driver.getCurrentUrl();
			
			Assert.assertTrue(CurrentUrl.contains(Url), "The Inspiration tab not redirecting to ideas.kohler.com");
			log("The Inspiration tab redirects to ideas.kohler.com",LogType.STEP);
			
			getCommand().driver.close();
			getCommand().driver.switchTo().window(tabs.get(0));
			
			getCommand().waitFor(10);
			
		}catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	// Verify Search functionality for Resource tabs
	public Kohler_SearchPage VerifyFunctionalityResource()
	{
		try {
			
			String Url="ideas.kohler.com";
			log("Click on Resource link",LogType.STEP);
			getCommand().selectDropDown(tops_Dropdown, 2);
			
			getCommand().waitFor(5);
			getCommand().scrollTo(tops_Results_cat);
  	       	getCommand().sendKeys(Read_the_article,Keys.chord(Keys.CONTROL,Keys.RETURN));
  	       	ArrayList<String> tabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			
		    getCommand().driver.switchTo().window(tabs.get(1));
  	       	
			getCommand().driver.getWindowHandle();
			String CurrentUrl=getCommand().driver.getCurrentUrl();
			
			Assert.assertTrue(CurrentUrl.contains(Url), "The Resource tab redirects to ideas.kohler.com");
			if(CurrentUrl.contains(Url)) {
				log("The results are shown from "+Url,LogType.STEP);
			}
			getCommand().driver.close();
			getCommand().driver.switchTo().window(tabs.get(0));
			
		}catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	// Verify Search functionality for Memoirs
	public Kohler_SearchPage VerifySearchFunctionalityMemoirs()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchMem = KohlerSearchData.fetch("SearchMem");
				String SearchMemFetched = SearchMem.keyword;
				getCommand().sendKeys(search_Input, SearchMemFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='product-category-content']/div[1]/div[1]/h1")));
				
				getCommand().isTargetVisible(Collection);
				String collect_text=getCommand().getText(Collection);
				
				Assert.assertTrue(collect_text.contains("Collections"), "Collection is not displayed");
				log("Collection is displayed",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search functionality for Moxie
	public Kohler_SearchPage VerifySearchFunctionalityMoxie()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchMox = KohlerSearchData.fetch("SearchMox");
				String SearchMoxFetched = SearchMox.keyword;
				getCommand().sendKeys(search_Input, SearchMoxFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.urlContains("article"));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				
				Assert.assertTrue(CurrentPageUrl.contains("article"), "Moxie Article is not displayed");
				log("Moxie Article is displayed",LogType.STEP);
				        		
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify Search functionality for Two and results
	public Kohler_SearchPage VerifySearchFunctionalityTw()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchTw = KohlerSearchData.fetch("SearchTw");
				String SearchTwFetched = SearchTw.keyword;
				getCommand().sendKeys(search_Input, SearchTwFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[1]/h2[2]")));
			
				String search_for_two=getCommand().getText(Did_Mean_Two);
				Assert.assertTrue(search_for_two.contains("Did you mean two?"), "Did you mean two? is not displayed");
				log("Did you mean two? is displayed",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify typeahead with 3 letter i.e.Tou
	public Kohler_SearchPage VerifySearchFunctionalityTou()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				KohlerSearchData SearchTou = KohlerSearchData.fetch("SearchTou");
				
				String SearchTouFetched = SearchTou.keyword;
				getCommand().sendKeys(search_Input, SearchTouFetched);
			
				getCommand().waitFor(5);
				
				List<WebElement> linkElements= getCommand().driver.findElements(By.xpath("//*[@id='search-suggestions--mobile']/div[1]/div/a"));
				//String[] linkTexts= new String[linkElements.size()];		
				int size=linkElements.size();
				
				for (int i=0;i<size;i++) 
				{							
					String tou = linkElements.get(i).getText();		
					Assert.assertEquals(tou,"Touchless","Typeahead with 3 letters results are not displayed");
					
					if(tou.contains("Touchless"))
						log("Typeahead with 3 letters results are displayed",LogType.STEP);
					break;		
		        }
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	// Verify Search term(Leed) and the page loads to external website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_Leed()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchL = KohlerSearchData.fetch("SearchL");
				String SearchLFetched = SearchL.keyword;
				getCommand().sendKeys(search_Input, SearchLFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.urlContains("www.us.kohler.com"));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				
				Assert.assertTrue(CurrentPageUrl.contains("www.us.kohler.com"), "The searched term 'leed' not opened in an external website");
				getCommand().waitFor(10);
				log("The searched term 'leed' opened in an external website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify Search term(Robern) and the page loads to external website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_Robern()
	{
		try {
			
			    getCommand().driver.navigate().back();
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchR = KohlerSearchData.fetch("SearchR");
				String SearchRFetched = SearchR.keyword;
				getCommand().sendKeys(search_Input, SearchRFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.urlContains("www.robern.com"));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				
				Assert.assertTrue(CurrentPageUrl.contains("www.robern.com"), "The searched term 'robern' not opened in an external website");
				getCommand().waitFor(10);
				log("The searched term 'robern' opened in an external website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify Search term(Privacy) and the page loads to external website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_Privacy()
	{
		try {
			
				getCommand().driver.navigate().back();
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);;
				
				KohlerSearchData SearchP = KohlerSearchData.fetch("SearchP");
				String SearchPFetched = SearchP.keyword;
				getCommand().sendKeys(search_Input, SearchPFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
				wait.until(ExpectedConditions.urlContains("www.kohlercompany.com"));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				
				Assert.assertTrue(CurrentPageUrl.contains("www.kohlercompany.com"), "The searched term 'privacy' not opened in an external website");
				log("The searched term 'privacy' opened in an external website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage()); 
		}
		return this;
	}
	
	// Verify Search term(Bath Tub) and the page loads to internal website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_BathTub()
	{
		try {
			
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchBT = KohlerSearchData.fetch("SearchBT");
				String SearchBTFetched = SearchBT.keyword;
				getCommand().sendKeys(search_Input, SearchBTFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='product-category-content']/div[1]/div[1]/h1")));
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				
				Assert.assertTrue(CurrentPageUrl.contains("preprod.us.kohler.com"), "The searched term 'bath tub' not opened in an internal website");
				log("The searched term 'Bath tub' opened in an internal website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify Search term(Night Light) and the page loads to internal website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_NightLight()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchNL = KohlerSearchData.fetch("SearchNL");
				String SearchNLFetched = SearchNL.keyword;
				getCommand().sendKeys(search_Input, SearchNLFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='article-page']/div[1]/div/div[1]/div/div/h1")));
			
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				Assert.assertTrue(CurrentPageUrl.contains("preprod.us.kohler.com"), "The searched term 'night light' not opened in an internal website");
				log("The searched term 'Night light' opened in an internal website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
	// Verify Search term(Pressure Balance) and the page loads to internal website
	public Kohler_SearchPage VerifySearchFunctionalityTerm_PressureBalance()
	{
		try {
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchPB = KohlerSearchData.fetch("SearchPB");
				String SearchPBFetched = SearchPB.keyword;
				getCommand().sendKeys(search_Input, SearchPBFetched);
			
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='product-category-content']/div[1]/div[1]/h1")));
				
				String CurrentPageUrl=getCommand().driver.getCurrentUrl();
				getCommand().waitFor(10);
				Assert.assertTrue(CurrentPageUrl.contains("preprod.us.kohler.com"), "The searched term 'pressure balance' not opened in an internal website");
				log("The searched term 'pressure balance' opened in an internal website",LogType.STEP);
				
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
		
	}
	
/* Vimala Scripts*/
	
	//  Verify article redirects are turned off by searching 'moxie'
	public Kohler_SearchPage VerifyArticlesTurnedOffbyParts()
	{
		try
		{
			log("Verifying Parts Landing page in application",LogType.STEP);
			getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
            getCommand().waitForTargetVisible(Kohler_HomePage.parts_btn).click(Kohler_HomePage.parts_btn);
            getCommand().waitFor(2);
			
			log("Click on Parts menu",LogType.STEP);
			getCommand().isTargetVisible(findAll_parts);
			getCommand().click(findAll_parts);
			
			KohlerSearchData SearchMox = KohlerSearchData.fetch("SearchMox");
			String SearchMoxFetched = SearchMox.keyword;
			//getCommand().scrollTo);
			getCommand().sendKeys(Search_partsTerms, SearchMoxFetched);
			
			log("Click on search button",LogType.STEP);
			getCommand().isTargetVisible(button_PartsSearchIcon);
			getCommand().click(button_PartsSearchIcon);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
	     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
			
			String CurrentPageUrl= getCommand().driver.getCurrentUrl();
			
			Assert.assertTrue(!CurrentPageUrl.contains("article"),"Moxie is not an article page");
			
			log("Moxie is an article page",LogType.STEP);
			
	    }
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
	    }
	    return this;
	}
	
	// Verify collections redirects are turned off by searching 'memoirs'
	public Kohler_SearchPage VerifyCollectionsTurnedOffbyParts()
	{ 
		try
		{
			log("Verifying Parts Landing page in application",LogType.STEP);
			getCommand().waitForTargetVisible(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
            getCommand().waitForTargetVisible(Kohler_HomePage.parts_btn).click(Kohler_HomePage.parts_btn);
            getCommand().waitFor(2);
			
			log("Click on Parts menu",LogType.STEP);
			getCommand().isTargetVisible(findAll_parts);
			getCommand().click(findAll_parts);
			
			
			KohlerSearchData SearchMem = KohlerSearchData.fetch("SearchMem");
			String SearchMemFetched = SearchMem.keyword;
			//getCommand().scrollTo(Kohler_GeneralNavigation.Link_PartsWizard);
			getCommand().sendKeys(Search_partsTerms, SearchMemFetched);
			
			log("Click on search button",LogType.STEP);
			getCommand().isTargetVisible(button_PartsSearchIcon);
			getCommand().click(button_PartsSearchIcon);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
	     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
			
			String CurrentPageUrl= getCommand().driver.getCurrentUrl();
			Assert.assertTrue(!CurrentPageUrl.contains("collections"),"Memoir collections are not listed here");
			log("Memoir Collections page is shown",LogType.STEP);
			     		
	    }
		catch(Exception ex)
	    {
			Assert.fail(ex.getMessage());
	    }
	    	return this;
		}

		// Verify Search term(Poplin/Poplen) and each term display the same results
		public Kohler_SearchPage VerifySearchFunctionalityTermsDisplaySameResults()
		{
		try 
		{
			log("Verifying Search option in application",LogType.STEP);
			getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
			
			KohlerSearchData SearchPop = KohlerSearchData.fetch("SearchPop");
			String SearchPopFetched = SearchPop.keyword;
			getCommand().sendKeys(search_Input, SearchPopFetched);
			
			log("Click on search button",LogType.STEP);
		    getCommand().isTargetVisible(search_btn);
			getCommand().click(search_btn);
			
			WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
	     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
			
			String ProductTitle=getCommand().getText(Poplin_Products);
			Assert.assertTrue(ProductTitle.contains("Poplin"),"Poplin search results are not displayed for Poplen");
			log("Poplin search results are displayed",LogType.STEP);
		
		}
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
			
		} 		
		
		// Verify Search term(choreo/choreograph) and each term display the same results
		public Kohler_SearchPage VerifySearchFunctionalityTerm_choreo()
		{ 
		try
		{
				log("Verifying Search option in application",LogType.STEP);
				getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
				
				KohlerSearchData SearchCho = KohlerSearchData.fetch("SearchCho");
				String SearchChoFetched = SearchCho.keyword;
				getCommand().sendKeys(search_Input, SearchChoFetched);
				
				log("Click on search button",LogType.STEP);
				getCommand().isTargetVisible(search_btn);
				getCommand().click(search_btn);
				
				WebDriverWait wait = new WebDriverWait(getCommand().driver, 180);
		     	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='search-hero']/div/div[2]/h2")));
		     	
				String ProductTitle=getCommand().getText(choreograph_Products);
				if(ProductTitle.contains("choreograph"))
				Assert.assertTrue(ProductTitle.contains("choreograph"),"choreograph search results are not displayed");
				log("choreograph search results are displayed",LogType.STEP);
		}
		catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
			
		}

		
	
}
