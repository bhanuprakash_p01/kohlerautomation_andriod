package com.components.yaml;

import com.iwaf.framework.BasePage;

public class KohlerSearchData  {
	
	public String keyword;
	//public String itemCode;
	
	public static KohlerSearchData fetch(String key){
	BasePage pageObj = new BasePage();
	KohlerSearchData obj = pageObj.getCommand().loadYaml(key, "data-pool/KohlerSearchData.yaml");
		return obj;
	}
}
