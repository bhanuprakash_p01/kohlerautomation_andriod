package com.components.pages;

import com.components.repository.SiteRepository;
import com.iwaf.framework.BasePage;


public class SitePage extends BasePage{
	protected SiteRepository repository;
	
	SitePage(SiteRepository repository){
		this.repository=repository;
	}
	

	
	
	public Kohler_GeneralNavigation _GoToGeneralNavigation() 
	{
		return this.repository.generalNavigationPage();
	}
	
	public Kohler_DTVPage _gotoDTVPage()
	{
		return this.repository.dtvPage();
	}
	
	public Kohler_SearchPage  _GoToSearchPage() {
		return this.repository.kohlerSearchPage();
	}
	
	/*public WeatherPage _AtWeatherPage(){
		return this.repository.weatherPage();
	}*/
	/*
	public CommodityPage _AtCommodityPage(){
		return this.repository.commodityPage();
		
	}
	
	public FoundationPage _Pushnotification() {
		return this.repository.foundationPage();
	}
	
	public HomepageWeb _AtHomepageWeb() {
		return this.repository.homepageWeb();
	}
	
	public LoginpageWeb _AtLoginpageWeb()
	{
		return this.repository.loginpageWeb();
	}
	
	public AgeinfoPage _AtAgeinfopage()
	{
	
		return this.repository.ageinfoPage();
	}
	
	public FieldRegistrationPage _AtFieldRegistrationPage()
	{
	
		return this.repository.fieldRegistrationPage();
	}*/
}