/**
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 *																																		   	*
 * 2011-2012 Infosys Limited, Banglore, India. All Rights Reserved																			*
 * Version: 2.0																																*
 * 																																			*
 * Except for any free or open source software components embedded in this Infosys proprietary software program ("Program"),				*
 * this Program is protected by copyright laws, international treaties and other pending or existing intellectual property rights in India, *
 * the United States and other countries. Except as expressly permitted, any unautorized reproduction, storage, transmission 				*
 * in any form or by any means (including without limitation electronic, mechanical, printing, photocopying, recording or otherwise), 		*
 * or any distribution of this Program, or any portion of it, may result in severe civil and criminal penalties, 							*
 * and will be prosecuted to the maximum extent possible under the law 																		*
 *																																			*
 ********************************************************************************************************************************************
 ********************************************************************************************************************************************
 **/
package com.components.pages;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.Color; 

import com.components.repository.SiteRepository;
import com.components.yaml.LoginData;
import com.components.yaml.SearchData;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;


public class Kohler_HomePage extends SitePage
{

	/* Defining the locators on the Page */ 
	
	//public static final Target LOGO = new Target("target-logo","hplogo",Target.ID);
	//public static final Target btn_SignIn = new Target("btn_SignIn","//*[@id='gb_70']",Target.XPATH);
	public static final Target Link_HelpUsToImproveMore = new Target("link_HelpUsToImpMore","//a[@id='kampylink']",Target.XPATH);	
	public static final Target text_commenttextbox = new Target("text_commenttextbox","//textarea[@id='textarea']",Target.XPATH);	
	public static final Target btn_sendmyComments = new Target("btn_comments","//button[@class='submitButton']",Target.XPATH);	
	public static final Target btn_suggestion = new Target("btn_Sugg","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[1]/label/span/span",Target.XPATH);	
	public static final Target btn_dislike = new Target("btn_dlike","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[2]/label/span/span",Target.XPATH);	
	public static final Target btn_praise = new Target("btn_praise","//*[@id='QtnId-9741']/fieldset/section/form/ul/li[3]/label/span/span",Target.XPATH);	
	public static final Target btn_Rating = new Target("btn_rating","//*[@id= 'ItemId-35770-2']",Target.XPATH);	
	public static final Target btn_closeTheWind = new Target("btn_closeTheWind","//*[@id= 'Finish']",Target.XPATH);	
	public static final Target text_homePageTitle = new Target("text_title","//*[@id= 'Finish']",Target.XPATH);	
	
	public static final Target text_UtilityBarSignin = new Target("text_signin","//*[@id= 'user-name-tray']",Target.XPATH);	
	public static final Target text_UtilityBarMyFld = new Target("text_MyFolder","//a[@title= 'View My Folders']",Target.XPATH);	
	public static final Target text_UtilityBarContactUs = new Target("text_ContactUs","//*[text()= 'Contact Us']",Target.XPATH);	
	public static final Target text_UtilityBarFindAPro = new Target("text_FindAPro","//*[text()= 'Find a Pro']",Target.XPATH);	
	public static final Target text_UtilityBarFindAStore = new Target("text_FindAStore","//*[text()= 'Find a Store']",Target.XPATH);	
	public static final Target element_utilityBar = new Target("ele_Utilitybar","//*[@id= 'sign-in-bar']",Target.XPATH);	
	public static final Target button_slick_prev = new Target("button_slickPre","//*[@id=\"page-content-home\"]//following::button[text()='Previous']",Target.XPATH);	
	public static final Target button_slick_next = new Target("button_slickNxt","//*[@id=\"page-content-home\"]//following::button[text()='Next']",Target.XPATH);	
	
	public static final Target button_slick_next1 = new Target("button_slick_next1","//*[@id='page-content-home']/div[1]/div[1]/div/div/div[5]/div/div/div/div",Target.XPATH);	
	
	/* Defining the locators for SignIn */
	public static final Target btn_SigninHomePage  = new Target("btn_SigninHomePage","//*[@id='sign-in-bar__inner']/button[2]",Target.XPATH);
	public static final Target SignInModalPopup  = new Target("SignInModalPopup","//*[@id='modal--sign-in']/div",Target.XPATH);
	public static final Target EmailAddress  = new Target("EmailAddress","//*[@id='traySignInForm']/fieldset/div/input[@id='email']",Target.XPATH);
	public static final Target Password  = new Target("Password","//*[@id='traySignInForm']/fieldset/div/input[@id='password']",Target.XPATH);
	public static final Target btn_Signin  = new Target("btn_Signin","//*[@id='trayProfileSignIn']",Target.XPATH);
	public static final Target UserNameTray  = new Target("UserNameTray","//*[@id='user-name-tray']",Target.XPATH);
	public static final Target UserNameTrayDropDown  = new Target("UserNameTrayDropDown","//*[@id='sign-in-bar__account-dropdown']/div",Target.XPATH);
	public static final Target MyAccount  = new Target("MyAccount","//*[@id='sign-in-bar__account-dropdown']/div/a[1]",Target.XPATH);	
	public static final Target SignOut  = new Target("SignOut","//*[@id='sign-in-bar__account-dropdown']/div/a[2]",Target.XPATH);
	public static final Target FirstName_MyAccount  = new Target("FirstName_MyAccount","//*[@id='tabbed-results__account-details']/div/div[3]/div/div[2]/p",Target.XPATH);
	public static final Target EditAccount  = new Target("EditAccount","//*[@id='account-details--button']",Target.XPATH);
	public static final Target ActualAccountFieldDetail = new Target("ActualAccountFieldDetail","//*[@id='tabbed-results__account-details']/div/div[1]/div/div[7]/p",Target.XPATH);
	public static final Target AccountFieldtoEdit = new Target("AccountFieldtoEdit","//*[@id='describe-yourself-dropdown-page_title']",Target.XPATH);
	public static final Target SaveEditedAccount = new Target("SaveEditedAccount","//*[@id='account-details--edit']/div[3]/div/div[1]/div[1]/div[1]/input[1]",Target.XPATH);
	public static final Target SaveSuccess = new Target("SaveSuccess","//*[@id='updateSuccess']/span",Target.XPATH);	
	public static final Target BlueBanner = new Target("BlueBanner","//*[@id='header']/div[2]/div[1]/div",Target.XPATH);

	/* Defining the locators for MyFolder */
	public static final Target MyFolder= new Target("MyFolder","//*[@id='sign-in-bar__inner']/span/a",Target.XPATH);
	public static final Target CreateFolder= new Target("CreateFolder","//*[@id='my-folders']/div[3]/div/div/p/span[2]/a",Target.XPATH);
	public static final Target FolderName= new Target("FolderName","//*[@id='folder-name']",Target.XPATH);
	public static final Target CreateFolderNotes= new Target("CreateFolderNotes","//*[@id='textarea-140']",Target.XPATH);
	public static final Target btn_CreateFolder= new Target("btn_CreateFolder","//*[@id='createNewFolder']/button[1]",Target.XPATH);
	public static final Target ActualFolderName= new Target("btn_CreateFolder","//*[@id='shareFolderName']",Target.XPATH);
	public static final Target MyFolderItemsGrid = new Target("BlueBanner","//*[@id='my-folders-detail__items']/div[1]/div/div/div[2]/div[2]/p[@class='order-detail-table__sku']",Target.XPATH);
	
	public static final Target MyFolderImage = new Target("MyFolderImage","//*[@id='folderMainImage']",Target.XPATH);
	public static final Target MyFolderCost = new Target("MyFolderCost","//*[@id='my-folders']/div[1]/div/div[2]/div/div[2]/p[1]",Target.XPATH);
	public static final Target MyFolderNotes = new Target("MyFolderNotes","//*[@id='editFolderNote']",Target.XPATH);
	public static final Target MyFolderItemsListing = new Target("MyFolderItemsListing","//*[@id='my-folders']/div[3]/div/div/div/div[3]",Target.XPATH);
	public static final Target CheckBox = new Target("CheckBox","//*[@id=\"my-folders-detail__items\"]/div[2]/div[1]/div/div/div[2]/div[3]/label/span[1]",Target.XPATH);
	public static final Target SecondCheckBox = new Target("SecondCheckBox","//*[@id='my-folders-detail__items']/div[1]/div[2]/div/div[1]/label/span[1]",Target.XPATH);
	public static final Target ItemText = new Target("ItemText","//*[@id=\"my-folders-detail__items\"]/div[2]/div[1]/div/div/div[2]/p[1]",Target.XPATH);
	public static final Target SecondItemText = new Target("ItemText","//*[@id='my-folders-detail__items']/div[1]/div[2]/div/div[2]/div[2]/p[1]",Target.XPATH);
	
	public static final Target CopyMyFolder_Signedin = new Target("CopyMyFolder_Signedin","//*[@id='my-folders']/div[3]/div/div/div/div[2]/div[4]/div/button[1]",Target.XPATH);
	public static final Target MoveMyFolder_Signedin = new Target("MoveMyFolder_Signedin","//*[@id='my-folders']/div[3]/div/div/div/div[2]/div[4]/div/button[2]",Target.XPATH);
	public static final Target Add_CopyItem= new Target("Add_CopyItem","//*[@id='copyToFolder']/button[1]",Target.XPATH);
	
	public static final Target ShareFolderOption= new Target("ShareFolderOption","//*[@id='my-folders']/div[1]/div/div[2]/div/div[4]/div/button[4]",Target.XPATH);
	public static final Target Share_EmailAFriend= new Target("Share_EmailAFriend","//*[@id='modal-myfolder-email-friend']/div/h4",Target.XPATH);
	public static final Target Share_EmailAFriendClose= new Target("Share_EmailAFriendClose","//*[@id='modal-myfolder-email-friend']/div/button/i",Target.XPATH);
	public static final Target Share_ShareKohlerClose= new Target("Share_ShareKohlerClose","//*[@id='modal-with-showroom']/div/button/i",Target.XPATH);
	public static final Target Share_KohlerShowroom= new Target("Share_KohlerShowroom","//*[@id='modal-with-showroom']/div/h4",Target.XPATH);
	public static final Target CopyFolderOption= new Target("CopyFolderOption","//*[@id='my-folders']/div[1]/div/div[2]/div/div[4]/div/button[2]",Target.XPATH);	
	public static final Target SaveAsNewFolder= new Target("SaveAsNewFolder","//*[@id='saveAsNewFolder']/button[2]",Target.XPATH);
	public static final Target DeleteFolderOption= new Target("DeleteFolderOption","//*[@id='my-folders']/div[1]/div/div[2]/div/div[4]/div/button[1]",Target.XPATH);
	public static final Target DeleteFolderConfirmation= new Target("DeleteFolderConfirmation","//*[@id='removeFolder']/button[1]",Target.XPATH);

	/* Defining the locators for Kohler Ideas grid */ 
	public static final Target KohlerIdeaslayout_LeftArrow  = new Target("KohlerIdeaslayout_LeftArrow","//*[@id='curalate-content']/button[1]",Target.XPATH);
	public static final Target KohlerIdeaslayout_RightArrow = new Target("KohlerIdeaslayout_RightArrow","//*[@id='curalate-content']/button[2]",Target.XPATH);
	public static final Target KohlerIdeaslayout_ViewGallery = new Target("KohlerIdeaslayout_ViewGallery","curalate-view-gallery",Target.CLASS);
	public static final Target KohlerIdeaslayout_SubmitaPhoto = new Target("KohlerIdeaslayout_SubmitaPhoto","curalate-upload-photos",Target.ID);
	public static final Target KohlerIdeaslayout_Uploader = new Target("KohlerIdeaslayout_Uploader","/html/body/div[2]/div[1]",Target.XPATH);
	public static final Target KohlerIdeaslayout = new Target("KohlerIdeaslayout","curalate-thumbs",Target.CLASS);
	
	/* Defining the locators for Overlay popup on the Page */ 
	public static final Target OverlayDisplayCross = new Target("OverlayDisplayCross","curalate-modal-close",Target.CLASS);
	public static final Target OverlayDisplay_RightArrow = new Target("OverlayDisplay_RightArrow","/html/body/div[17]/div/div[1]/button[2]",Target.XPATH);
	public static final Target OverlayDisplay_LeftArrow = new Target("OverlayDisplay_LeftArrow","/html/body/div[17]/div/div[1]/button[1]",Target.XPATH);
	public static final Target OverlayDisplay_UserName= new Target("OverlayDisplay_UserName","curalate-username",Target.CLASS);
	public static final Target OverlayDisplay_Share= new Target("OverlayDisplay_Share","curalate-share-header",Target.CLASS);	
	public static final Target OverlayDisplay_Share_F= new Target("OverlayDisplay_Share_F","/html/body/div[17]/div/div[3]/div[2]/div[2]/button[1]",Target.XPATH);
	public static final Target OverlayDisplay_Share_T= new Target("OverlayDisplay_Share_T","/html/body/div[17]/div/div[3]/div[2]/div[2]/button[2]",Target.XPATH);
	public static final Target OverlayDisplay_Share_P= new Target("OverlayDisplay_Share_P","/html/body/div[17]/div/div[3]/div[2]/div[2]/button[3]",Target.XPATH);
	public static final Target OverlayDisplay_ShopHeader= new Target("OverlayDisplay_ShopHeader","curalate-shop-header",Target.CLASS);
	public static final Target OverlayDisplay_SmallProductImage= new Target("OverlayDisplay_SmallProductImage","curalate-product-image",Target.CLASS);
	public static final Target OverlayDisplay_SmallProductDescription= new Target("OverlayDisplay_SmallProductDescription","curalate-product-name",Target.CLASS);
	public static final Target OverlayDisplay_SmallProductNxt= new Target("OverlayDisplay_SmallProductNxt","curalate-products-carousel-next",Target.CLASS);
	public static final Target OverlayDisplay_SmallProductPrev= new Target("OverlayDisplay_SmallProductPrev","curalate-products-carousel-prev",Target.CLASS);
	
	/* Defining the locators for Promo grid on the Page */ 
	public static final Target PromoModuleGrid= new Target("PromoModuleGrid","//*[@class='container' ]/ul/li[@class='promotion']/a",Target.XPATH);
	public static final Target BackToKohler= new Target("BackToKohler","backToLink",Target.ID);
	
	/* Defining the locators for Discover the Possibilities grid on the Page */
	public static final Target NextArrow_DiscoverthePossibilities= new Target("NextArrow_DiscoverthePossibilities","//*[@id='container-home']/div[3]/div/div/div[1]/button[2]",Target.XPATH);
	public static final Target PrevArrow_DiscoverthePossibilities= new Target("PrevArrow_DiscoverthePossibilities","//*[@id='container-home']/div[3]/div/div/div[1]/button[1]",Target.XPATH);	
	public static final Target AddToFolderTextArea= new Target("AddToFolderTextArea","//*[@id='addToFolder']/div[4]/div/textarea",Target.XPATH);	
	public static final Target AddToFolderTextArea_Signin= new Target("AddToFolderTextArea_Signin","//*[@id='addToFolder']/div[7]/div/textarea",Target.XPATH);
	public static final Target AddToFolderDropDown= new Target("AddToFolderDropDown","//*[@id='folderDropdown_title']",Target.XPATH);
	public static final Target AddToFolderSubmitButton= new Target("AddToFolderSubmitButton","//*[@id='addToFolder']/button[1]",Target.XPATH);
	public static final Target AddToFolderContinueShopping= new Target("AddToFolderContinueShopping","//button[contains(text(),'CONTINUE SHOPPING')]",Target.XPATH);	
	public static final Target MyKohlerFolder= new Target("MyKohlerFolder","//*[@id='my-folders']/div[3]/div/div[2]/div/a/img",Target.XPATH);
	public static final Target AddToFolder= new Target("AddToFolder","//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div[2]/div[6]/div[1]/button[1]",Target.XPATH);
	public static final Target EditTextArea_Folder= new Target("EditTextArea_Folder","//*[@id='textarea-140-3']",Target.XPATH);
	public static final Target EditSave_Folder= new Target("EditSave_Folder","//*[@id='editItemNote']/button[1]",Target.XPATH);
	public static final Target Deletebtn_Folder= new Target("Deletebtn_Folder","(//button[@title='Delete'])[1]",Target.XPATH);
	public static final Target SelectAllCheckBox= new Target("SelectAllCheckBox","//*[@id='my-folders']/div[3]/div/div/div/div[1]/div[3]/button",Target.XPATH);
	public static final Target DeleteConfirmation= new Target("DeleteConfirmation","//*[@id='deleteItems']",Target.XPATH);
	//##################################Mobile xpaths#########################################################################
	
	
	public static final Target hamBurger_Icon=new Target("hamBurger_Icon","//*[@id='mobile-nav-top__button--menu']",Target.XPATH);
	public static final Target kohler_Logo=new Target("kohler_Logo","//*[@class='logoImg']",Target.XPATH);
	public static final Target bathroom_btn=new Target("bathroom_btn","//*[@id='main-nav__button--bathroom']",Target.XPATH);
	public static final Target kitchen_btn=new Target("kitchen_btn","//*[@id='main-nav__button--kitchen']",Target.XPATH);
	public static final Target parts_btn=new Target("parts_btn","//*[@id='main-nav__button--parts']",Target.XPATH);
	public static final Target ideas_btn=new Target("ideas_btn","//*[@id='main-nav__button--ideas ']",Target.XPATH);
	public static final Target search_Icon=new Target("search_Icon","(//*[@class='icon--search'])[1]",Target.XPATH);
	public static final Target link_findPro=new Target("link_findPro","(//*[@class='mobile-sign-in__link'])[2]",Target.XPATH);
	public static final Target link_findStore=new Target("link_findStore","(//*[@class='mobile-sign-in__link'])[1]",Target.XPATH);
	public static final Target link_myFolders=new Target("link_myFolders","(//*[@class='mobile-sign-in__link'])[3]",Target.XPATH);
	public static final Target link_Register=new Target("link_Register","(//*[@class='mobile-sign-in__link'])[4]",Target.XPATH);
	public static final Target link_contactUs=new Target("link_contactUs","(//*[@class='mobile-sign-in__link'])[5]",Target.XPATH);
	public static final Target link_worldWide=new Target("link_worldWide","(//*[@class='mobile-sign-in__link'])[6]",Target.XPATH);
	public static final Target link_signIn=new Target("link_signIn","//*[@class='mobile-sign-in__link 22']",Target.XPATH);
	public static final Target password_Input=new Target("password_Input","//*[@id='password']",Target.XPATH);
	public static final Target userName_Input=new Target("userName_Input","//*[@id='email']",Target.XPATH);
	public static final Target signIn_btn=new Target("signIn_btn","//*[@id='trayProfileSignIn']",Target.XPATH);
	public static final Target myKohler_Folder=new Target("myKohler_Folder","//*[@id='my-folders']/div[3]/div/div[2]/div/a/img",Target.XPATH);
	public static final Target link_MyAccount=new Target("link_MyAccount","//*[@id='main-nav__buttons']/li[9]/a",Target.XPATH);
	public static final Target signOut_btn=new Target("signOut_btn","//*[@id='main-nav__buttons']/li[10]/a",Target.XPATH);
	public static final Target search_Input=new Target("search_Input","//*[@id='search']",Target.XPATH);
	public static final Target search_btn=new Target("search_btn","//*[@id='header__button--search-button-mobile']",Target.XPATH);
	public static final Target viewGallery = new Target("viewGallery_btn","//*[@id='curalate-fan-reel']/div[4]/a",Target.XPATH);
    public static final Target other_Products=new Target("other_Products","//*[@data-toggle-target='#footer__tab--2']",Target.XPATH);
	
    public static final Target KCC_RightArrow=new Target("KCC_RightArrow","//*[@id='curalate-content']/button[2]",Target.XPATH);
    public static final Target KCC=new Target("KCC","//*[@id='curalate-fan-reel']/div[1]",Target.XPATH);
    public static final Target KCC_LeftArrow=new Target("KCC_LefftArrow","//*[@id='curalate-content']/button[1]",Target.XPATH);
    public static final Target viewGallery_CTA=new Target("viewGallery_CTA","//*[@id='curalate-fan-reel']/div[4]/a",Target.XPATH);
    public static final Target submitPhoto_CTA=new Target("submitPhoto_CTA","//*[@id='curalate-upload-photos']",Target.XPATH);
    
    
    public static final Target addToFolderTextArea=new Target("AddToFolderTextArea","//*[@id='textarea-140-5']",Target.XPATH);
    public static final Target addToFolderSubmitButton=new Target("AddToFolderSubmitButton","//*[@id=\"addToFolder\"]/button[1]",Target.XPATH);
    public static final Target addToFolderContinueShopping=new Target("AddToFolderContinueShopping","//*[@class='btn btn--white marg-l-35-sm-only marg-t-10-sm-only']",Target.XPATH);
    public static final Target myKohlerFolder=new Target("MyKohlerFolder","//*[@id='my-folders']/div[3]/div/div[2]/p/a",Target.XPATH);
   

   
    public static final Target learn_more=new Target("clcking on Learn More button","//div[@class='cta-grouping slick-slide slick-active']//a",Target.XPATH);
    
    public static final Target say_hellotext=new Target("say_hellotext","//div[@class='cta-grouping slick-slide slick-active']//h2",Target.XPATH);
    
    public static final Target our_Company=new Target("our_Company","//*[@data-toggle-target='#footer__tab--1']",Target.XPATH);
    public static final Target kohler_Co=new Target("kohler_Co","//*[@data-toggle-target='#footer__tab--2']",Target.XPATH);

    public static final Target total_count_notes=new Target("total_count_notes","//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder']",Target.XPATH);
    
    public static final Target edit=new Target("edit","(//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder'][1]//span)[10]",Target.XPATH);
    public static final Target ViewFolder=new Target("ViewFolder"," //*[@id='folderIdThankYou']",Target.XPATH);

    public static final Target firstname_Edit=new Target("firstname_Edit","//*[@id='profile-first-name']",Target.XPATH);
	public static final Target firstname=new Target("firstname","//*[@id='tabbed-results__account-details']/div/div[1]/div/div[2]/p",Target.XPATH);
	public static final Target textArea=new Target("textArea","	//*[@id='textarea-140-5']",Target.XPATH);
	public static final Target selectAll=new Target("selectAll","//*[@id='my-folders']/div[3]/div/div/div/div[2]/div[3]/button",Target.XPATH);
	public static final Target DeleteBtn=new Target("DeleteBtn","	(//*[@title='Delete'])[3]",Target.XPATH);

	
    

    
	public Kohler_HomePage(SiteRepository repository)
	{
		super(repository);
	}

	/* Functions on the Page are defined below */
	
	public Kohler_HomePage atHomePage()
	{
		log("Launched Kohler Site",LogType.STEP);
		
		return this;
		
	}

	// Verify the list of links in Home Page Footer
			public Kohler_HomePage VerifyFooterLinks()
			{
				System.out.println("VerifyFooterLinks()...");
				
				log("Verifying HomePage_fotter Links",LogType.STEP);
				
			
				List<WebElement> all_footer_header = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[2]/div/div"));
				int  all_footerheader_count = all_footer_header.size();
			
				try 
				{
					for (int j =1; j <= all_footerheader_count; j++) 
					{
						
						String part1="//*[@id='footer']/div[2]/div/div[";
						String part2="]";
						
						String part3=part1+j+part2;
						
						getCommand().getDriver().findElement(By.xpath(part3)).click();
						getCommand().waitFor(2);
						//all_footer_header.get(j).click();
						// get the list of footer header
						List<WebElement> footer_header_links_all = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[2]/div/div["+j+"]"));
						int footerHeaderlinks_count = footer_header_links_all.size();
						System.out.println("footer header count.."+footerHeaderlinks_count);
						for (int k = 1; k <= footerHeaderlinks_count; k++)
						{
							// get the list of links under each column
							List<WebElement> footer_elements = getCommand().driver.findElements(By.xpath("//*[@id='footer']/div[2]/div/div["+j+"]/div[@id ='footer__tab--"+j+"']/a"));
							int footerColLinks_count = footer_elements.size();
							for (int l = 0; l <2 ; l++) 
							{
								//String current_win = getCommand().driver.getWindowHandle();
								//System.out.println(current_win);
								//footer_elements.get(l).click();
								int Win_size = getCommand().driver.getWindowHandles().size();
								log("Open Windows Size is :" + Win_size ,LogType.STEP);
								//System.out.println(Win_size);
								System.out.println("footer link text.."+footer_elements.get(l).getText());
								//System.out.println(footer_elements.get(l).getText());
									log(footer_elements.get(l).getText(),LogType.STEP);
									String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
									
									/*log("Verify the color on hover",LogType.STEP);
									Actions builder = new Actions(getCommand().driver);
									builder.moveToElement(footer_elements.get(l)).build().perform();
									String hoverOnColor  = footer_elements.get(l).getCssValue("color");
									
									String[] color1 = hoverOnColor.replace("rgba(", "").split(",");       
									 String hex = String.format("#%02x%02x%02x", Integer.parseInt(color1[0].trim()), Integer.parseInt(color1[1].trim()), Integer.parseInt(color1[2].trim()));  
									log("Convert rgb to hex : " + hex.toUpperCase(),LogType.STEP); 
									
									if(hex.equals("#FFFFFF"))
										Assert.assertEquals(hex, "#FFFFFF", "link is not lighten on hover");*/
									
									
									footer_elements.get(l).sendKeys(selectLinkOpeninNewTab);
									getCommand().waitFor(5);
									
									//footer_elements.get(l).sendKeys(Keys.CONTROL +"t");
									ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
									getCommand().driver.switchTo().window(tabs2.get(1));
									Win_size = getCommand().driver.getWindowHandles().size();
								    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
								    					    
								    String pageTitle = getCommand().driver.getTitle();
								    //System.out.println("Page title is:" + pageTitle);
								    log("Page title is:" + pageTitle,LogType.STEP);
								    //if(!getCommand().driver.getCurrentUrl().isEmpty())
								    	//getCommand().driver.navigate().refresh();
								    	getCommand().driver.close();
								    	getCommand().driver.switchTo().window(tabs2.get(0));
									
							}
						
						
						}
					}
				}catch(Exception ex)
				{
					Assert.fail(ex.getMessage());
				}
				

				return this;
			}
		
	// Verify HelpUsToImproveMore link functionality
	public Kohler_HomePage VerifyHelpUsToImproveMore(String text, String feedback)
	{
		 log("Verify the Help Us Improve This Site link",LogType.STEP);
	       try {
	                    
	                    log("Verify the clicking of Help Us Improve This Site link",LogType.STEP);
	                    
	                     getCommand().scrollTo(viewGallery_btn);
	                     getCommand().waitFor(5);
	                     getCommand().scrollTo(viewGallery_btn);
	             
	                     getCommand().click(Link_HelpUsToImproveMore);
	                     getCommand().waitFor(10);
	                    
	                    ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                    getCommand().driver.switchTo().window(tabs2.get(1));
	                   String pageTitle = getCommand().driver.getTitle();
	                  
	                  log("Page title is:" + pageTitle,LogType.STEP);
	                  Assert.assertEquals(pageTitle, "Kohler US Study", "Title mismatch");
	                  
	                  log("Select the type of feedback" , LogType.STEP);
	                  
	                  switch (feedback) {
	                    case "Suggestion": 
	                                 getCommand().waitForTargetVisible(btn_suggestion);
	                                 getCommand().click(btn_suggestion);
	                           break;
	                    case "Dislike": 
	                           getCommand().waitForTargetVisible(btn_dislike);
	                        getCommand().click(btn_dislike);
	                        break;
	                    case "Praise": 
	                           getCommand().waitForTargetVisible(btn_praise);
	                           getCommand().click(btn_praise);
	                       break;

	                    default:
	                           break;
	                    }
	                  
	                  
	                  log("Give the Comment in the text box" , LogType.STEP);
	                  getCommand().sendKeys(text_commenttextbox,text);
	                  getCommand().scrollTo(arrow_Forward).click(arrow_Forward);
	                  
	                  log("Select the rating based on feedback" , LogType.STEP);
	                  getCommand().waitForTargetVisible(btn_Rating);
	                  getCommand().click(btn_Rating);
	                  getCommand().waitFor(2);
	                  getCommand().scrollTo(arrow_Forward).click(arrow_Forward);
	                  getCommand().waitFor(5);
	                
	                  log("Click on Close the window button after submitting the feedback" , LogType.STEP);
	                  getCommand().waitForTargetVisible(btn_closeTheWind);
	                  getCommand().click(btn_closeTheWind);
	                  
	                  log("Switch back to the main window from the Feedback window" , LogType.STEP);
	                  getCommand().driver.switchTo().window(tabs2.get(0));
	                
	                     
	              
	       }catch(Exception ex)
	       {
	    	   Assert.fail(ex.getMessage());
	       }
return this;

		
		
	}
	
	// verify Home Page title
	public Kohler_HomePage verifyHomePageTitle()
	{
		try {
				log("Verify the Home Page Title",LogType.STEP);
				Assert.assertEquals(getCommand().driver.getTitle() ,"KOHLER | Toilets, Showers, Sinks, Faucets and More for Bathroom & Kitchen","Home Page Title mismatch");
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	// Verify Home Page URL
	public Kohler_HomePage verifyHomePageURL()
	{
		try {
				boolean condtion= true;
				log("Verify the Home Page URL",LogType.STEP);
				String URL = getCommand().driver.getCurrentUrl();
				if(URL.contains("https://"))
				Assert.assertTrue(condtion);
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
		
		return this;
	}
		
	// Verify utility Bar color layout and content
	
	public static final Target myFolder_Header=new Target("myFolder_Header","//*[@id='my-folders']/div[1]/h1",Target.XPATH);
	public Kohler_HomePage VerifyHamburger()
	{
		try {
			 String Url = getCommand().driver.getCurrentUrl();
			log("Verifying of Hamburger menu elements",LogType.STEP);
			getCommand().waitForTargetVisible(hamBurger_Icon).click(hamBurger_Icon);
			getCommand().waitFor(2);
			getCommand().isTargetVisible(link_signIn);
			log("Signin link is available under hamburger menu",LogType.STEP);
			getCommand().isTargetVisible(link_myFolders);
			log("My Folder link is available under hamburger menu",LogType.STEP);
			getCommand().waitFor(3);
			getCommand().isTargetVisible(link_contactUs);
			log("Contact us link is available under hamburger menu",LogType.STEP);
			getCommand().isTargetVisible(link_findPro);
			log("Find a pro link is available under hamburger menu",LogType.STEP);
			getCommand().isTargetVisible(link_findStore);
			log("Find A store link is available under hamburger menu",LogType.STEP);
			List<WebElement> hamburger_Links=getCommand().driver.findElements(By.xpath("//*[@class='mobile-sign-in__link']"));
			int count=hamburger_Links.size();
			System.out.println(count);
			for(int i=1;i<count;i++) {
				Target menu_Links=new Target("menu_Links","(//*[@class='mobile-sign-in__link'])["+i+"]",Target.XPATH);
				String text=getCommand().getText(menu_Links);
				log("Clicking of "+text+" link",LogType.STEP);
				getCommand().sendKeys(menu_Links, Keys.chord(Keys.CONTROL,Keys.RETURN) );
				ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
				getCommand().waitFor(1);
				getCommand().driver.switchTo().window(listofTabs.get(1));
				
				String currentURL=getCommand().driver.getCurrentUrl();
				 if(Url.equals(currentURL))
			           
		            {
		                  log("Clicking on link "+text+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
		                  Assert.fail("Clicking on link "+text+" is not redirecting to the corresponding page");
		            }

		           else
		              {                                             
		                   log("Clicking on link "+text+" is redirecting to the corresponding page",LogType.STEP);
		              }

		            getCommand().driver.close();
		            getCommand().driver.switchTo().window(listofTabs.get(0));
				
			}
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	// verify worldwide countries expansion and landing on related page
	public Kohler_HomePage VerifyWorldWideCountriesLink()
	{
		try {
			
			getCommand().waitForTargetVisible(hamBurger_Icon).click(hamBurger_Icon);
			   WebElement element=getCommand().driver.findElement(By.xpath("(//*[@class='mobile-sign-in__link'])[6]"));
        	      JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
        	      jse.executeScript("arguments[0].scrollIntoView(true);", element);
        	       Thread.sleep(2000);
        	    
        	      jse.executeScript("window.scrollBy(0,1000)");

			log("Click on Worldwide click in Hamburger menu",LogType.STEP);
			getCommand().click(link_worldWide);
			List<WebElement> ListOfHeader = getCommand().driver.findElements(By.xpath("//*[@id='regionlist']/div/div/h5"));
			
			log("Verify worldWide Countries link",LogType.STEP);
			
			 log("Checking the navigation of each link",LogType.STEP);
			//List<WebElement> ListOfAllCountries = getCommand().driver.findElements(By.xpath("//*[@class='container regionlist regionListFlags row']/div"));
			 
			for (int i = 1; i<=ListOfHeader.size(); i++)
				
			{
				 String Url = getCommand().driver.getCurrentUrl();
		          getCommand().driver.findElement(By.xpath("//*[@id='regionlist']/div/div["+i+"]/h5")).click();

				List<WebElement> ListOfCuntries_div = getCommand().driver.findElements(By.xpath("//*[@class='dropdown active']/li/a"));
				int country=ListOfCuntries_div.size();
				for(int j=1;j<=country;j++) {
					Target country_Name=new Target("country_Name","//*[@class='dropdown active']/li["+j+"]/a",Target.XPATH);
					 String Linktext =getCommand().getText(country_Name);
		             log("Clicking on the link "+Linktext,LogType.STEP);
					getCommand().sendKeys(country_Name,Keys.chord(Keys.CONTROL,Keys.RETURN) );
					

					ArrayList<String> tab = new ArrayList<String> (getCommand().driver.getWindowHandles());
				    getCommand().waitFor(1);
				    getCommand().driver.switchTo().window(tab.get(1));
				    
				   
				    String CurrentpageUrl = getCommand().driver.getCurrentUrl();
			           if(Url.equals(CurrentpageUrl))
			           
			            {
			                  log("Clicking on link "+Linktext+" is not redirecting to the corresponding page",LogType.ERROR_MESSAGE);
			                  Assert.fail("Clicking on link "+Linktext+" is not redirecting to the corresponding page");
			            }

			           else
			              {                                             
			                   log("Clicking on link "+Linktext+" is redirecting to the corresponding page",LogType.STEP);
			              }

			            getCommand().driver.close();
			            getCommand().driver.switchTo().window(tab.get(0));
						
					
				}
				 getCommand().driver.findElement(By.xpath("//*[@id='regionlist']/div/div["+i+"]/h5")).click();
				
			}

			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	// Verify brand tray disappears when scrolling down
	public Kohler_HomePage VerifyBrandTrayVisibility()
	{
		boolean condition = true;
		try {
				
				log("Verify brand tray visibility on scroll down",LogType.STEP);
				boolean state = getCommand().isTargetVisible(element_utilityBar);
				if(state)
				{
					log("Scroll down if the tray is visible",LogType.STEP);
					JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
					js.executeScript("window.scrollBy(0,3000)");

				}
				
				condition = getCommand().isTargetVisible(element_utilityBar);

				if(!condition)
					log("verify the condition when the brand tray is invisible",LogType.STEP);
					Assert.assertTrue(true, "utitliy bar is not viewable");

			
			}catch(Exception ex)
			{
				Assert.fail(ex.getMessage());
			}
		return this;
	}

	public static final Target image=new Target("image","(//*[@class='carousel-hero full-bleed slick-initialized slick-slider' or @class='slick-list draggable'])",Target.XPATH);
	public static final Target hero_SlickNext=new Target("hero_SlickNext","(//*[@class='slick-next'])[1]",Target.XPATH);
	public static final Target hero_NavDots=new Target("hero_NavDots","//*[@id='page-content-home']/div[1]/div[1]/ul/li",Target.XPATH);
	
	//Verify Hero carousel
	public Kohler_HomePage VerifyHero()
	{
		try {
				String imgText=null;
				int count=2;
				
				log("Verifying of Learn more CTA presence",LogType.STEP);
				getCommand().isTargetVisible(learn_more);
				
				log("Verify the text displayed below Hero",LogType.STEP);
				List<String> img_Text=new ArrayList<String>();
				List<WebElement> nav_Dots=new ArrayList<WebElement>(getCommand().driver.findElements(By.xpath("//*[@id='page-content-home']/div[1]/div[1]/ul/li")));
				int Win_size = getCommand().driver.getWindowHandles().size();
				System.out.println(Win_size);
				String url=getCommand().driver.getCurrentUrl();
				//Target img=new Target("img","//*[@id='page-content-home']/div[1]/div[1]/div/div/div["+count+"]/div/div/div",Target.XPATH);
				
				for (WebElement navDot:nav_Dots) 
				{
					log("verify the list of navigationation buttons on Hero and click on each;",LogType.STEP);
					navDot.click();
					log("Hold the carousel by hovering on it",LogType.STEP);
					getCommand().mouseHover(button_slick_next1);
					String navDotsClass = navDot.getAttribute("class");
					if(navDotsClass.contains("slick-active"))
					{
						log("verify the text below  of each Carousel by clicking on hero side arrows",LogType.STEP);
						imgText = getCommand().driver.findElement(By.xpath("//*[@id='page-content-home']/div[1]/div/div/div/div["+count+"]/h2")).getText();
						System.out.println(imgText);
						
						log("Image text of carousel is :" +imgText ,LogType.STEP);
						count++;
						
						img_Text.add(imgText);
						String LinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
						getCommand().sendKeys(learn_more,LinkOpeninNewTab);
						
						getCommand().waitFor(10);
						ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
						getCommand().waitFor(1);
						getCommand().driver.switchTo().window(listofTabs.get(1));
						String CurrentUrl=getCommand().driver.getCurrentUrl();
						if(url.equals(CurrentUrl)) {
							log("Clicking of "+imgText+" not navigated to the corresponding page",LogType.STEP);
						}
						else {
							log("Clicking of "+imgText+ " navigated to the corresponding page",LogType.STEP);
						}
						
						getCommand().driver.close();
				        getCommand().driver.switchTo().window(listofTabs.get(0));
						
						
					    
						
				     }
					
				}
			
			
		
			
			}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	// Verify links to other Kohler brands opens a new window.
	public Kohler_HomePage VerifyLinkOtherKohlerBrands()
	{
		try {
			log("Verify Link Other than Kohler Brands: ",LogType.STEP);
			getCommand().scrollTo(submitPhoto_CTA).click(kohler_Co);
			List<WebElement> Footer_KohlerColinks = getCommand().driver.findElements(By.xpath("//*[@id='footer__tab--2']/a"));
			for(WebElement footer_link: Footer_KohlerColinks)
			{
				if(footer_link.getText().contains("Ann Sacks") ||footer_link.getText().contains("Kallista") || footer_link.getText().contains("Robern") || footer_link.getText().contains("Sterling") || footer_link.getText().contains("Novita"))
				{
					int Win_size = getCommand().driver.getWindowHandles().size();
					log("Verify the size of open windows currently: "+ Win_size,LogType.STEP);
					
					log("click on each link and verify in new tab",LogType.STEP);
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
					footer_link.sendKeys(selectLinkOpeninNewTab);
					
					getCommand().waitFor(10);
					
					ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
					
					getCommand().driver.switchTo().window(tabs2.get(1));
					Win_size = getCommand().driver.getWindowHandles().size();
				    Assert.assertEquals(2, Win_size, "On clicking on link, the page is not opned in new window");
				    
					String pageTitle = getCommand().driver.getTitle();
					log("verify the Page title"+ pageTitle,LogType.STEP);
					//System.out.println(pageTitle);
					getCommand().driver.close();
					log("Close the tab and switch back to parent window",LogType.STEP);
					getCommand().driver.switchTo().window(tabs2.get(0));
				}
			}
			
			
		}catch(Exception ex)
		{
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_HomePage signIn(String Data)
	{	
		  LoginData loginCred=LoginData.fetch(Data);
		try
		{
			 log("SignIn to the application",LogType.STEP);
             String userName=loginCred.UserName;
             String passWord=loginCred.Password;
       
              getCommand().waitForTargetPresent(hamBurger_Icon).click(hamBurger_Icon);
              getCommand().scrollTo(link_myFolders);
         
             getCommand().waitForTargetPresent(link_signIn).click(link_signIn);
             getCommand().waitFor(3);
             log("Clicking of Signin link from the hamburegr menu",LogType.STEP);
             getCommand().scrollTo(password_Input);
             
             getCommand().sendKeys(userName_Input, userName);
             getCommand().sendKeys(password_Input, passWord);
             log("Entered the User name and Password",LogType.STEP);
             
             getCommand().waitFor(2);
             getCommand().click(signIn_btn);
             log("Clicking of Sign in button",LogType.STEP);
             
             getCommand().waitFor(5);
             String signIn=getCommand().driver.getCurrentUrl();
             log("The URL after Sign in is:"+signIn,LogType.STEP);
             if(signIn.contains("loginSuccessful")) {
                    log("Login successfully",LogType.STEP);
             }
 
			else {
				log("sign in modal not displays", LogType.ERROR_MESSAGE);
				Assert.fail("sign in modal not displays");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifySignIn()
	{
		try
		{
			signIn("LoginData");
			
			log("Checking of My account option in hamburger menu after login",LogType.STEP);
			getCommand().isTargetPresent(hamBurger_Icon);
			getCommand().click(hamBurger_Icon);
			
			getCommand().scrollTo(link_MyAccount);
			
			if(getCommand().isTargetVisible(link_MyAccount) && getCommand().isTargetVisible(signOut_btn)) {
				log("Expected target My account link and Sign out links are available",LogType.STEP);
			}
			
			
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifySignout() throws InterruptedException
	{
		  try {
		    	
	           log("Verifying of SignOut functionality",LogType.STEP);
	           getCommand().waitForTargetPresent(hamBurger_Icon).click(hamBurger_Icon);
	           getCommand().waitFor(2);
	           log("Clicking of hamburger icon",LogType.STEP);
	           
	           WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='main-nav__buttons']/li[9]/a"));
		       JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
		       jse.executeScript("arguments[0].scrollIntoView(true);", element);
		       Thread.sleep(4000);
	        
	           getCommand().waitForTargetPresent(signOut_btn).click(signOut_btn);
	           getCommand().waitFor(10);
	           log("Clicking of Signout button",LogType.STEP);
	          
	           
	           String pagetitle=getCommand().getDriver().getTitle();
	           System.out.println("page title.."+pagetitle);
	           String current_url=getCommand().getDriver().getCurrentUrl();
	           log("URL after clicking sign out is:"+current_url,LogType.STEP);
	          
	           if(current_url.contains("DPSLogout=true")) {
	            	System.out.println("enter into if condition");
	                 log("LogOut successfully",LogType.STEP);
	           }
	            else{
	            	System.out.println("enter into else condition");
	                getCommand().captureScreenshot("Test\\build\\test-output\\IwafReport\\tests\\LogOut.png");
	           	    log("<a href=\"LogOut.png\">M: Log out is unsuccesful</a>" ,LogType.STEP);
	           		 
	            }
	           
	           
	    }
	    catch(Exception ex) {
	           ex.getMessage();
	    }
	    return this;
	}
	
	
	
	public Kohler_HomePage VerifyEditDelete_NotSignedIn() throws InterruptedException
	{
		try
		{
			String Text = "TestEdit";
	        log("Adding items to my kohler folder",LogType.STEP);
			
	        
	        WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='container-home']/div[3]/div/div/h2"));
	        JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
  	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
  	      
	        
	        getCommand().click(AddToFolder);
			
			getCommand().isTargetPresent(AddToFolderTextArea);
			getCommand().sendKeys(AddToFolderTextArea, Text);
			
			getCommand().scrollTo(AddToFolderTextArea);
			getCommand().click(AddToFolderSubmitButton);
			getCommand().waitFor(2);
			getCommand().isTargetPresent(AddToFolderContinueShopping);
			getCommand().click(AddToFolderContinueShopping);
			
			getCommand().waitFor(3);
			
			
			log("Clicking on My Kohler Folder",LogType.STEP);
			getCommand().click(hamBurger_Icon);
			getCommand().waitFor(2);
	        getCommand().waitForTargetPresent(link_myFolders).click(link_myFolders);
			getCommand().waitFor(3);
			getCommand().waitForTargetPresent(myKohler_Folder).click(myKohler_Folder);
			getCommand().waitFor(2);
			
			WebElement element1=getCommand().driver.findElement(By.xpath("(//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder'][1]//span)[1]"));
	        JavascriptExecutor jse1=(JavascriptExecutor)getCommand().driver;
  	       jse1.executeScript("arguments[0].scrollIntoView(true);", element1);
  	       Thread.sleep(3000);
  	     log("Clicking on Edit option",LogType.STEP);
  	       getCommand().click(edit);
  	       
  	       	getCommand().waitForTargetPresent(EditTextArea_Folder);
			getCommand().sendKeys(EditTextArea_Folder, "1");
			getCommand().scrollTo(EditSave_Folder).click(EditSave_Folder);
			
			String text_afteredit=getCommand().driver.findElement(By.xpath("(//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder'][1]//span)[9]")).getText();
			
			if(!Text.equals(text_afteredit)&&text_afteredit.equals(Text+"1")) {
				log("Edit is working as expected",LogType.STEP);
			}
			else
			{
				log("Edit is not working as expected",LogType.ERROR_MESSAGE);
				Assert.fail("Edit is not working as expected");
			}	
			//verify delete
			log("Seleting all items using select all check box",LogType.STEP);

			WebElement element2=getCommand().driver.findElement(By.xpath("//*[@id='my-folders']/div[2]/div/div/div[1]/div"));
	        JavascriptExecutor jse2=(JavascriptExecutor)getCommand().driver;
  	       jse2.executeScript("arguments[0].scrollIntoView(true);", element2);
  	      
			
			//getCommand().scrollTo(SelectAllCheckBox);
			getCommand().click(SelectAllCheckBox);
			getCommand().waitFor(3);
			
			log("Verifying delete action for an item",LogType.STEP);
			getCommand().click(Deletebtn_Folder);
			getCommand().waitForTargetPresent(DeleteConfirmation);
			getCommand().click(DeleteConfirmation);
			getCommand().waitFor(2);
			
			List<WebElement> items=getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div[2]/div/div/div/div[2]"));
			System.out.println("Available items in cart "+items.size());
			if(items.size()==0) {
				log("Delete action happened successfully",LogType.STEP);
			}
			else {
				log("Delete action not happened",LogType.STEP);
			}
			
			
			
			
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifyEditDelete_SignedIn(String Data) throws InterruptedException
	{
		try
		{
			String Text = "TestEdit";
			String FolderText = "TestFolderCreation";
	        log("Adding items to my kohler folder",LogType.STEP);
			
	        
	        WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='container-home']/div[3]/div/div/h2"));
	        JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
  	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
  	      
	        
	        getCommand().click(AddToFolder);
	        getCommand().waitFor(2);
	    
	        getCommand().click(AddToFolderDropDown);
	       
	        List<WebElement> Options = getCommand().driver.findElements(By.xpath("//*[@id='folderDropdown_msdd']/div[2]/ul/li/span"));
			
			for(WebElement Option : Options) {
				String dropdownText = Option.getText();					
				if(FolderText.equals(dropdownText)) 
				{
					log("Selecting the folder from drop down",LogType.STEP);
					Option.click();
					break;
				}
				
			}
			
	       
			getCommand().isTargetPresent(textArea);
			getCommand().sendKeys(textArea, Text);
			
			getCommand().click(AddToFolderSubmitButton);
			getCommand().waitFor(2);
			getCommand().click(ViewFolder);
			
			getCommand().waitFor(3);
		
				WebElement element1=getCommand().driver.findElement(By.xpath("(//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder'][1]//span)[1]"));
		        JavascriptExecutor jse1=(JavascriptExecutor)getCommand().driver;
	  	       jse1.executeScript("arguments[0].scrollIntoView(true);", element1);
	  	       Thread.sleep(3000);
	  	       log("Clicking on Edit option",LogType.STEP);
	  	       getCommand().click(edit);
	  	       
	  	       	getCommand().waitForTargetPresent(EditTextArea_Folder);
				getCommand().sendKeys(EditTextArea_Folder, "1");
				getCommand().scrollTo(EditSave_Folder).click(EditSave_Folder);
				
				String text_afteredit=getCommand().driver.findElement(By.xpath("(//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder'][1]//span)[9]")).getText();
				
				if(!Text.equals(text_afteredit)&&text_afteredit.equals(Text+"1")) {
					log("Edit is working as expected",LogType.STEP);
				}
				else
				{
					log("Edit is not working as expected",LogType.ERROR_MESSAGE);
					Assert.fail("Edit is not working as expected");
				}	
				//verify delete
				log("Seleting all items using select all check box",LogType.STEP);

				WebElement element2=getCommand().driver.findElement(By.xpath("//*[@id='my-folders']/div[2]/div/div/div"));
		        JavascriptExecutor jse2=(JavascriptExecutor)getCommand().driver;
	  	       jse1.executeScript("arguments[0].scrollIntoView(true);", element2);
				
							
				getCommand().click(selectAll);
				getCommand().waitFor(3);
				
				log("Verifying delete action for an item",LogType.STEP);
				getCommand().click(DeleteBtn);
				getCommand().waitForTargetPresent(DeleteConfirmation);
				getCommand().click(DeleteConfirmation);
				getCommand().waitFor(2);
				
				List<WebElement> items=getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div[2]/div/div/div/div[2]"));
				System.out.println("Available items in cart "+items.size());
				if(items.size()==0) {
					log("Delete action happened successfully",LogType.STEP);
				}
				else {
					log("Delete action not happened",LogType.STEP);
				}
				
				
			}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public static final Target new_Folder= new Target("new_Folder","//*[@id='my-folders']/div[3]/div/form/div[3]/p/a",Target.XPATH);

	public Kohler_HomePage VerifyMyFolderPageOptions(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String FolderText = search.FolderName1;
			String NewFolderText = search.FolderName2;
			log("Clicking on Myfolders button in hamburger menu",LogType.STEP);
			getCommand().waitForTargetPresent(hamBurger_Icon).click(hamBurger_Icon);
			
			log("Navigating to My Account Page", LogType.STEP);
			getCommand().waitForTargetPresent(link_myFolders).click(link_myFolders);
			
			getCommand().waitFor(3);
			log("Selecting My personal folders",LogType.STEP);

			SelectFolder(FolderText);
			
			Thread.sleep(3000);
			
			//Verify Share
			log("Checking Share folder option",LogType.STEP);
			
			
			List<WebElement> Shareoptions = getCommand().driver.findElements(By.xpath("/html/body/div[8]/div[4]"));
			log("Clicking on each share option and checking the navigation",LogType.STEP);
			for(WebElement Shareoption : Shareoptions)
			{			
				getCommand().waitForTargetPresent(ShareFolderOption);
				
				getCommand().click(ShareFolderOption);

				String Text = Shareoption.getText();
				
				log("Clicking on "+Text+" share option and checking the navigation",LogType.STEP);
				
				Shareoption.click();
				
				if(Text.equals("Email a Friend"))
				{
					String PopupText = getCommand().getText(Share_EmailAFriend);			

					if(PopupText.equals("EMAIL A FRIEND"))
					{
						log(PopupText+" window is displayed after clicking on "+Text+" share option",LogType.STEP);
						
						getCommand().click(Share_EmailAFriendClose);
					}
					
					else
					{
						log("No Window is not displayed after clicking on "+Text+" share option",LogType.STEP);
						Assert.fail("No Window is not displayed after clicking on "+Text+" share option");
					}
				}
				
				if(Text.equals("Share with a Showroom"))
				{
					String PopupText = getCommand().getText(Share_KohlerShowroom);			

					if(PopupText.equals("SHARE WITH A KOHLER SHOWROOM"))
					{
						log(PopupText+" window is displayed after clicking on "+Text+" share option",LogType.STEP);
						
						getCommand().click(Share_ShareKohlerClose);
					}
					
					else
					{
						log("No Window is displayed after clicking on "+Text+" share option",LogType.STEP);
						Assert.fail("No Window is displayed after clicking on "+Text+" share option");
					}
				}		
			}
			
			//Verify Copy
			
			log("Checking Copy folder option",LogType.STEP);
			
			log("Clicking on Copy option",LogType.STEP);
			
			List<WebElement> Items = getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div[1]/div[@class='add_to_cart_folder']"));
			
			getCommand().isTargetPresent(CopyFolderOption);
			
			getCommand().click(CopyFolderOption);
			
			
			log("Providing Folder Name",LogType.STEP);
			getCommand().sendKeys(FolderName, NewFolderText);
					
			getCommand().isTargetPresent(SaveAsNewFolder);
			
			log("Cliking on save",LogType.STEP);
			getCommand().click(SaveAsNewFolder);
			getCommand().waitFor(3);
			log("Navigating to new folder where the item are copied",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//*[@id='my-folders']/div[2]/div/div/div/div/a/span")).click();
			String after_save=getCommand().driver.findElement(By.xpath("//*[@id='my-folders']/div[3]/div/form/div[3]/p/a")).getText();
			if(after_save.equals(NewFolderText)) {
				log("Checking item are copied to new folder",LogType.STEP);
			}
			getCommand().scrollTo(new_Folder).click(new_Folder);
			
		
			SelectFolder(NewFolderText);
			
			Thread.sleep(4000);
			
			List<WebElement> Items_NEwFolder_Copied = getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div[1]/div[@class='add_to_cart_folder']"));
			
			log("Checking item are copied to new folder",LogType.STEP);
			
			Assert.assertEquals(Items_NEwFolder_Copied.size(), Items.size(), "Mismacth in items count. Copy page option is not working");
			
			log("Copy page option is working",LogType.STEP);
			
			//Verify delete
			
	        log("Checking Delete folder option",LogType.STEP);
			
			
			SelectFolder(FolderText);
			
			Thread.sleep(4000);
			
			log("Clicking on Delete option",LogType.STEP);
			
	        getCommand().isTargetPresent(DeleteFolderOption);
	        
	        getCommand().click(DeleteFolderOption);
	        
	        log("Clicking on Delete option from pop up",LogType.STEP);
	        
			getCommand().isTargetPresent(DeleteFolderConfirmation);
			
			getCommand().click(DeleteFolderConfirmation);
			
			
			log("Navigating to My folder and checking folder is deleted",LogType.STEP);
			
	        getCommand().mouseHover(BlueBanner);
			
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);
			
	        List<WebElement> FoldersName= getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/p/a"));
			
			for(WebElement Foldersname : FoldersName ) 
			{
				String name = Foldersname.getText();
				
				Assert.assertNotEquals(name, FolderText, "Delete is not working. Still deleted folder is visible under My folder");			
			}		
			
			log("Delete is working. Deleted folder is not visible under My folder",LogType.STEP);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	
	
	public Kohler_HomePage VerifyAccountEdit() 
	{	
		try
		{
			getCommand().waitForTargetPresent(hamBurger_Icon).click(hamBurger_Icon);
			
			log("Navigating to My Account Page", LogType.STEP);
			getCommand().waitForTargetPresent(link_MyAccount).click(link_MyAccount);
			
			getCommand().waitFor(3);
			
			
			
				
				String TextbeforeEdit = getCommand().getText(firstname);
				
				log("Clicking on Edit Account button in my Account Page", LogType.STEP);
				getCommand().click(EditAccount);
				
				getCommand().sendKeys(firstname_Edit, "s");	
				
				getCommand().click(SaveEditedAccount);
				getCommand().waitFor(2);
				
				if(getCommand().isTargetVisible(SaveSuccess))
				{
					Assert.assertEquals("Your Account Details Have Been Updated.", getCommand().getText(SaveSuccess),"Account success mesage is not as expected");
					
					String TextAfterEdit = getCommand().getText(ActualAccountFieldDetail);
					
					Assert.assertNotEquals(TextbeforeEdit, TextAfterEdit,"Editing the account is not working");
					log("Successfully updated", LogType.STEP);
				}
				
				else {
					Assert.fail("Account is not updated");
				}
				
				/*String Text = getCommand().getAttribute(AccountFieldtoEdit, "id");
				
				if(!Text.contains("describe-yourself") && !Text.contains("profile-country"))
				{
					getCommand().sendKeys(AccountFieldtoEdit, "s");				
				}
				
				else
				{
					if(Text.contains("profile-country"))
					{
						
						String text = getCommand().getText(AccountFieldtoEdit);
						getCommand().click(AccountFieldtoEdit);
						
						List<WebElement> Options  = getCommand().driver.findElements(By.xpath("//*[@id='account-details--edit']/div[3]/div/div[6]/div[2]/div/div[2]/div[2]/ul/li"));
						
						for(int i=1; i<=Options.size();i++) 
						{
							String actulatext = Options.get(i).getText();
							if(!text.equals(actulatext)) {
								Options.get(i).click();
								break;
							}
						}
					}
					
					if(Text.contains("describe-yourself"))
					{
						
						String text = getCommand().getText(AccountFieldtoEdit);
						getCommand().click(AccountFieldtoEdit);
						
						List<WebElement> Options  = getCommand().driver.findElements(By.xpath("//*[@id='account-details--edit']/div[3]/div/div[9]/div[2]/div/div[2]/div[2]/ul/li"));
						
						for(int i=1; i<=Options.size();i++) 
						{
							String actulatext = Options.get(i).getText();
							if(!text.equals(actulatext) && actulatext.equals("Consumer/Homeowner")) {
								Options.get(i).click();
								break;
							}
						}
					}
				}
				   WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='account-details--edit']/div[3]/div/div[1]/div[1]/div[1]/input[1]"));
			        JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
		  	       jse.executeScript("arguments[0].scrollIntoView(true);", element);
		  	       Thread.sleep(3000); 
				getCommand().click(SaveEditedAccount);
				
				if(getCommand().isTargetVisible(SaveSuccess))
				{
					Assert.assertEquals("Your Account Details Have Been Updated.", getCommand().getText(SaveSuccess),"Account success mesage is not as expected");
					
					String TextAfterEdit = getCommand().getText(ActualAccountFieldDetail);
					
					Assert.assertNotEquals(TextbeforeEdit, TextAfterEdit,"Editing the account is not working");
				}
				
				else {
					Assert.fail("Account is not updated");
				}*/
				
		}
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifyAddNewFolder(String Data) throws InterruptedException
	{
        try
        {
        	SearchData search = SearchData.fetch(Data);
        	
        	int min=10;int max=9999;
        	 Random random = new Random();
        	 int myvalue=random.ints(min,(max+1)).findFirst().getAsInt();
        	 System.out.println("random value..."+random.ints(min,(max+1)).findFirst().getAsInt());
        	 String myrandom_stringvalue=String.valueOf(myvalue);
        	
    		String Foldername = search.FolderName1+myrandom_stringvalue;
    		String FolderNote = "TestFolderCreation";
    		log("Clicking on Myfolders button in Hamburger",LogType.STEP);
    		getCommand().waitForTargetPresent(hamBurger_Icon).click(hamBurger_Icon);
			
			log("Navigating to My Account Page", LogType.STEP);
			getCommand().waitForTargetPresent(link_myFolders).click(link_myFolders);
			
			getCommand().waitFor(3);
    		if(getCommand().isTargetVisible(CreateFolder)) 
    		{
    			log("Able to access My Folder",LogType.STEP);
    			log("Clicking on Create folder in My Folder Page",LogType.STEP);
    			getCommand().isTargetPresent(CreateFolder);
    			getCommand().click(CreateFolder);
    			
    			log("Providing Folder Name",LogType.STEP);
    			getCommand().sendKeys(FolderName, Foldername);
    			
    			log("Providing Notes",LogType.STEP);
    			getCommand().sendKeys(CreateFolderNotes, FolderNote);
    			
    			
    			log("Clicking on Create button",LogType.STEP);
    			getCommand().click(btn_CreateFolder);
    			
    			getCommand().waitFor(5);
    			//List<WebElement> FoldersList = getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/div/a"));
    			log("Checking each folder available in my folder page and clicking on newely created folder",LogType.STEP);
    			List<WebElement> FoldersName= getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/p/a"));
    			
    			for(WebElement Foldersname : FoldersName ) 
    			{
    				String name = Foldersname.getText();
    				System.out.println("folder name..."+name);
    				if(name.equals(Foldername)) 
    				{
    					Foldersname.click();
    					log("Able to access created folder in my folder page",LogType.STEP);
    					Assert.assertEquals(Foldername, getCommand().getText(ActualFolderName),"Not able to access created folder");
    					break;	
    				}
    				
    			}
    			
    		}
    		
    		else 
    		{
    			log("My Folder is not accessable",LogType.ERROR_MESSAGE);
    			Assert.fail("My Folder is not accessable");
    		}
        }
		
        catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
        
		return this;
	}
	
	public Kohler_HomePage VerifyPromoModuleGrid() throws InterruptedException
	{		
		try
		{
			int PromoModuleLinkCounter = 0;
			
	        List<WebElement> PromoModuleLinks = getCommand().getDriver().findElements(By.xpath("//*[@class='container' ]/ul/li[@class='promotion']/a"));
	        
	        log("Getting Total No. of Promo Module links available on the grid",LogType.STEP);
		    
		    log("Total No. of Promo Module links available on the grid are: "+PromoModuleLinks.size(),LogType.STEP);
		    
		    Actions Action = new Actions(getCommand().getDriver());
		    
		    log("Verifying access of each Promo Module Link",LogType.STEP);
		    
		    String pageTitle = getCommand().driver.getTitle();
		

		    for(WebElement PromoModuleLink:PromoModuleLinks)
		    {	    	
		    	JavascripExecutor(PromoModuleLink,"Y","N");    	
		    	if(PromoModuleLink.isDisplayed()) 
		    	{		    			    		
		    		String LinkText = PromoModuleLink.getAttribute("href");
		    		log("Getting Help Text for Promo Module link: "+LinkText,LogType.STEP);

		    		Action.moveToElement(PromoModuleLink).build().perform();	    		
		    		
		    		String HelText = PromoModuleLink.getAttribute("title");	    			    		
		    		log("Help Text for promotion: "+LinkText+" is, "+HelText,LogType.STEP);
		    		
		    		log("Accessing Promo Module Link: "+LinkText+" in new tab",LogType.STEP);
		    		
	                String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
	                PromoModuleLink.sendKeys(selectLinkOpeninNewTab);
	                getCommand().waitFor(10);
	                ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                log("Switching to new tab",LogType.STEP);
	                getCommand().driver.switchTo().window(listofTabs.get(1));
		    		
	                log("Getting new tab page title",LogType.STEP);
		    		String CurrentpageTitle = getCommand().driver.getTitle();
		    	
		    		if(pageTitle.equals(CurrentpageTitle))
		    		{	    			
		    			log("Promo Module link: "+LinkText+" is not accessable",LogType.ERROR_MESSAGE);
		    		}
		    		else
		    		{    			
		    			log("Promo Module link: "+LinkText+" is accessable",LogType.STEP); 
		    			log("Page title after accessing Promo Module link: "+LinkText+" is, "+CurrentpageTitle ,LogType.STEP);
		    		}
		    		
		    		getCommand().driver.close();
		    		getCommand().driver.switchTo().window(listofTabs.get(0));
		    		
		    		PromoModuleLinkCounter++;
		    	}
		    	
		    	else
		    	{
		    		log("Promo Module link: "+PromoModuleLink.getAttribute("href")+ " is not visible",LogType.STEP);
		    	}
		    }
		    
		    if(PromoModuleLinkCounter == PromoModuleLinks.size())
		    {
		    	 log("Verification of Help text & access of each Promo Module Link is completed",LogType.STEP);
		    }
		    else
		    {
		    	log("Verification of Help text & access of each Promo Module Link is not comapleted.Mismatch in Actual promo linka and accessesd promo links",LogType.ERROR_MESSAGE);
		    }
		}
	 
	    catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifyDiscoverthePossibilities_NavCircles()
	{
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Total No. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid: "+SlickDots.size(),LogType.STEP);
			ArrayList<String> list = new ArrayList<String>();
			
			log("Clicking on each Slick dots(Nav Circles) in 'Discover the Possibilities' grid",LogType.STEP);
			for(int i = 0; i < SlickDots.size(); i++)
			{			
				SlickDots.get(i).click();
				String SlickDotClass = SlickDots.get(i).getAttribute("class");
				if(SlickDotClass.contains("active"))
				{
					int index=i+1;
					String SlickDotXpath = "//*[@id='container-home']/div[3]/div/div/div[1]/ul/li["+index+"]/button";
					String SlickDotText = getCommand().getDriver().findElement(By.xpath(SlickDotXpath)).getText();
					log(SlickDotText+"st Slick dots(Nav Circles) is active and moving to next Slick dots(Nav Circles)",LogType.STEP);
					list.add(SlickDotText);		
				}
			}	
			CompareDataFromSameList(list);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage CompareDataFromSameList(List<String> list)
	{
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{			
				if(list.get(i).equals(list.get(k)))
				{
					Assert.fail("Mismatch in data present in the list");
				}				      
			}	      
		}		
		return this;
	}
	
	public Kohler_HomePage VerifyDiscoverthePossibilities_Arrows()
	{
		try
		{
			 WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='container-home']/div[3]/div/div/h2"));
		        JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
	  	       	jse.executeScript("arguments[0].scrollIntoView(true);", element);
			List<WebElement> CarouselSlides = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div"));
			List<Target> TargetArrowElements = new ArrayList<Target>();
			TargetArrowElements.add(NextArrow_DiscoverthePossibilities);
			TargetArrowElements.add(PrevArrow_DiscoverthePossibilities);
			
			for(Target TargetArrowElement:TargetArrowElements)
			{
				String ElementText = getCommand().getText(TargetArrowElement);
				ArrayList<String> list = new ArrayList<String>();
				int i=0;
				log("Navigating to each carouselSlide using "+ElementText+" arrow and getting the title of each carouselSlide",LogType.STEP);
				for(WebElement CarouselSlide:CarouselSlides)
				{
					String IndexPosition = CarouselSlide.getAttribute("index");
					i=i+1;
					if(IndexPosition.equals("0") || IndexPosition.equals("1") ||  IndexPosition.equals("2"))
					{
						String Xpath = "//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+i+"]/div[1]";
						String Title = getCommand().getDriver().findElement(By.xpath(Xpath)).getAttribute("title");
						
						log("Title of the carouselSlide is: "+Title,LogType.STEP);
						list.add(Title);
						log("Clicking on "+ElementText+" arrow",LogType.STEP);					
						getCommand().waitForTargetPresent(TargetArrowElement);
						getCommand().click(TargetArrowElement);
					}
				}
				CompareDataFromSameList(list);
				log("Navigating to each carouselSlide using "+ElementText+" arrow is Working",LogType.STEP);
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifyDiscoverthePossibilities_LearnMore()
	{
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Lear more CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> LearnMore = getCommand().getDriver().findElements(By.xpath("//*[@class='carousel-cta__right']/a"));
			int i=0;
			for(WebElement Learnmore:LearnMore)
			{			
				if(Learnmore.isDisplayed())
				{
					String href = Learnmore.getAttribute("href");
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
					log("Opening Learn more CTA with link: "+href+" in new tab",LogType.STEP);
					Learnmore.sendKeys(selectLinkOpeninNewTab);
	                getCommand().waitFor(6);
	                ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                log("Switching to new tab",LogType.STEP);
	                getCommand().driver.switchTo().window(listofTabs.get(1));    		
	                log("Getting new tab page url",LogType.STEP);
		    		String Currentpageurl = getCommand().driver.getCurrentUrl();
		    		String[] parts = Currentpageurl.split("\\/");
		    		String ExpectedPartofpageurl = parts[2];    
		    	
		    		if(ExpectedPartofpageurl.equals("ideas.kohler.com"))
		    		{	    			
		    			log("Learn More CTA opens a ideas.kohler.com page",LogType.STEP);
		    		}
		    		else
		    		{    			
		    			log("Learn More CTA not opens a ideas.kohler.com page",LogType.ERROR_MESSAGE); 
		    			log("Page url after clicking on learn more CTA is: , "+Currentpageurl ,LogType.STEP);
		    		}
		    		
		    		getCommand().driver.close();
		    		getCommand().driver.switchTo().window(listofTabs.get(0));
		    		log("Moving to next slide of 'Discover the Possibilities' grid",LogType.STEP);
		    		i=i+1;
				}
				
				if(i<SlickDots.size())
				{
					SlickDots.get(i).click();
					getCommand().waitFor(5);
				}
				
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifyDiscoverthePossibilities_HotSpots_GetDetails() throws InterruptedException
	{
		try
		{
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));	
			int i=2;
			int j=2;
			int k=2;
			for(WebElement SlickDot : SlickDots)
			{
				SlickDot.click();			
				log("Getting Total no. of hot spots available in the slide",LogType.STEP);
				List<WebElement> HotSpots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+i+"]/div[1]/div/div"));
				int HotSpotsCount = HotSpots.size();
				log("Total " +HotSpotsCount+" hot spots available in the slide",LogType.STEP);
				log("Clicking on each hot spot",LogType.STEP);
				for(WebElement HotSpot : HotSpots) 
				{
					Thread.sleep(4000);
					JavascriptExecutor execute = (JavascriptExecutor)getCommand().driver;
					execute.executeScript("arguments[0].click();", HotSpot);
					String GetDetailsXpath = "//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+k+"]/div["+j+"]/div/p[2]/a";
					WebElement GetDetails = getCommand().driver.findElement(By.xpath(GetDetailsXpath));
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
					log("Opening get details link in new tab ",LogType.STEP);
					GetDetails.sendKeys(selectLinkOpeninNewTab);
					ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                log("Switching to new tab",LogType.STEP);
	                getCommand().driver.switchTo().window(listofTabs.get(1));    		
	                log("Getting new tab page url",LogType.STEP);                
		    		String Currentpageurl = getCommand().driver.getCurrentUrl();
		    		
		    		if(Currentpageurl.contains("productDetail")) {
		    			log("Product page is displayed",LogType.STEP);
		    		}
		    		else
		    		{
		    			log("product page is not displayed",LogType.ERROR_MESSAGE);
		    		}
		    		j++;
		    		getCommand().driver.close();
		    		getCommand().driver.switchTo().window(listofTabs.get(0));  		
				}
				j=2;
				k++;
				i++;
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifyDiscoverthePossibilities_HotSpots_StoreLocator() throws InterruptedException
	{
		try
		{
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));	
			int i=2;
			int j=2;
			int k=2;
			for(WebElement SlickDot : SlickDots)
			{
				SlickDot.click();			
				log("Getting Total no. of hot spots available in the slide",LogType.STEP);
				List<WebElement> HotSpots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+i+"]/div[1]/div/div"));
				int HotSpotsCount = HotSpots.size();
				log("Total " +HotSpotsCount+" hot spots available in the slide",LogType.STEP);
				log("Clicking on each hot spot",LogType.STEP);
				for(WebElement HotSpot : HotSpots) 
				{
					Thread.sleep(4000);
					JavascriptExecutor execute = (JavascriptExecutor)getCommand().driver;
					execute.executeScript("arguments[0].click();", HotSpot);
					String GetDetailsXpath = "//*[@id='container-home']/div[3]/div/div/div[1]/div/div/div["+k+"]/div["+j+"]/div/a";
					WebElement GetDetails = getCommand().driver.findElement(By.xpath(GetDetailsXpath));
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
					log("Opening Find a store link in new tab ",LogType.STEP);
					GetDetails.sendKeys(selectLinkOpeninNewTab);
					ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                log("Switching to new tab",LogType.STEP);
	                getCommand().driver.switchTo().window(listofTabs.get(1));    		
	                log("Getting new tab page url",LogType.STEP);                
		    		String Currentpageurl = getCommand().driver.getCurrentUrl();
		    		
		    		if(Currentpageurl.contains("storelocator")) {
		    			log("Store locator page is displayed",LogType.STEP);
		    		}
		    		else
		    		{
		    			log("Store locator page is not displayed",LogType.ERROR_MESSAGE);
		    		}
		    		j++;
		    		getCommand().driver.close();
		    		getCommand().driver.switchTo().window(listofTabs.get(0));  		
				}
				j=2;
				k++;
				i++;
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public Kohler_HomePage VerifyDiscoverthePossibilities_Share()
	{
		try
		{	
			  
	        WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='container-home']/div[3]/div/div/h2"));
	        JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
  	       	jse.executeScript("arguments[0].scrollIntoView(true);", element);
  	      
			//log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			//List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Share CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			WebElement Share = getCommand().getDriver().findElement(By.xpath("(//*[@class='carousel-cta__right']/button[2])[2]"));
			List<String> ActualSocialmedia = new ArrayList<String>();
			
			
				int ExpectedSocialMediaCount = 7;
			
				int ActualSocialMediaCount = 0;
				ActualSocialmedia.clear();
				if(Share.isDisplayed())
				{
					log("Share option is displayed",LogType.STEP);
					Share.click();
	                List<WebElement> ShareList = getCommand().driver.findElements(By.xpath("//*[@class ='share-tip-inner']/ul/li"));
		    		
		    		for(WebElement Sharelist : ShareList) {
		    			String SocialSiteText = Sharelist.getText();
		    			if(Sharelist.isDisplayed() && !SocialSiteText.isEmpty()) 
		    			{
		    				ActualSocialmedia.add(SocialSiteText);
		    				log(SocialSiteText+ " is displayed in share CTA of carousel slide in 'Discover the Possibilities' grid",LogType.STEP);
		    			}
		    			
		    			else 
		    			{
		    				log(SocialSiteText+ " is not displayed in share CTA of carousel slide in 'Discover the Possibilities' grid",LogType.STEP);
		    			}
		    		}
		    		
		    		for(String Actualsocialmedia : ActualSocialmedia)
		    		{
		    			if(Actualsocialmedia.equals("Pinterest") || Actualsocialmedia.equals("Houzz") || Actualsocialmedia.equals("Facebook") || Actualsocialmedia.equals("Google+") || Actualsocialmedia.equals("Email a friend") || Actualsocialmedia.equals("Twitter") || Actualsocialmedia.equals("Share with a Kohler Showroom"))
		    			{
		    				ActualSocialMediaCount++;	    				
		    			}
		    		}
		    		
		    		Assert.assertEquals(ExpectedSocialMediaCount, ActualSocialMediaCount, "Mismatch in media Count to be present in share CTA drop sown");
		    		
		    	
				}
				
			
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
   
	
	public Kohler_HomePage VerifyDiscoverthePossibilities_AddToFolder() throws InterruptedException
	{
		System.out.println("enter VerifyDiscoverthePossibilities_AddToFolder()....");
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Add to Folder CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> AddtoFolder = getCommand().getDriver().findElements(By.xpath("//*[@class ='carousel-cta__right']/button[1]"));
			List<String> ActualNoteText = new ArrayList<String>();
			int i=0;
			int j=0;
			int k=1;
			String Text = "Testnote";
			for(WebElement Addtofolder:AddtoFolder)
			{
				if(Addtofolder.isDisplayed()) {
					log("Clicking on Save to folder CTA of "+k+"st carousel slide in 'Discover the Possibilities' grid",LogType.STEP);
					Addtofolder.click();
					j=j+1;
					log("Adding Note and Saving to My folder",LogType.STEP);
					getCommand().waitFor(2);
					getCommand().isTargetPresent(AddToFolderTextArea);
					getCommand().sendKeys(AddToFolderTextArea,Text+j);
					System.out.println("text added..."+j);
					getCommand().isTargetPresent(AddToFolderSubmitButton);
					getCommand().click(AddToFolderSubmitButton);
					
					log("Clicking on Continue shopping and navigating to next slide",LogType.STEP);
					
					getCommand().waitFor(3);
					getCommand().click(AddToFolderContinueShopping);
					i++;
					k++;
				}

				if(i<SlickDots.size())
				{
					SlickDots.get(i).click();
					getCommand().waitFor(5);
				}
				
			}		
			
			
			//getCommand().executeJavaScript("arguments[0].click();", MyFolder);
			
			//Thread.sleep(4000);
			getCommand().waitFor(3);
			getCommand().waitForTargetPresent(hamBurger_Icon).click(hamBurger_Icon);
			getCommand().waitFor(2);
			getCommand().waitForTargetPresent(link_myFolders).click(link_myFolders);
		
			getCommand().waitFor(2);
			getCommand().driver.findElement(By.xpath("//*[@id='my-folders']/div[3]/div/div[2]/div/a/img")).click();
			List<WebElement> NoteText = getCommand().driver.findElements(By.xpath("//*[@class='add_to_cart_folder']/div/div[2]/div[2]/div/p/span[2]"));
			
			String notes_part1="(//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder'][";
			String notes_part2="]//span)[9]";
	for(int m=1;m<=NoteText.size();m++)
	{
		String notes_part3=notes_part1+m+notes_part2;
		System.out.println("final xpath...."+notes_part3);
		
		String app_get_notename=getCommand().driver.findElement(By.xpath(notes_part3)).getText();
		System.out.println("note names is..."+app_get_notename);
		
		if(app_get_notename.equalsIgnoreCase("Test1") || app_get_notename.equals("Test2") || app_get_notename.equals("Test3") )
		{
			log(app_get_notename+ " is added to myfolder",LogType.STEP);
		}else
		{
			log(app_get_notename+ " is not added to myfolder",LogType.STEP);
		}
		
	}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage VerifyDiscoverthePossibilities_AddToFolderSignin() throws InterruptedException
	{
		try
		{
			log("Getting Total no. of Slick dots(Nav Circles) available in 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> SlickDots = getCommand().getDriver().findElements(By.xpath("//*[@id='container-home']/div[3]/div/div/div[1]/ul/li"));
			log("Clicking on Add to Folder CTA available in each carousel slide of 'Discover the Possibilities' grid",LogType.STEP);
			List<WebElement> AddtoFolder = getCommand().getDriver().findElements(By.xpath("//*[@class ='carousel-cta__right']/button[1]"));
			//List<String> ActualNoteText = new ArrayList<String>();
			int i=0;
			int j=0;
			int k=1;
			String Text = "Test";
			String FolderText = "TestFolderCreation";
			for(WebElement Addtofolder:AddtoFolder)
			{
				if(Addtofolder.isDisplayed()) {
					log("Clicking on Save to folder CTA of "+k+"st carousel slide in 'Discover the Possibilities' grid",LogType.STEP);
					Addtofolder.click();
					Thread.sleep(4000);
					getCommand().executeJavaScript("arguments[0].click();", AddToFolderDropDown);
					
					List<WebElement> Options = getCommand().driver.findElements(By.xpath("//*[@id='folderDropdown_msdd']/div[2]/ul/li/span"));
					
					for(WebElement Option : Options) {
						String dropdownText = Option.getText();					
						if(FolderText.equals(dropdownText)) 
						{
							log("Selecting the folder from drop down",LogType.STEP);
							Option.click();
							break;
						}
						
					}
					
					j=j+1;
					log("Adding Note and Saving to selected folder",LogType.STEP);
					getCommand().isTargetPresent(AddToFolderTextArea_Signin);
					getCommand().sendKeys(AddToFolderTextArea_Signin, Text+j);
					
					getCommand().isTargetPresent(AddToFolderSubmitButton);
					getCommand().click(AddToFolderSubmitButton);
					
					log("Clicking on Continue shopping and navigating to next slide",LogType.STEP);
					
					getCommand().waitFor(5);
					getCommand().isTargetPresent(AddToFolderContinueShopping);
					getCommand().click(AddToFolderContinueShopping);
					i++;
					k++;
				}

				if(i<SlickDots.size())
				{
					SlickDots.get(i).click();
					getCommand().waitFor(5);
				}
				
			}		
			
			
			getCommand().waitFor(3);
			getCommand().waitForTargetPresent(hamBurger_Icon).click(hamBurger_Icon);
			getCommand().waitFor(2);
			getCommand().waitForTargetPresent(link_myFolders).click(link_myFolders);
			
		
			
		
			
			getCommand().waitFor(2);
			List<WebElement> FoldersName= getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/p/a"));
			System.out.println(FoldersName.size());
			
			for(WebElement Foldersname : FoldersName ) 
			{
				String name = Foldersname.getText();
				if(name.equals(FolderText)) 
				{
					Foldersname.click();				
					Assert.assertEquals(FolderText, getCommand().getText(ActualFolderName));
					log("Able to access created folder in my folder page",LogType.STEP);
				}
				break;	
			}		
			
			Thread.sleep(4000);
			
				
		//	List<WebElement> NoteText = getCommand().driver.findElements(By.xpath("//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder']"));
			
				int totla_notes_countis=getCommand().waitForTargetPresent(total_count_notes).getTargetCount(total_count_notes);	
				System.out.println("total notes count..."+totla_notes_countis);
				String notes_part1="(//div[@class='col-full-sm pad-b-30-sm display-none-md order-detail-table']//div[@class='add_to_cart_folder'][";
					String notes_part2="]//span)[9]";
			for(int m=1;m<=totla_notes_countis;m++)
			{
				String notes_part3=notes_part1+m+notes_part2;
				System.out.println("final xpath...."+notes_part3);
				
				String app_get_notename=getCommand().driver.findElement(By.xpath(notes_part3)).getText();
				System.out.println("note names is..."+app_get_notename);
				
				if(app_get_notename.equalsIgnoreCase("Testnote1") || app_get_notename.equals("Testnote2") || app_get_notename.equals("Testnote3") )
				{
					log(app_get_notename+ " is added to myfolder",LogType.STEP);
				}else
				{
					log(app_get_notename+ " is not added to myfolder",LogType.STEP);
				}
				
			}
			
			
			
			
			/*for(WebElement Notetext : NoteText) {
					
			String get_foldername=Notetext.getText();	
			System.out.println("foldername..."+get_foldername);
			
			if(get_foldername.equalsIgnoreCase("Test1") || get_foldername.equals("Test2") || get_foldername.equals("Test3") )
			{
				log(get_foldername+ " is added to myfolder",LogType.STEP);
			}else
			{
				log(get_foldername+ " is not added to myfolder",LogType.STEP);
			}
			}*/
				/*ActualNoteText.add(Notetext.getText());
			}
			Set<String> Set = new LinkedHashSet<>(ActualNoteText);*/
			
			/*for(String set : Set)
			{
				if(set.equals("Test1") || set.equals("Test2") || set.equals("Test3")) {
					log(set+ " is added to myfolder",LogType.STEP);
				}
				else {
					log(set+ " is not added to myfolder",LogType.ERROR_MESSAGE);
					Assert.fail(set+ " is not added to myfolder");
				}
			}
			*/
			log("Checking Folder layout", LogType.STEP);
		//	Assert.assertTrue(getCommand().isTargetVisible(MyFolderImage),"Folder Image is not present");
			log("Folder Image is present", LogType.STEP);
			Assert.assertTrue(getCommand().isTargetVisible(MyFolderCost),"Cost is not present");
			log("Cost is present", LogType.STEP);
			Assert.assertTrue(getCommand().isTargetVisible(MyFolderNotes),"Notes is not present");
			log("Notes is present", LogType.STEP);
	
			
			
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public Kohler_HomePage VerifyMyFoldersCopyAction_SignedIn(String Data) throws InterruptedException
	{	
		try
		{
			SearchData search = SearchData.fetch(Data);
			String FolderText = search.FolderName1;
			String Foldername = search.FolderName2;
			String FolderNote = "TestFolderCreation";
			log("Clicking on Myfolders button in hamburger menu",LogType.STEP);
			log("Clicking on Myfolders button in hamburger menu",LogType.STEP);
			getCommand().waitForTargetPresent(hamBurger_Icon).click(hamBurger_Icon);
			
			log("Navigating to My Account Page", LogType.STEP);
			getCommand().waitForTargetPresent(link_myFolders).click(link_myFolders);
			
			getCommand().waitFor(3);			
	if(getCommand().isTargetVisible(CreateFolder)) 
			{
				log("Able to access My Folder",LogType.STEP);
				log("Clicking on Create folder in My Folder Page",LogType.STEP);
				getCommand().isTargetPresent(CreateFolder);
				getCommand().click(CreateFolder);
				
				log("Providing Folder Name",LogType.STEP);
				getCommand().sendKeys(FolderName, Foldername);
				
				log("Providing Notes",LogType.STEP);
				getCommand().sendKeys(CreateFolderNotes, FolderNote);
				
				getCommand().waitFor(4);
				log("Clicking on Create button",LogType.STEP);
				//getCommand().click(btn_CreateFolder);
				
				getCommand().driver.findElement(By.xpath("//*[@id='createNewFolder']/button[1]")).click();
				
				//Thread.sleep(3000);
			}
			
			SelectFolder(FolderText);
			
			String Itemtext = getCommand().getText(ItemText);
			
			log("Select an item using check box",LogType.STEP);
			getCommand().isTargetPresent(CheckBox);
			
			getCommand().click(CheckBox);
			
			getCommand().isTargetPresent(CopyMyFolder_Signedin);
			
			getCommand().click(CopyMyFolder_Signedin);
			
			
			getCommand().executeJavaScript("arguments[0].click();", AddToFolderDropDown);
			
			List<WebElement> Options = getCommand().driver.findElements(By.xpath("//*[@id='folderDropdown_msdd']/div[2]/ul/li/span"));
			
			for(WebElement Option : Options) {
				String dropdownText = Option.getText();					
				if(Foldername.equals(dropdownText)) 
				{
					log("Selecting the folder from drop down",LogType.STEP);
					Option.click();
					break;
				}			
			}
			
	        getCommand().isTargetPresent(Add_CopyItem);
			
			getCommand().click(Add_CopyItem);
			
	      //  getCommand().mouseHover(BlueBanner);
			
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);
			
			SelectFolder(Foldername);
			
			String ItemtextFromCopiedFolder = getCommand().getText(ItemText);
			
			Assert.assertEquals(ItemtextFromCopiedFolder, Itemtext, "Copy is not working.");
			
			log("Copy is working",LogType.STEP);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
		
	public Kohler_HomePage VerifyMyFoldersMoveAction_SignedIn(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String FolderText = search.FolderName1;
			String Foldername = search.FolderName2;
			log("Clicking on Myfolders button in Home Page",LogType.STEP);
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);

			SelectFolder(FolderText);
			
			List<WebElement> Items = getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div[1]/div[@class='add_to_cart_folder']"));
			
			log("Select an item using check box",LogType.STEP);
			
			String itemtext = getCommand().getText(SecondItemText);
			getCommand().isTargetPresent(SecondCheckBox);
			
			getCommand().click(SecondCheckBox);
			
			getCommand().mouseHover(MoveMyFolder_Signedin);
			
			getCommand().isTargetPresent(MoveMyFolder_Signedin);
			
			getCommand().click(MoveMyFolder_Signedin);
			
			
			getCommand().executeJavaScript("arguments[0].click();", AddToFolderDropDown);
			
			List<WebElement> Options = getCommand().driver.findElements(By.xpath("//*[@id='folderDropdown_msdd']/div[2]/ul/li/span"));
			
			for(WebElement Option : Options) {
				String dropdownText = Option.getText();					
				if(Foldername.equals(dropdownText)) 
				{
					log("Selecting the folder from drop down",LogType.STEP);
					Option.click();
					break;
				}			
			}
			
	        getCommand().isTargetPresent(Add_CopyItem);
			
			getCommand().click(Add_CopyItem);
			
			List<WebElement> ItemsAfterMoved = getCommand().driver.findElements(By.xpath("//*[@id='my-folders-detail__items']/div[1]/div[@class='add_to_cart_folder']"));
			
			Assert.assertNotEquals(Items.size(), ItemsAfterMoved.size(),"Item from Main folder is not removed after move action");
			
	        getCommand().mouseHover(BlueBanner);
			
			getCommand().executeJavaScript("arguments[0].click();", MyFolder);
			
			SelectFolder(Foldername);
			
			String itemtextfromMovedFolder = getCommand().getText(SecondItemText);
			
			Assert.assertEquals(itemtextfromMovedFolder, itemtext, "Move is not working.");
			
			log("Move is working",LogType.STEP);
		}
				
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public Kohler_HomePage SelectFolder(String FolderText) throws InterruptedException
	{
       try
       {
    	 List<WebElement> FoldersName= getCommand().driver.findElements(By.xpath("//*[@id='my-folders']/div[3]/div/form/div/p/a"));
   		
   		for(WebElement Foldersname : FoldersName ) 
   		{
   			String name = Foldersname.getText();
   			if(name.equals(FolderText)) 
   			{
   				Foldersname.click();				
   				Assert.assertEquals(FolderText, getCommand().getText(ActualFolderName));
   				log("Able to access created folder with items in my folder page",LogType.STEP);
   				break;
   			}
   		}		
   		
   		Thread.sleep(3000);
       }

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	
	public Kohler_HomePage VerifyKohlerIdeaslayout_PromoImages()
	{
		try
		{
			log("Verifying of image display in KCC",LogType.STEP);
			getCommand().scrollTo(KCC);
			
			boolean img=getCommand().driver.findElement(By.xpath("(//*[@class='curalate-thumbnail'])[1]")).isDisplayed();
			if(img&&!getCommand().isTargetVisible(KCC_LeftArrow)) 
			{
				getCommand().isTargetVisible(KCC_RightArrow);
				getCommand().isTargetVisible(viewGallery_CTA);
				getCommand().isTargetVisible(submitPhoto_CTA);
				
				log("KCC section elements are verified",LogType.STEP);
				//getCommand().click(submitPhoto_CTA);

				//getCommand().driver.switchTo().frame(getCommand().driver.findElement(By.id("curalate-photo-picker")));
				
				//getCommand().waitForTargetPresent(KohlerIdeaslayout_Uploader);
				//getCommand().isTargetPresent(KohlerIdeaslayout_Uploader);
				
				
			}
			else {
				 log("KCC iage is not displayed",LogType.STEP);
			}
		    
		   
		    
		
		 }
		   

	    catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}

		return this;
	}
	
	public Kohler_HomePage VerifyKohlerIdeaslayout_Arrows()
	{		
				 try {
				
				getCommand().waitFor(3);
		         getCommand().waitForTargetVisible(KCC).scrollTo(KCC);
		         boolean arrow=getCommand().isClickable(KCC_RightArrow);
		         System.out.println(arrow);
		         getCommand().click(KCC_RightArrow);
		         
		         if(getCommand().isTargetVisible(KCC_LeftArrow)) {
		                         System.out.println("Left arrow displays after clicking of right arrow");
		         }
		         else {
		                         getCommand().captureScreenshot("Test\\build\\test-output\\IwafReport\\tests\\KCC.png");
		                           log("<a href=\"KCC.png\">M :Left arrow is not displayed</a>",LogType.STEP);
		         }
		}
		catch(Exception ex) {
		         ex.getMessage();
		}
		         
		         return this;

		
	}
	
	public Kohler_HomePage VerifyKohlerIdeaslayout_ButonsAndLinks()
	{
		try
		{
			getCommand().scrollTo(KCC);
			getCommand().waitForTargetPresent(viewGallery);
			getCommand().isTargetPresent(viewGallery);
			log("View Gallery is present",LogType.STEP);
			getCommand().waitForTargetPresent(submitPhoto_CTA);
			getCommand().isTargetPresent(submitPhoto_CTA);
			log("Submit a Photo is present",LogType.STEP);
			
			getCommand().sendKeys(submitPhoto_CTA,Keys.chord(Keys.CONTROL,Keys.RETURN));
			ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
			getCommand().waitFor(1);
			getCommand().driver.switchTo().window(listofTabs.get(1));
			

			//getCommand().driver.switchTo().frame(getCommand().driver.findElement(By.id("curalate-photo-picker")));
			
			getCommand().waitForTargetPresent(KohlerIdeaslayout_Uploader);
			getCommand().isTargetPresent(KohlerIdeaslayout_Uploader);
			log("Uploader page is displayed",LogType.STEP);
			getCommand().driver.close();
	        getCommand().driver.switchTo().window(listofTabs.get(0));
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public Kohler_HomePage VerifyKohlerIdeaslayout_ToolTip()
	{
		try
		{
			Actions Action = new Actions(getCommand().getDriver());
		    
		    WebElement Tootip = getCommand().driver.findElement(By.className("curalate-hover-username"));
		    
		    List<WebElement> listImages = getCommand().getDriver().findElements(By.className("curalate-thumbnail"));
		    
		    WebElement NextButton = getCommand().getDriver().findElement(By.xpath("//*[@id=\"curalate-content\"]/button[2]"));
		    
		    log("Total No. of Images exist in #KohlerIdeas layout: "+listImages.size(),LogType.STEP);

		    for(WebElement Images:listImages)
		    {
		    	
		    	String id = Images.getAttribute("id");
				char ch='"';
				String Xpath_ToolTip = "//*[@id="+ch+id+ch+"]/div/div/div/div";	
		    	
		    	if(Images.isDisplayed()) 
		    	{
		    		if(!Tootip.isDisplayed())
	    		    {
					    Action.moveToElement(Images).perform();
					    if(getCommand().driver.findElement(By.xpath(Xpath_ToolTip)).isDisplayed())
					    {
					    	String TooltipText = getCommand().driver.findElement(By.xpath(Xpath_ToolTip)).getText();
					    	if(TooltipText.equals("@"+TooltipText))
					    	{
					    		log("@"+TooltipText+" displays on hovering on image.",LogType.STEP);
					    	}
					    	else
					    	{
					    		log("@"+TooltipText+" is not as expected format text on hovering on image.",LogType.ERROR_MESSAGE);
					    	}
						    
					    }			    
					    Action.moveToElement(NextButton).perform();		
		    		}	    	   
		    	}	
		    	
		    	if(!Images.isDisplayed()) 
		    	{
		    		NextButton.click();
		    		if(!Tootip.isDisplayed())
	    		    {
		    			Action.moveToElement(Images).perform();
					    if(getCommand().driver.findElement(By.xpath(Xpath_ToolTip)).isDisplayed())
					    {
					    	String TooltipText = getCommand().driver.findElement(By.xpath(Xpath_ToolTip)).getText();
					    	if(TooltipText.equals("@"+TooltipText))
					    	{
					    		log("@"+TooltipText+" displays on hovering on image.",LogType.STEP);
					    	}
					    	else
					    	{
					    		log("@"+TooltipText+" is not as expected format text on hovering on image.",LogType.ERROR_MESSAGE);
					    	}
						    
					    }			    
					    Action.moveToElement(NextButton).perform();			    		    			    
		    		}	    	   
		    	}
		    }
		}

	    catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
	    
		return this;
	}
	
	public Kohler_HomePage VerifyViewGalleryScreen()
	{
		
		WebElement ViewGallery = getCommand().driver.findElement(By.className("curalate-view-gallery"));
		
		if(ViewGallery.isDisplayed())
		{
			ViewGallery.click();
			String PageTitle = getCommand().driver.findElement(By.className("curalate-header-copy")).getText();
			log(PageTitle+" is displayed",LogType.STEP);
		}
		
		return this;
	}
	public Kohler_HomePage VerifySubmitAPhotoCTA() {
		
		try {
			log("verifying of clicking of sumbit a photo CTA ",LogType.STEP);
			
			getCommand().scrollTo(KCC);
			getCommand().waitForTargetPresent(submitPhoto_CTA).click(submitPhoto_CTA);
			ArrayList<String> newTab = new ArrayList<String> (getCommand().driver.getWindowHandles());
			
			getCommand().driver.switchTo().window(newTab.get(1));
			String pageTitle = getCommand().driver.getTitle();
			log("verify the Page title"+ pageTitle,LogType.STEP);
			log("Verifying of media uploader model",LogType.STEP);
			getCommand().driver.findElement(By.xpath("//*[contains(text(),'Camera Roll')]")).isDisplayed();
			getCommand().driver.findElement(By.xpath("//*[contains(text(),'Facebook')]")).isDisplayed();
			getCommand().driver.findElement(By.xpath("//*[contains(text(),'Instagram')]")).isDisplayed();
			log("Multi step uploader models verified",LogType.STEP);
			getCommand().driver.switchTo().window(newTab.get(0));
	        
		}
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		
		
		return this;
		
		
	}
	
	public Kohler_HomePage VerifyKohlerIdeas_OverlayDisplay()
	{
		try
		{
			getCommand().scrollTo(KCC);
			/*String Backgroundcolor= "rgba(100, 100, 100, 0.8)";
		    
			String Style= "block";
			*/
	        List<WebElement> listImages = getCommand().getDriver().findElements(By.className("curalate-thumbnail"));
		    
		    log("Total No. of Images exist in #KohlerIdeas layout: "+listImages.size(),LogType.STEP);
		    
		    for(WebElement Images:listImages)
		    {
		    	if(Images.isDisplayed()) 
		    	{
		    		Images.click();
		    		getCommand().driver.findElement(By.xpath("//*[@class='curalate-photo-detail-container active']")).isDisplayed();
		    		 log("After clicking image the overlay is diaplayed",LogType.STEP);
		    		//CheckOverlayDisplay(OverlayDisplay,Backgroundcolor,Style);
		    		getCommand().driver.findElement(By.xpath("(//button[@class='curalate-modal-close'])[2]")).isDisplayed();
		    		 log("Close button 'x' is displayed",LogType.STEP);
		    		 List<WebElement> arrow=	 getCommand().driver.findElements(By.className("//*[contains(@class,'curalate-products-carousel')]"));
		    		 int count=arrow.size();
		    		 if(count>=2) {
		    			 log("The right and left arrows are displayed",LogType.STEP);
		    		 }
		    		 else {
		    			 log("The right and left arrows are not displayed",LogType.STEP);
		    		 }
		    		
		    	}
		    	
		    	else
		    	{
		    		getCommand().waitForTargetPresent(KCC_RightArrow);
		    		getCommand().click(KCC_RightArrow);
		    		Images.click();
		    	   getCommand().driver.findElement(By.xpath("//*[@class='curalate-photo-detail-container active']")).isDisplayed();
		    		 log("After clicking image the overlay is diaplayed",LogType.STEP);
		    		getCommand().driver.findElement(By.xpath("(//button[@class='curalate-modal-close'])[2]")).isDisplayed();
		    		log("Close button 'x' is displayed",LogType.STEP);
		    		 List<WebElement> arrow=	 getCommand().driver.findElements(By.className("//*[contains(@class,'curalate-products-carousel')]"));
		    		 int count=arrow.size();
		    		 if(count>=2) {
		    			 log("The right and left arrows are displayed",LogType.STEP);
		    		 }
		    		 else {
		    			 log("The right and left arrows are not displayed",LogType.STEP);
		    		 }
		    			    		
		    	}
		    }
		}
		
	    catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
	    
		return this;
	}
	

	
	public Kohler_HomePage CheckOverlayDisplay(WebElement Element, String BackGroundColor, String ElementStyle)
	{
		try
		{
			String Backgroundcolor= Element.getCssValue("background-color");
			String Style= Element.getAttribute("style");
			
			if(Backgroundcolor.equals(BackGroundColor) && Style.contains(ElementStyle))
			{
				log("Overlay Displays after clicking on image in #KohlerIdeas layout: ",LogType.STEP);    		    
			}
			
			else
			{
				log("Overlay popup is not Displayed after clicking on image in #KohlerIdeas layout: ",LogType.ERROR_MESSAGE);    
			}
			GetOverlayDisplayContent();
			CheckShareElementsFunctionality_OverlayDisplay();
			CheckCrossElementsFunctionality_OverlayDisplay();		
			if(!Style.contains("block"))
			{
				log("Overlay popup dissapear after clicking on cross in Overlay Display of KohlerIdeas",LogType.STEP);			
			}
		} 	
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public Kohler_HomePage GetOverlayDisplayContent() 
	{
		try
		{
			List<Target> TargetElements = new ArrayList<Target>();
			TargetElements.add(OverlayDisplayCross);
			TargetElements.add(OverlayDisplay_RightArrow);
			TargetElements.add(OverlayDisplay_UserName);
			TargetElements.add(OverlayDisplay_Share);
			TargetElements.add(OverlayDisplay_Share_F);
			TargetElements.add(OverlayDisplay_Share_T);
			TargetElements.add(OverlayDisplay_Share_P);
			TargetElements.add(OverlayDisplay_ShopHeader);		
			TargetElements.add(OverlayDisplay_SmallProductImage);
			TargetElements.add(OverlayDisplay_SmallProductDescription);
			TargetElements.add(OverlayDisplay_SmallProductNxt);
			TargetElements.add(OverlayDisplay_SmallProductPrev);
			
			CheckElementsVisibility(TargetElements,"Y","Y",OverlayDisplay_LeftArrow,OverlayDisplay_RightArrow);
		}
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public Kohler_HomePage CheckElementsVisibility(List<Target> TargetElements,String IsCheckMultipleElements, String IsCheckLeftArrow, Target LeftElement, Target RightElement)
	{
		try
		{
			if(IsCheckMultipleElements=="Y") 
			{
				for(Target Target:TargetElements)
			    {
					getCommand().waitForTargetPresent(Target);
					if(getCommand().isTargetPresent(Target))
					{
						log("Target Element is visible",LogType.STEP);			
					}
					
					else
					{
						log("Target Element is not visible",LogType.ERROR_MESSAGE);		
						Assert.fail("Target Element is not visible");
					}
			    }
			
			}		
			if(IsCheckLeftArrow=="Y")
			{
				getCommand().waitForTargetPresent(RightElement);
	    		getCommand().mouseHover(RightElement);
	    		getCommand().click(RightElement);
	    		
	    		getCommand().waitForTargetPresent(LeftElement);
				if(getCommand().isTargetPresent(LeftElement))
				{
					log("Target Element is visible",LogType.STEP);			
				}
				
				else
				{
					log("Target Element is not visible",LogType.ERROR_MESSAGE);		
					Assert.fail("Target Element is not visible");
				}
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage CheckCrossElementsFunctionality_OverlayDisplay() 
	{
		try
		{
			log("Checking cross Functionalit in Overlay Display of KohlerIdeas",LogType.STEP);
			getCommand().waitForTargetPresent(OverlayDisplayCross);
			getCommand().mouseHover(OverlayDisplayCross);
			getCommand().click(OverlayDisplayCross);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}		
		return this;
	}
	
	public Kohler_HomePage CheckShareElementsFunctionality_OverlayDisplay()
	{
		try
		{
//			getCommand().waitForTargetPresent(OverlayDisplay_Share);
//			if(getCommand().isTargetPresent(OverlayDisplay_Share))
//			{
//				String Sharetext = getCommand().getText(OverlayDisplay_Share);
//				log("Checking Share Header text: " +Sharetext,LogType.STEP);
//				
//				if(Sharetext==ShareText)
//				{
//					log("Share Header text is displayed as: " +Sharetext,LogType.STEP);	
//				}
//			}		
			List<Target> TargetShareElements = new ArrayList<Target>();
			TargetShareElements.add(OverlayDisplay_Share_F);
			TargetShareElements.add(OverlayDisplay_Share_T);
			TargetShareElements.add(OverlayDisplay_Share_P);
			
			log("Verifying new Window page opens after clicking on each share button",LogType.STEP);	
			for(Target Target:TargetShareElements)
		    {			
				getCommand().waitForTargetPresent(Target);
				getCommand().mouseHover(Target);
				getCommand().click(Target);
				
				String OldWindow = getCommand().getCurrentWindowID();
				
				WindowHandles();
				getCommand().driver.switchTo().window(OldWindow);
		    }
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_HomePage WindowHandles()
	{
		try
		{
			int Win_size = getCommand().driver.getWindowHandles().size();
			log("Open Windows Size is :" + Win_size ,LogType.STEP);
			ArrayList<String> Windows = new ArrayList<String> (getCommand().driver.getWindowHandles());
			
			for(int i=0;i<Windows.size();i++)
			{
				getCommand().driver.switchTo().window(Windows.get(i));
				String pageTitle = getCommand().driver.getTitle();
				log("Page title is :"+pageTitle ,LogType.STEP);				
			}
			getCommand().driver.close();
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public Kohler_HomePage JavascripExecutor(WebElement Element, String IsScrollIntoView, String Click)
	{
		try
		{
			if(IsScrollIntoView.equals("Y"))
			{
				JavascriptExecutor js = (JavascriptExecutor) getCommand().getDriver(); 
				js.executeScript("arguments[0].scrollIntoView(true);", Element);
			}
			
			if(Click.equals("Y"))
			{
				JavascriptExecutor js = (JavascriptExecutor) getCommand().getDriver(); 
				js.executeScript("arguments[0].click();", Element);
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	public static final Target viewGallery_btn = new Target("viewGallery_btn","//*[@id='curalate-fan-reel']/div[4]/a",Target.XPATH);
	public static final Target arrow_Forward = new Target("arrow_Forward","//*[@class='buttonWrap grid__item one-whole']//button[2]",Target.XPATH);

	public Kohler_HomePage verifyHelpUsToImproveMore(String text, String feedback)
	{
		
		
	
	       log("Verify the Help Us Improve This Site link",LogType.STEP);
	       try {
	                    
	                    log("Verify the clicking of Help Us Improve This Site link",LogType.STEP);
	                    
	                     getCommand().scrollTo(KCC);
	                     getCommand().waitFor(5);
	                    // getCommand().scrollTo(viewGallery_btn);
	             
	                     getCommand().click(Link_HelpUsToImproveMore);
	                     getCommand().waitFor(10);
	                    
	                    ArrayList<String> tabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
	                    getCommand().driver.switchTo().window(tabs2.get(1));
	                   String pageTitle = getCommand().driver.getTitle();
	                  
	                  log("Page title is:" + pageTitle,LogType.STEP);
	                  Assert.assertEquals(pageTitle, "Kohler US Study", "Title mismatch");
	                  
	                  log("Select the type of feedback" , LogType.STEP);
	                  
	                  switch (feedback) {
	                    case "Suggestion": 
	                                 getCommand().waitForTargetVisible(btn_suggestion);
	                                 getCommand().click(btn_suggestion);
	                           break;
	                    case "Dislike": 
	                           getCommand().waitForTargetVisible(btn_dislike);
	                        getCommand().click(btn_dislike);
	                        break;
	                    case "Praise": 
	                           getCommand().waitForTargetVisible(btn_praise);
	                           getCommand().click(btn_praise);
	                       break;

	                    default:
	                           break;
	                    }
	                  
	                  
	                  log("Give the Comment in the text box" , LogType.STEP);
	                  getCommand().sendKeys(text_commenttextbox,text);
	                  getCommand().scrollTo(arrow_Forward).click(arrow_Forward);
	                  
	                  log("Select the rating based on feedback" , LogType.STEP);
	                  getCommand().waitForTargetVisible(btn_Rating);
	                  getCommand().click(btn_Rating);
	                  getCommand().waitFor(2);
	                  getCommand().scrollTo(arrow_Forward).click(arrow_Forward);
	                  getCommand().waitFor(5);
	                
	                  log("Click on Close the window button after submitting the feedback" , LogType.STEP);
	                  getCommand().waitForTargetVisible(btn_closeTheWind);
	                  getCommand().click(btn_closeTheWind);
	                  
	                  log("Switch back to the main window from the Feedback window" , LogType.STEP);
	                  getCommand().driver.switchTo().window(tabs2.get(0));
	                
	                     
	              
	       }catch(Exception ex)
	       {
	    	   Assert.fail(ex.getMessage());

	       }
	       
	       return this;
	}
	
//--------------------------------------------------Search----------------------------------------------------------
public static final Target arm_Dropdown  = new Target("arm_Dropdown","//*[@id='search-hero-select']/select",Target.XPATH);




public Kohler_HomePage verify_SearchArm() {
	try {
		log("Searching with the keyword Arm",LogType.STEP);
		
		getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
		getCommand().sendKeys(search_Input, "Arm");
		getCommand().click(search_btn);
		getCommand().waitFor(10);
		if(getCommand().isTargetVisible(arm_Dropdown)) {
			log("Drop down is displayed",LogType.STEP);
			getCommand().click(arm_Dropdown);
			
		}
		
	}
	catch(Exception ex) {
		  Assert.fail(ex.getMessage());
	}
	return this;
}

public static final Target two_Link=new Target("two_Link","//*[@id='search-hero']/div/div[1]/h2[2]/a",Target.XPATH);
public Kohler_HomePage verify_Tw() {
	
	try {
		getCommand().waitForTargetVisible(search_Icon).click(search_Icon);
		getCommand().sendKeys(search_Input, "tw");
		getCommand().click(search_btn);
		getCommand().waitFor(10);
		String text=getCommand().driver.findElement(By.xpath("//*[@id=search-hero']/div/div[1]/h2[2]")).getText();
		System.out.println(text);
		Assert.assertEquals(text, "Did you mean two?","Text is displayed");
		
		getCommand().click(two_Link);
		getCommand().waitFor(5);
		String pageTitle=getCommand().getPageTitle();
		System.out.println(pageTitle);
	
	
		
	}
	catch(Exception ex) {
		 Assert.fail(ex.getMessage());
	}
	return this;
}

}