package com.components.yaml;

import com.iwaf.framework.BasePage;

public class NewsLetterSignUp  {
	
	public String EmailId;
	public String FirstName;
	public String LastName;
	public String PostCode;
	public String Country;
	public String Occupation;
		
	public static NewsLetterSignUp fetch(String key){
	BasePage pageObj = new BasePage();
	NewsLetterSignUp obj = pageObj.getCommand().loadYaml(key, "data-pool/NewsLetterSignUp.yaml");
		return obj;
	}
}
