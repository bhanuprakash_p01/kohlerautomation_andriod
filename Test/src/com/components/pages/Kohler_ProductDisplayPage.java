package com.components.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.components.repository.SiteRepository;
import com.components.yaml.LoginData;
import com.components.yaml.SearchData;
import com.iwaf.framework.components.Target;
import com.iwaf.framework.components.IReporter.LogType;

public class Kohler_ProductDisplayPage extends SitePage
{
	
	
	/* Defining the locators on the Page */ 
	
	public static final Target Search_HomePage  = new Target("Search_HomePage","//*[@id='nav-searchbox']",Target.XPATH);
	
	public static final Target Searchbtn_HomePage  = new Target("Searchbtn_HomePage","//*[@id='header__button--search-button-desktop']/i",Target.XPATH);
	
	public static final Target PDP_Thubnailsdownarrow  = new Target("PDP_Thubnailsdownarrow","//*[@id='js-productDetail-imageSwitcherView']/div[3]/div/button",Target.XPATH);
	
	public static final Target StoreLocator_PDPPage  = new Target("StoreLocator_PDPPage","//*[@id='product-detail__tier']/div/div[1]/div/div/div/div/div[2]/button",Target.XPATH);
	
	public static final Target StoreLocatorHeader  = new Target("StoreLocatorHeader","//*[@id='store-locator']/div/h1",Target.XPATH);
	
	public static final Target CompareButton  = new Target("StoreLocatorHeader","//*[@id='product-detail__compare_wrapper']/div[3]/button",Target.XPATH);
	
	public static final Target CompareOverlay  = new Target("CompareOverlay","//*[@id='compare-products-bar__dropdown-button']/span[1]",Target.XPATH);
	
	public static final Target CompareOverlayProductdetails  = new Target("CompareOverlayProductdetails","//*[@id='compare-product--1']/div/p[2]",Target.XPATH);
	
	public static final Target CompareOverlay_ClearAll  = new Target("CompareOverlay_ClearAll","//*[@id='compare-products-bar__clear-all-button']",Target.XPATH);
	
	public static final Target PDP_AddToCart  = new Target("PDP_AddToCart","//*[@id='productDetails']/div[3]/button[1]",Target.XPATH);	
	
	public static final Target PDP_AddToCartModal  = new Target("PDP_AddToCartModal","//*[@id='modal--added-to-cart']/div",Target.XPATH);
	
	public static final Target PDP_AddToCartModalHeader  = new Target("PDP_AddToCartModalHeader","//*[@id='modal--added-to-cart']/div/div[1]/div[2]/h5",Target.XPATH);
		
	public static final Target PDP_AddToCartContinueShopping  = new Target("PDP_AddToCartContinueShopping","//*[@id='added-to-cart']/div/div/footer/div/button",Target.XPATH);
		
	public static final Target PDP_CartIconFlag  = new Target("PDP_CartIconFlag","//*[@id='tray-cart-subtotal']/span/span[1]",Target.XPATH);
	
	public static final Target PDP_CartIcon = new Target("PDP_CartIcon","(//*[@id='tray-cart-subtotal'])[1]",Target.XPATH);
	
	public static final Target CartPage_Header = new Target("CartPage_Header","//*[@id='shopping-cart']/div[1]/div/h1",Target.XPATH);
		
	public static final Target Productdetail_CartPage = new Target("Productdetail_CartPage","//*[@id='shopping-cart']/div[2]/div[1]/ol/li[1]/div/div[2]/div/span/a",Target.XPATH);
	
	public static final Target ContinueShopping_CartPage = new Target("ContinueShopping_CartPage","//*[@id='cart-continue-shopping-button']",Target.XPATH);
	
	public static final Target PairsWell = new Target("PairsWell","//*[@id='product-detail__pairs-well-with']/h2",Target.XPATH);
	
	public static final Target AddRequiredItems = new Target("AddRequiredItems","//*[@id='product-detail__add-required-items-button']",Target.XPATH);
	
	public static final Target RequiredItemsHeader = new Target("RequiredItemsHeader","//*[@id='modal--required-items']/div/h4",Target.XPATH);
	
	public static final Target SubTotal = new Target("SubTotal","//*[@id='product-detail__required-items-sub-total']/span[2]",Target.XPATH);
		
	public static final Target DiscontinuedCrousel = new Target("DiscontinuedCrousel","//*[@id='product-detail__carousel-hero-slides']/div/div/div/div[1]/div",Target.XPATH);
	
	
	/* Defining the locators for Comparing products */
	
	public static final Target Kitchens = new Target("Kitchens","//*[@id='main-nav__button--kitchen']",Target.XPATH);
	
	public static final Target Sinks = new Target("Sinks","//*[@id='sub-nav-tab--kitchen']/div/div/div/div[1]/ul/li[2]/a",Target.XPATH);
	
	public static final Target ProductMaterial = new Target("ProductMaterial","//*[@id='article-page__article-panels']/div/div[1]/div[1]/a/img",Target.XPATH);
	
	public static final Target Compare_btn = new Target("Compare_btn","//*[@id='compare-products-bar__dropdown-button-container']/a",Target.XPATH);
	
	public static final Target CompareModal_Header = new Target("CompareModal_Header","//*[@id='modal--compare-products']/div/h2",Target.XPATH);

	/* Defining the locators for SignIn */
	public static final Target btn_SigninHomePage  = new Target("btn_SigninHomePage","//*[@id='sign-in-bar__inner']/button[2]",Target.XPATH);
	public static final Target SignInModalPopup  = new Target("SignInModalPopup","//*[@id='modal--sign-in']/div",Target.XPATH);
	public static final Target EmailAddress  = new Target("EmailAddress","//*[@id='traySignInForm']/fieldset/div/input[@id='email']",Target.XPATH);
	public static final Target Password  = new Target("Password","//*[@id='traySignInForm']/fieldset/div/input[@id='password']",Target.XPATH);
	public static final Target btn_Signin  = new Target("btn_Signin","//*[@id='trayProfileSignIn']",Target.XPATH);
//##########################################Mobile xpaths#############################################################
	public static final Target search_Input=new Target("search_Input","//*[@id='search']",Target.XPATH);
	public static final Target search_btn=new Target("search_btn","//*[@id='header__button--search-button-mobile']",Target.XPATH);
	public static final Target search_Icon=new Target("search_Icon","(//*[@class='icon--search'])[1]",Target.XPATH); 
	      
	
	
	
	public Kohler_ProductDisplayPage(SiteRepository repository)
	{
		super(repository);
	}

	/* Functions on the Page are defined below */
	
	public Kohler_ProductDisplayPage atkohlerproductdisplaypage()
	{
		log("Launched Kohler Site",LogType.STEP);
		//getCommand().captureScreenshot("C:\\Users\\Arvind01\\Desktop\\Add To Cart\\HomePage.png");
		return this;
	}
	
	public Kohler_ProductDisplayPage VerifyPDPpage(String Data)
	{
		try
	       {	
	              SearchData search = SearchData.fetch(Data);
	              
	              String productID = search.ProductSku;
	              String expectedTitle = search.ExpectedTitle;
	              System.out.println("product id is...."+productID);
	              
	           
	              getCommand().waitForTargetPresent(search_Icon).click(search_Icon);
	              log("Clicking of search icon from the home page",LogType.STEP);
	              
	              getCommand().waitForTargetPresent(search_Input).sendKeys(search_Input, productID);
	              log("Enter the valid product id in the search input",LogType.STEP);
	              
	              getCommand().click(search_btn);
	              log("Click of search button after entering the product Id",LogType.STEP);
	              
	              getCommand().waitFor(5);
	              
	              String ActualTitle = getCommand().getPageTitle();
	              log("The current page title is:"+ActualTitle,LogType.STEP);
	              
	              String SkuDetails = "K-"+productID;
	              
	              String SkuFromTitle = ActualTitle.substring(0, 9);
	              log("Verifying The Sku/product number in the page title is feature as (K-14660-4 or K-14660-4-CP)",LogType.STEP);
	              Assert.assertEquals(SkuDetails, SkuFromTitle, "The Sku/product number in the page title is not feature as (K-14660-4 or K-14660-4-CP). Expected: "+ SkuDetails+ ", Actula: "+SkuFromTitle);
	              log("Verifying page title",LogType.STEP);
	             
	              getCommand().captureScreenshot("Test\\build\\test-output\\IwafReport\\tests\\pagetitlenotmatch.png");
	      	      log("<a href=\"pagetitlenotmatch.png\">M: Page title is Mismatch.</a>" ,LogType.STEP);
	      		 
	              
	              Assert.assertEquals(ActualTitle, expectedTitle, "The titles are mismatched. Expected: "+ expectedTitle+ ", Actual: "+ActualTitle);
	              
	              log("Page title is as expected",LogType.STEP);
	       }
	       
	       catch(Exception ex) {
	              Assert.fail(ex.getMessage());
	       }
	       
	       return this;
	}
	
	
	/*public Kohler_ProductDisplayPage VerifyBreadcrumbs_StoreLocator_PDPpage(String Data)
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			
		    String ProductID = search.ProductSku;
			//String ExpectedTitle = search.ExpectedTitle;
			Search(ProductID);
			
			List<WebElement> BreadCrumbs = getCommand().driver.findElements(By.xpath("//*[@id='breadcrumb-navigation']/div/a"));
			String pageTitle = getCommand().getPageTitle();
			for(WebElement BreadCrumb : BreadCrumbs)
			{
				String BreadCrumbText = BreadCrumb.getText();
				BreadCrumbText = BreadCrumbText.substring(0, BreadCrumbText.length() - 2);
				String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
				BreadCrumb.sendKeys(selectLinkOpeninNewTab);
	            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
	            log("Switching to new tab",LogType.STEP);
	            getCommand().driver.switchTo().window(listofTabs.get(1));
	    		
	            log("Getting new tab page title",LogType.STEP);
	    		String CurrentpageTitle = getCommand().driver.getTitle();
	    	
	    		if(!pageTitle.equals(CurrentpageTitle))
	    		{	    			
	    			log("Clicking on breadcrumb "+BreadCrumbText+" redirecting correctly",LogType.STEP);
	    		}
	    		else
	    		{    			
	    			log("Clicking on breadcrumbs links not redirecting correctly",LogType.ERROR_MESSAGE);
	    			Assert.fail("Clicking on breadcrumbs links not redirecting correctly");
	    		}
	    		
	    		getCommand().driver.close();
	    		getCommand().driver.switchTo().window(listofTabs.get(0));
			}
			
			getCommand().isTargetPresent(StoreLocator_PDPPage);
			getCommand().click(StoreLocator_PDPPage);
			
			String StorelocatorpageTitle = getCommand().driver.getTitle();
			
			if(!pageTitle.equals(StorelocatorpageTitle) && StorelocatorpageTitle.contains(getCommand().getText(StoreLocatorHeader)))
			{	    			
				log("Clicking on Find a store redirecting correctly",LogType.STEP);
			}
			else
			{    			
				log("Clicking on Find a store not redirecting correctly",LogType.ERROR_MESSAGE);
				Assert.fail("Clicking on Find a store not redirecting correctly");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}*/
	
	/*public Kohler_ProductDisplayPage VerifyCompareOverlayDisplayandDissapear(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
		    String ProductID = search.ProductSku;	    
		    log("Searching for product: "+ ProductID,LogType.STEP);
			Search(ProductID);
			
			Thread.sleep(3000);
			log("Clicking on Compare button in PDP page",LogType.STEP);
			getCommand().isTargetPresent(CompareButton);
			getCommand().click(CompareButton);
			
	        List<WebElement> CompareProductsCount = getCommand().driver.findElements(By.xpath("//*[@id='compare-products-bar__dropdown-container']/div"));
	        log("Checking Compare Overlay is displayed after clicking on +Compare button",LogType.STEP);
			if(getCommand().isTargetVisible(CompareOverlay) && CompareProductsCount.size()==3 )
			{
				log("Compare Overlay is displayed",LogType.STEP);
				
				String Text = getCommand().getText(CompareOverlayProductdetails);
				
				//Text = Text+"-0";
				
				Assert.assertEquals(Text, ProductID, "Mismatch in Product that was added for comapre");
				
				log("Product is added to comapare",LogType.STEP);
			}
			
			else
			{
				log("Compare Overlay is not displayed",LogType.ERROR_MESSAGE);
				Assert.fail("Compare Overlay is not displayed");
			}
			
			log("Clicking on Clear all option from Compare overlay display",LogType.STEP);
			getCommand().isTargetPresent(CompareOverlay_ClearAll);
			
			getCommand().click(CompareOverlay_ClearAll);
			
			log("Checking Compare overlay is dissapeared after Clicking on Clear all option",LogType.STEP);
			if(!getCommand().isTargetVisible(CompareOverlay))
			{
				log("Compare Overlay is dissapear after clicking on clearall",LogType.STEP);
			}
			
			else
			{
				log("Compare Overlay is not dissapear after clicking on clear all",LogType.ERROR_MESSAGE);
				Assert.fail("Compare Overlay is not dissapear after clicking on clear all");
			}
		}

		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}*/
	
	public static final Target cart_Count  = new Target("cart_Count","//*[@id='tray-cart-subtotal']/span/span",Target.XPATH);
	public Kohler_ProductDisplayPage VerifyAddtoCart_PDPPage(String Data) throws InterruptedException
	{
		try
		{
			String pageTitle = getCommand().getPageTitle();
			
	        SearchData search = SearchData.fetch(Data);
		    String ProductID = search.ProductSku;
		    log("Searching for product: "+ ProductID,LogType.STEP);
			Search(ProductID);
			getCommand().waitFor(10);
			
			String CartFlagText = getCommand().getText(cart_Count);
			
			log("Checking Add To Cart button is displayed in PDP page",LogType.STEP);
			if(getCommand().isTargetVisible(PDP_AddToCart)) 
			{
				log("Add To Cart button is displayed in PDP page",LogType.STEP);
				
				log("Clicking on Add To Cart button is displayed in PDP page",LogType.STEP);
				
				getCommand().click(PDP_AddToCart);
				
				String Text =  getCommand().getText(PDP_AddToCartModalHeader);
				
				log("Checking Add To Cart modal is displayed",LogType.STEP);
				if(getCommand().isTargetVisible(PDP_AddToCartModal) && Text.equals("ADDED TO CART"))
				{
					log("Add To Cart modal popup is displayed and product added to cart after clicking on Add to cart button",LogType.STEP);
					log("Clicking on continue shopping inadd to cart modal",LogType.STEP);
					   WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='added-to-cart']/div/div/footer/div/button"));
                       JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
                       jse.executeScript("arguments[0].scrollIntoView(true);", element);
                       Thread.sleep(3000);
                       
                       
					
					getCommand().click(PDP_AddToCartContinueShopping);
				}
				
				else
				{
					log("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button",LogType.ERROR_MESSAGE);
					Assert.fail("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button");
				}
				
				String CartFlagtextafterAddingproduct = getCommand().getText(PDP_CartIconFlag);
				
				log("Checking Quantity of 1 display in the cart icon flag on to top right corner of the page after adding product to cart",LogType.STEP);
				if(getCommand().getText(PDP_CartIconFlag).equals("1") && !CartFlagtextafterAddingproduct.equals(CartFlagText))
				{
					log("Quantity of 1 display in the cart icon flag on to top right corner of the page after adding product to cart",LogType.STEP);
				}
				
				else
				{
					log("Quantity of 1 is not display in the cart icon flag on to top right corner of the page after adding product to cart",LogType.ERROR_MESSAGE);
					Assert.fail("Quantity of 1 is not display in the cart icon flag on to top right corner of the page after adding product to cart");
				}
				
				
				log("Clicking on Cart icon and checking product 5588-0 displays on the page",LogType.STEP);
				
				getCommand().isTargetPresent(PDP_CartIcon);
				
				getCommand().click(PDP_CartIcon);
				
				String Cartpageheadder = getCommand().getText(CartPage_Header);
				
				if(Cartpageheadder.equals("Shopping Cart")) 
				{
					log("Cart page displays after clicking on cart icon",LogType.STEP);
					
					String ProductDetails = getCommand().getText(Productdetail_CartPage);
					
					Assert.assertEquals(ProductDetails, ProductID, "Mismatch in product. Product Displayed: "+ ProductDetails + " and expected: "+ProductID);
					
					log(ProductID + " is dispalyed in cart page",LogType.STEP);
					
					log("Clicking on continue shopping in cart page",LogType.STEP);
					
					   WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='shopping-cart']/div[4]/div/div/div[1]/div[1]"));
                       JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
                       jse.executeScript("arguments[0].scrollIntoView(true);", element);
                       Thread.sleep(3000);

					
					getCommand().click(ContinueShopping_CartPage);
					
					String pagetitle = getCommand().getPageTitle();
					
					Assert.assertEquals(pagetitle, pageTitle, "Clciking on continue shopping in cart page not navigating to home page");
				}
				else
				{
					log("Cart page is not displayed after clicking on cart icon",LogType.STEP);
					Assert.fail("Cart page is not displayed after clicking on cart icon");
				}		
			}
			
			else
			{
				log("Add To Cart button is not displayed in PDP page",LogType.ERROR_MESSAGE);
				Assert.fail("Add To Cart button is not displayed in PDP page");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public static final Target findPro_PDP  = new Target("findPro_PDP","//*[@id='product-detail__parts-and-resources']/div[3]/div[1]/div[6]/div[1]/p/a",Target.XPATH);





	
	public Kohler_ProductDisplayPage VerifyOptions_PDPPage(String Data) throws InterruptedException
	{	
        try
        {
        	SearchData search = SearchData.fetch(Data);
    	    String ProductID = search.ProductSku;
    	    log("Searching for product: "+ ProductID,LogType.STEP);
    		Search(ProductID);
    		
    		getCommand().waitFor(10);
    		getCommand().scrollTo(findPro_PDP);
    		String Text = getCommand().getText(PairsWell);
    		
    		if(getCommand().isTargetVisible(PairsWell) && Text.equals("PAIRS WELL WITH"))
    		{
    			log("pairs well with options displays", LogType.STEP);
    			
    			List<WebElement> Options = getCommand().driver.findElements(By.xpath("//*[@id='product-detail__pairs-well-with']/div/a"));
    			
    			log("pairs well is displays with "+Options.size()+" Options", LogType.STEP);
    		}
    		
    		else
    		{
    			log("pairs well with options is not displays", LogType.ERROR_MESSAGE);
    			Assert.fail("pairs well with options is not displays");
    		}
    				
    		log("Checking Required items displayed for product: "+ProductID, LogType.STEP);
    		
    		WebElement element=getCommand().driver.findElement(By.xpath("//*[@id='msdrpdd20_title']"));
            JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
            jse.executeScript("arguments[0].scrollIntoView(true);", element);
            Thread.sleep(3000);

    		
    		if(getCommand().isTargetVisible(AddRequiredItems))
    		{
    			log("Required items displays for product: "+ProductID, LogType.STEP);
    			
    			log("Clicking on Add required items button", LogType.STEP);
    			
    			getCommand().isTargetPresent(AddRequiredItems);
    			
    			getCommand().click(AddRequiredItems);
    			
    			List<WebElement> IncludeItemsList = getCommand().driver.findElements(By.xpath("//*[@id='required-items']/div[@class='additional-item row marg-b-30-sm pad-b-30-sm row--bottom-border']"));
    			
    			String Header = getCommand().getText(RequiredItemsHeader);
    			
    			if(Header.equals("REQUIRED ITEMS"))
    			{
    				log("Required items modal displayed with "+IncludeItemsList.size()+" items", LogType.STEP);
    			}
    			
    			else
    			{
    				log("Required items modal not displays with items", LogType.ERROR_MESSAGE);
    				Assert.fail("Required items modal not displays with items");
    			}
    			
    		}
    		
    		else
    		{
    			log("Required items not displays for product: "+ProductID, LogType.ERROR_MESSAGE);
    			Assert.fail("Required items not displays for product: "+ProductID);
    		}
        }
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}

	/*public Kohler_ProductDisplayPage VerifyCompareproductModal() throws InterruptedException
	{
		try
		{
			log("Clicking on Kitchens and navigating to sinks section",LogType.STEP);
			
			getCommand().isTargetPresent(Kitchens);
			
			getCommand().click(Kitchens);
			
			Thread.sleep(2000);
			
			getCommand().isTargetPresent(Sinks);
			
			getCommand().click(Sinks);
			
			Thread.sleep(2000);
			
			log("Clicking on cast iron sink material",LogType.STEP);
			
			getCommand().isTargetPresent(ProductMaterial);
			
			getCommand().click(ProductMaterial);
			
			
			log("Adding products for compare",LogType.STEP);
			
	        int productscount = 1;
			
			List<WebElement> Products = getCommand().driver.findElements(By.xpath("//*[@id='product-category__product-panels']/div"));
		    
			for(WebElement Product : Products)
			{
				if(productscount<=3)
				{
					Actions Action = new Actions(getCommand().driver);
					
					Action.moveToElement(Product).build().perform();
					
					String Xpath = "//*[@id='product-category__product-panels']/div["+productscount+"]/div/a[2]";
					
					WebElement Compare = getCommand().driver.findElement(By.xpath(Xpath));
					
					if(Compare.isDisplayed())
					{
						Compare.click();
						Thread.sleep(2000);
					}
					productscount++;
				}			
			}
			
			List<WebElement> CompareProductsCount = getCommand().driver.findElements(By.xpath("//*[@id='compare-products-bar__dropdown-container']/div"));
			
			String Compare = getCommand().getText(CompareOverlay);
			
			if(CompareProductsCount.size()==3 && Compare.equals("COMPARE PRODUCTS"))
			{
				log("Compare panel displayed with three products",LogType.STEP);
				
				log("Clicking om Compare button in Compare overlay display",LogType.STEP);
				
				getCommand().isTargetPresent(Compare_btn);
				
				getCommand().click(Compare_btn);
				
				Thread.sleep(3000);
				
				log("Checking Compare modal displayed",LogType.STEP);
				
				String Text = getCommand().getText(CompareModal_Header);
				
				List<WebElement> CompareModalProducts = getCommand().driver.findElements(By.xpath("//*[@id='compareItem']/div[1]/div/div/a"));
				
				if(Text.equals("COMPARE PRODUCTS") && CompareModalProducts.size()==3)
				{
					log("Compare modal displayed after cliking on compare button",LogType.STEP);
				}
				
				else
				{				
					log("Compare modal is not displayed after cliking on compare button", LogType.ERROR_MESSAGE);
					Assert.fail("Compare modal is not displayed after cliking on compare button");
				}
			}
			
			else
			{
				
				log("Compare panel is not displayed with three products", LogType.ERROR_MESSAGE);
				Assert.fail("Compare panel is not displayed with three products");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	*/
	public Kohler_ProductDisplayPage DiscontinuedProduct(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String ProductID = search.ProductSku;
			String ProductID2 = search.ProductSku1;
			log("Searching for product: "+ ProductID,LogType.STEP);
		    Search(ProductID);
				
			Thread.sleep(3000);
			
			log("Checking Discontinued product detail page displays for product"+ ProductID,LogType.STEP);
			
			String text = getCommand().getText(DiscontinuedCrousel);
			
			if(getCommand().isTargetVisible(DiscontinuedCrousel) && text.equals("Discontinued"))
			{
				log("Discontinued product detail page displays for product"+ ProductID,LogType.STEP);
			}
			
			else
			{
				log("Discontinued product detail page is not displays for product"+ ProductID, LogType.ERROR_MESSAGE);
				Assert.fail("Discontinued product detail page is not displays for product"+ ProductID);
			}
			
			
			log("Checking Discontinued product Links to replacement parts (if applicable)"+ ProductID,LogType.STEP);
			
			log("Searching for product: "+ ProductID2,LogType.STEP);
		    Search(ProductID2);
				
			Thread.sleep(3000);
			
			List<WebElement> Links = getCommand().driver.findElements(By.xpath("//*[@id='product-detail__features']/div[2]/p/a"));
			
			if(Links.size()>0)
			{
				log("Links to replacement parts are present",LogType.STEP);
				
				String pageTitle = getCommand().getPageUrl();
				
				for(WebElement Link : Links)
				{
					String LinkText = Link.getText();
					String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
					Link.sendKeys(selectLinkOpeninNewTab);
		            Thread.sleep(4000);
		            ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
		            log("Switching to new tab",LogType.STEP);
		            getCommand().driver.switchTo().window(listofTabs.get(1));
		    		
		            log("Getting new tab page url",LogType.STEP);
		    		String CurrentpageTitle = getCommand().getPageUrl();
		    		
		    		log("Checking "+LinkText+" to replacement parts is navigated to respective page",LogType.STEP);
		    	
		    		if(pageTitle.equals(CurrentpageTitle))
		    		{	    			
		    			log("Link "+LinkText+" to replacement parts is not working",LogType.ERROR_MESSAGE);
		    			Assert.fail("Link "+LinkText+" to replacement parts is not working");
		    		}
		    		else
		    		{    			
		    			log("Link "+LinkText+" to replacement parts is navigated to respective page",LogType.STEP); 
		    		}
		    		
		    		getCommand().driver.close();
		    		getCommand().driver.switchTo().window(listofTabs.get(0));
				}

			}
			
			else
			{
				log("Links to replacement parts are not present",LogType.ERROR_MESSAGE);
				Assert.fail("Links to replacement parts are not present");
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
	public Kohler_ProductDisplayPage SellableServicepart(String Data) throws InterruptedException
	{
		try
		{
			SearchData search = SearchData.fetch(Data);
			String ProductID = search.ProductSku;
			log("Searching for product: "+ ProductID,LogType.STEP);
		    Search(ProductID);
				
			Thread.sleep(3000);
			
			log("Checking Add To Cart button is displayed in PDP page",LogType.STEP);
			if(getCommand().isTargetVisible(PDP_AddToCart)) 
			{
				log("Add To Cart button is displayed in PDP page",LogType.STEP);
				
				log("Clicking on Add To Cart button",LogType.STEP);
				
				getCommand().click(PDP_AddToCart);
				
				String Text =  getCommand().getText(PDP_AddToCartModalHeader);
				
				log("Checking Add To Cart modal is displayed",LogType.STEP);
				if(getCommand().isTargetVisible(PDP_AddToCartModal) && Text.equals("ADDED TO CART"))
				{
					log("Add To Cart modal popup is displayed and product added to cart after clicking on Add to cart button",LogType.STEP);
					log("Clicking on continue shopping in add to cart modal",LogType.STEP);
					
					getCommand().scrollTo(PDP_AddToCartContinueShopping);
					
					getCommand().click(PDP_AddToCartContinueShopping);
				}			
				else
				{
					log("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button",LogType.ERROR_MESSAGE);
					Assert.fail("Add To Cart modal popup is not displayed and product not added to cart after clicking on Add to cart button");
				}		
			}
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;		
	}
	public static final Target findStore_btn  = new Target("findStore_btn","//*[@id='product-detail__compare_wrapper']/div[2]/button",Target.XPATH);

	public Kohler_ProductDisplayPage verify_StoreLocatorPage(String Data)  throws InterruptedException {
		
		try {
			SearchData search = SearchData.fetch(Data);
			String ProductID = search.ProductSku;
			log("Searching for product: "+ ProductID,LogType.STEP);
			Search(ProductID);
			
			WebElement element=getCommand().driver.findElement(By.xpath("(//*[@id='product-detail__add-required-items-button'])[1]"));
            JavascriptExecutor jse=(JavascriptExecutor)getCommand().driver;
            jse.executeScript("arguments[0].scrollIntoView(true);", element);
            
            if(getCommand().isTargetVisible(findStore_btn)) {
            	log("Clicking of find a store button and check the correct navigation page",LogType.STEP);
            	getCommand().click(findStore_btn);
            	getCommand().waitFor(3);
            	String pageURl=getCommand().getPageUrl();
            	if(pageURl.contains("storelocator")) {
            		log("Navigated to the store locator page",LogType.STEP);
            	}
            	else {
            		log("Navigated to the wrong page",LogType.STEP);
            	}
            	
            }
            
           
		}
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		
		return this;
	}
	
 	public Kohler_ProductDisplayPage signIn(String Data)
	{		
 		 LoginData loginCred=LoginData.fetch(Data);
 	    
 	    try {
 	                 log("SignIn to the application",LogType.STEP);
 	                 String userName=loginCred.UserName;
 	                 String passWord=loginCred.Password;
 	           
 	                  getCommand().waitForTargetPresent(Kohler_HomePage.hamBurger_Icon).click(Kohler_HomePage.hamBurger_Icon);
 	                  getCommand().scrollTo(Kohler_HomePage.link_myFolders);
 	             
 	                 getCommand().waitForTargetPresent(Kohler_HomePage.link_signIn).click(Kohler_HomePage.link_signIn);
 	                 getCommand().waitFor(3);
 	                 log("Clicking of Signin link from the hamburegr menu",LogType.STEP);
 	                 getCommand().scrollTo(Kohler_HomePage.password_Input);
 	                 
 	                 getCommand().sendKeys(Kohler_HomePage.userName_Input, userName);
 	                 getCommand().sendKeys(Kohler_HomePage.password_Input, passWord);
 	                 log("Entered the User name and Password",LogType.STEP);
 	                 
 	                 getCommand().waitFor(2);
 	                 getCommand().click(Kohler_HomePage.signIn_btn);
 	                 log("Clicking of Sign in button",LogType.STEP);
 	                 
 	                 getCommand().waitFor(5);
 	                 String signIn=getCommand().driver.getCurrentUrl();
 	                 log("The URL after Sign in is:"+signIn,LogType.STEP);
 	                 if(signIn.contains("loginSuccessful")) {
 	                        log("Login successfully",LogType.STEP);
 	                 }
 	     
 	           
 	    }
 	    catch(Exception ex) {
 	           ex.getMessage();
 	    }
 	    return this;
	}
 	

	public Kohler_ProductDisplayPage Search(String ProductID)
	{
		try
		{
			getCommand().isTargetPresent(search_Icon);
			log("providing the required product sku details in search box",LogType.STEP);
			getCommand().click(search_Icon);
			
			getCommand().sendKeys(search_Input, ProductID);
			
			getCommand().isTargetPresent(search_btn);
			log("Clicking on search button",LogType.STEP);
			getCommand().click(search_btn);
		}
		
		catch(Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
}
