package com.components.pages;

import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.components.repository.SiteRepository;
import com.components.yaml.DTVData;
import com.components.yaml.LoginData;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;
import com.gargoylesoftware.htmlunit.javascript.host.dom.Document;
import com.iwaf.framework.components.IReporter.LogType;
import com.iwaf.framework.components.Target;

public class Kohler_DTVPage extends SitePage {
	
	public Kohler_DTVPage (SiteRepository repository)
	{
		super(repository);
	}
	
	/* Defining the locators on the Page */
	public static final Target dtv_Title = new Target("dtv_Title","/html/head/title", Target.XPATH);
	public static final Target gn_Bathroom = new Target("gn_bathroom","//*[@id=\"main-nav__button--bathroom\"]",Target.XPATH);
	public static final Target ExploreAll = new Target("ExploreAll","//*[@id='section']/div[2]/div/div/div[2]/a",Target.XPATH);
	public static final Target car_DTV = new Target("car_DTV","//*[@id=\"section__carousel-hero\"]/div/div/div[2]/div/div/div/div/div/h2", Target.XPATH);
	public static final Target share_DTV = new Target("share_DTV","//*[@id='shareHouzz']/img",Target.XPATH);
	public static final Target car_nav_DTV = new Target("car_nav_DTV","//*[@id=\"section__carousel-hero\"]/ul/li[1]/button", Target.XPATH);
	public static final Target share_fb_DTV = new Target("share_fb_DTV","//*[@id=\"shareFacebook\"]/a/img", Target.XPATH);
	public static final Target share_pin_DTV = new Target("share_pin_DTV","//*[@id=\"sharePinterest\"]/a/img", Target.XPATH);
	public static final Target share_houzz_DTV = new Target("share_houzz_DTV","//*[@id=\"shareHouzz\"]/img", Target.XPATH);
	public static final Target img_DTV = new Target("img_DTV","//*[@id=\"dtv-promo\"]/div[1]/h2/img",Target.XPATH);
	public static final Target discover_CTA_DTV = new Target("discover_CTA_DTV","//*[@id=\"dtv-promo\"]/div[1]/p[2]/a",Target.XPATH);
	public static final Target enter_zip_DTV = new Target("enter_zip_DTV","//*[@id=\"dtv-promo-form\"]/input[2]",Target.XPATH);
	public static final Target btn_store_locator_DTV = new Target("btn_store_locator_DTV","//*[@id=\"dtv-promo-form\"]/button",Target.XPATH);
	public static final Target scrubber_div_DTV = new Target("scrubber_DTV","//*[@id=\"dtv-details-content\"]",Target.XPATH);
	public static final Target scrubber_restart_DTV = new Target("scrubber_Restart_DTV","//*[@id=\"detailsScrubberReset\"]/span",Target.XPATH);
	public static final Target scrubber_DTV = new Target("scrubber_DTV","//*[@id=\"detailsScrubber\"]",Target.XPATH);
	public static final Target btn_prev_shower_DTV = new Target("btn_prev_shower_DTV","//*[@id=\"dtv-shower-btnPrev\"]",Target.XPATH);
	public static final Target btn_next_shower_DTV = new Target("btn_next_shower_DTV","//*[@id=\"dtv-shower-btnNext\"]",Target.XPATH);
	
	
	
	public static final Target spa_Experience  = new Target("spa_Experience","//*[@id='dtv-details-experiences']",Target.XPATH);
	public static final Target details_sign  = new Target("details_sign","//*[@id='dtv-details-details']",Target.XPATH);
	public static final Target play_btn  = new Target("play_btn","//*[@id='dtv-details-video']/video",Target.XPATH);
	DTVData dtvData = DTVData.fetch("DTVData");
	
	public Kohler_DTVPage atDTVPage()
	{
		try 
		{
			getCommand().waitForTargetPresent(Kohler_HomePage.hamBurger_Icon);
			getCommand().click(Kohler_HomePage.hamBurger_Icon);
			getCommand().click(Kohler_HomePage.bathroom_btn);
			getCommand().waitForTargetPresent(Kohler_GeneralNavigation.bathroom_Link).click(Kohler_GeneralNavigation.bathroom_Link);
			getCommand().waitForTargetPresent(ExploreAll).click(ExploreAll);
	
		} 
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		log("DTV page opened",LogType.STEP);
		return this;
	}
	
	public Kohler_DTVPage verifyDTVPageLoad()
	{
		try 
		{
			//Retrieving page state
			JavascriptExecutor js = (JavascriptExecutor) getCommand().driver;
			String test = js.executeScript("return document.readyState").toString();
			//Verifying page load
			if (test.equalsIgnoreCase("complete")) {
				Assert.assertEquals(getCommand().driver.getTitle(), dtvData.PageTitle);
				log ("DTV Page loaded",LogType.STEP);
			}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_DTVPage verifyDTVTitle()
	{
		try 
		{
			String pageTitle = getCommand().driver.getTitle();
			Assert.assertEquals(pageTitle, dtvData.PageTitle);
			log ("DTV Page Title Verified",LogType.STEP);
		} 
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_DTVPage verifyDTVShare()
	{
		try 
		{
			//Finding Share section and its opacity
			//WebElement share_btns = getCommand().driver.findElement(By.xpath("//*[@id='shareButtons']/li"));
			//String opacity = share_btns.getCssValue("opacity");
			scrollDown();
			//Share section activation and assertion
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);
			//getCommand().waitForTargetPresent(share_DTV).click(share_DTV);
			//Assert.assertNotEquals(share_btns.getCssValue("opacity"), opacity);
			//log ("Share buttons activated", LogType.STEP);
			
			//Verifying share icons
			 List<WebElement> share_links = getCommand().driver.findElements(By.xpath("//*[@id='shareButtons']/li"));
    		for (int i=0; i<share_links.size();i++)
    		{
    			switch(share_links.get(i).getAttribute("id"))
    			{
    			case "shareHouzz":
    				share_links.get(i).click();
    				share_links.get(i).click();
    				log("Share on Houzz activated",LogType.STEP);
    				ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
                    log("Switching to new tab",LogType.STEP);
                    getCommand().driver.switchTo().window(listofTabs.get(1));
                    Assert.assertEquals("Sign In", getCommand().driver.getTitle());
                    log("Houzz page verified",LogType.STEP);
                    getCommand().driver.close();
                    getCommand().driver.switchTo().window(listofTabs.get(0)); 
                    log("Switching back to original tab",LogType.STEP);
    				break;
    				
    			case "sharePinterest":
    				share_links.get(i).click();
    				log("Share on Pinterest activated",LogType.STEP);
    				ArrayList<String> listofTabs1 = new ArrayList<String> (getCommand().driver.getWindowHandles());
                    log("Switching to new tab",LogType.STEP);
                    getCommand().driver.switchTo().window(listofTabs1.get(1));
                    Assert.assertEquals("Pinterest", getCommand().driver.getTitle());
                    log("Pinterest page verified",LogType.STEP);
                    getCommand().driver.close();
                    getCommand().driver.switchTo().window(listofTabs1.get(0)); 
                    log("Switching back to original tab",LogType.STEP);
    				break;
    				
    			case "shareFacebook":
    				share_links.get(i).click();
    				log("Share on Facebook activated",LogType.STEP);
    				ArrayList<String> listofTabs2 = new ArrayList<String> (getCommand().driver.getWindowHandles());
                    log("Switching to new tab",LogType.STEP);
                    getCommand().driver.switchTo().window(listofTabs2.get(1));
                    Assert.assertEquals("Log in to Facebook | Facebook", getCommand().driver.getTitle());
                    log("Facebook page verified",LogType.STEP);
                    getCommand().driver.close();
                    getCommand().driver.switchTo().window(listofTabs2.get(0)); 
                    log("Switching back to original tab",LogType.STEP);
    				break;
    			}
    		}			
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}

	public Kohler_DTVPage verifyDTVWTB()
	{
		try 
		{
			//scrolling to CTA
			scrollDown();
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);
			//Verify CTA opens PDP page
			log("DTV+ PDP tab opened",LogType.STEP);
			getCommand().sendKeys(discover_CTA_DTV, Keys.chord(Keys.CONTROL,Keys.RETURN));
			ArrayList<String> listofTabs = new ArrayList<String> (getCommand().driver.getWindowHandles());
            log("Switching to DTV+ PDP tab",LogType.STEP);
            getCommand().driver.switchTo().window(listofTabs.get(1));
            boolean status_PDP = getCommand().driver.getCurrentUrl().contains("/productDetail/");
            Assert.assertEquals(true, status_PDP);
            Assert.assertEquals( dtvData.WTBProduct_PageTitle, getCommand().driver.getTitle());
            log("DTV+ PDP tab verified",LogType.STEP);
            getCommand().driver.close();
            getCommand().driver.switchTo().window(listofTabs.get(0));
            log("Switching back to original tab",LogType.STEP);
            
            //Verify enter ZIP code field
    		DTVData dtvData = DTVData.fetch("DTVData");
    		String zipCode_text=getCommand().getAttribute(enter_zip_DTV, "placeholder");
    		System.out.println(zipCode_text);
    		
    		if(getCommand().isTargetVisible(enter_zip_DTV)&&zipCode_text.contains("Please enter zip code")) {
    			
    			log("Entering ZipCode and Finding Showroom",LogType.STEP);
                getCommand().sendKeys(enter_zip_DTV, dtvData.ZipCode);
                Assert.assertNotEquals(zipCode_text, getCommand().getText(enter_zip_DTV),"Zip code box text is not visible");
                getCommand().click(btn_store_locator_DTV);
             
                boolean store_Locator = getCommand().driver.getCurrentUrl().contains("/storelocator/");
                Assert.assertEquals(true, store_Locator);
                Assert.assertEquals(dtvData.StoreFinder_PageTitle, getCommand().driver.getTitle());
                log("Store Locator tab verified",LogType.STEP);
              
    		}
    		else {
    			log("Entering ZipCode field is not available",LogType.STEP);
    		}
    		
    		
    		
         
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	/*public Kohler_DTVPage verifyDTVHeroNav()
	{
		try
		{
			//Identify Navigation dots
			WebElement nav_section = getCommand().driver.findElement(By.xpath("//*[@id=\"fullPage-nav\"]/ul"));
    		List<WebElement> nav_links = nav_section.findElements(By.tagName("li"));
    		log("Clicking on each nav dot available in hero grid",LogType.STEP);
			int nav_index = 1;
    		
			//Verify navigation click and display
    		for (WebElement ele:nav_links)
    		{
    			if (ele.getAttribute("style") != "display: none;" && nav_index < nav_links.size())
    			{
    				ele.click();
    				Thread.sleep(2000);
    				WebElement itemClass = getCommand().driver.findElement(By.xpath("//*[@id=\"fullPage-nav\"]/ul/li["+nav_index+"]/a"));
    				Assert.assertEquals("active", itemClass.getAttribute("class"));
    				log("Clicked on Nav dot:"+nav_index+" and verified",LogType.STEP);
    				nav_index++;
    			}
    		}
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}*/
	
	public Kohler_DTVPage verifyDTVGlobalNav()
	{
		try
		{
			//Verify global nav is visible on page load
			boolean brand_Tray=getCommand().driver.findElement(By.xpath("//*[@id='mobile-nav-top']/div")).isDisplayed();
			System.out.println(brand_Tray);
			if(brand_Tray) {
				
				log("Brand banner is visible on page",LogType.STEP);
				scrollDown();
				getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", discover_CTA_DTV);	
				boolean brand_Tray1=getCommand().driver.findElement(By.xpath("//*[@id='mobile-nav-top']/div")).isDisplayed();
				if(brand_Tray1) {
					log("Brand banner is  visible after scroll",LogType.STEP);
				}
				else {
					log("Brand banner is not visible after scroll",LogType.STEP);
				}
				}
			else {
				log("Brand banner is not visible on page before scroll",LogType.STEP);
			}
			
		
			
			
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	



	
	
	public Kohler_DTVPage verifyDTVSpaExperience()
	{
		try
		{
			
			scrollDown();			
			//getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", scrubber_div_DTV);
			log("Clicking of Play button for video playing",LogType.STEP);
			
			if(getCommand().isClickable(play_btn)) {
				
				getCommand().click(play_btn);
				getCommand().waitFor(7);
				getCommand().click(spa_Experience);
				//String after_ClickSpa=getCommand().getAttribute(spa_Experience, "class").substring(-1, -3);
				boolean details=getCommand().driver.findElement(By.xpath("//*[@id='dtv-details-experiences']/p[1]")).isDisplayed();
				
				if(details==true) {
					log("The spa experience is displayed",LogType.STEP);
					getCommand().click(spa_Experience);
					getCommand().click(details_sign);
					//String after_ClickDetails=getCommand().getAttribute(details_sign, "class").substring(-1, -3);
					
					if(details) {
						log("The details are displayed",LogType.STEP);
						getCommand().click(details_sign);
					}
					else {
						log("The details are not displayed",LogType.STEP);
					}
				}
				else {
					log("The spa experience is not displayed",LogType.STEP);
				}
				
				
			}
			else {
				log("Video is not playing",LogType.STEP);
			}
			
			
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	public Kohler_DTVPage verifyDTVShowerExperience()
	{
		try
		{
			WebElement shower_dtv = getCommand().driver.findElement(By.xpath("//*[@id='dtv-shower-presets']/div[5]"));
			
			scrollDown();			
			//getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", share_DTV);
			getCommand().executeJavaScript("arguments[0].scrollIntoView(true);", btn_prev_shower_DTV);
			
			
    		List<WebElement> shower_links_dtv = shower_dtv.findElements(By.className("dtv-shower-copy"));
    		ArrayList<String> list = new ArrayList<String>();
    		log("Identified shower experiences",LogType.STEP);
    		
    		for (WebElement ele:shower_links_dtv)
    		{
    			String span_shower_dtv = "//*[@id=\""+ele.getAttribute("id") +"\"]" + "/span";
    			list.add(getCommand().driver.findElement(By.xpath(span_shower_dtv)).getText());
    			log("Shower Experience: "+getCommand().driver.findElement(By.xpath(span_shower_dtv)).getText()+" verified",LogType.STEP);
    			getCommand().click(btn_next_shower_DTV);
    			Thread.sleep(1000);
    		}
    		CompareDataFromSameList(list);
		}
		catch (Exception ex) {
			Assert.fail(ex.getMessage());
		}
		return this;
	}
	
	//Helpers
	public Kohler_DTVPage scrollDown()
	{
		Actions actions = new Actions(getCommand().driver);
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		actions.keyDown(Keys.CONTROL).sendKeys(Keys.END).perform();
		return this;
	}
	
	public Kohler_DTVPage CompareDataFromSameList(List<String> list)
	{
		for (int i = 0; i < list.size()-1; i++) 
		{
			for (int k = i+1; k < list.size(); k++) 
			{
				if(list.get(i).equals(list.get(k)))
				{
					Assert.fail("Mismatch in data present in the list");
				}				      
			}	      
		}		
		return this;
	}
	
	public Kohler_DTVPage closeBrowser()
	{
		log("Close Browser",LogType.STEP);
		getCommand().closeCurrentWindow();
		return this;
	}

}
